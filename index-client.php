<?php 
	$rambo_pro_theme_options = theme_data_setup();
	$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
	$rambo_client_home_speed = $current_options['rambo_client_home_speed'];
	$ThemeData = get_option('rambo_pro_theme_options');
	$client_options  = ! empty($current_options['rambo_clients_content']) ? $current_options['rambo_clients_content'] : '';
	if(empty($client_options))
	{
        if (!empty($current_options['slider_category'])){
			
			// Set default data from old pro theme.	
			$count_posts = wp_count_posts( 'rambopro_clientstrip')->publish;
			$args = array( 'post_type' => 'rambopro_clientstrip','posts_per_page' =>$count_posts);
			$clientstrip = new WP_Query( $args ); 

			if( $clientstrip->have_posts() )
				{
					while ( $clientstrip->have_posts() ) : $clientstrip->the_post();
					
					$pro_clientstrip_data_old[] = array(
					'title'      => get_the_title(),
					'text'       => get_post_meta( get_the_ID(), 'description_meta_save', true ),
					'link'       => get_post_meta( get_the_ID(), 'clientstrip_link', true ),
					'image_url'  => get_the_post_thumbnail_url(),
					'open_new_tab' => get_post_meta( get_the_ID(), 'meta_client_target', true ),
					'id'    => 'customizer_repeater_56d7ea7f40b96',
						);
				
					endwhile;  
					$client_options = json_encode($pro_clientstrip_data_old);
				}
        }else{
		$client_options= json_encode( array(
                    array(
                    
                    'link'       => '#',
                    'image_url'  => get_template_directory_uri().'/images/clients/logo1.png',
                    'open_new_tab' => 'no',
                    'id'         => 'customizer_repeater_56d7ea7f40b96',
                    ),
                    
                    array(
                    
                    'link'       => '#',
                    'image_url'  => get_template_directory_uri().'/images/clients/logo2.png',
                    'open_new_tab' => 'no',
                    'id'         => 'customizer_repeater_56d7ea7f40b97',
                    ),
                    
                    array(
                    
                    'link'       => '#',
                    'image_url'  => get_template_directory_uri().'/images/clients/logo3.png',
                    'open_new_tab' => 'no',
                    'id'         => 'customizer_repeater_56d7ea7f40b98',
                    
                    ),
                    
                    array(
                    
                    'link'       => '#',
                    'image_url'  => get_template_directory_uri().'/images/clients/logo4.png',
                    'open_new_tab' => 'no',
                    'id'         => 'customizer_repeater_56d7ea7f40b99',
                    
                    ),
                    
                    array(
                    
                    'link'       => '#',
                    'image_url'  => get_template_directory_uri().'/images/clients/logo5.png',
                    'open_new_tab' => 'no',
                    'id'         => 'customizer_repeater_56d7ea7f40b100',
                    
                    ),
                    
                    array(
                    
                    'link'       => '#',
                    'image_url'  => get_template_directory_uri().'/images/clients/logo6.png',
                    'open_new_tab' => 'no',
                    'id'         => 'customizer_repeater_56d7ea7f40b101',
                    
                    ),
                    
                    array(
                    
                    'link'       => '#',
                    'image_url'  => get_template_directory_uri().'/images/clients/logo7.png',
                    'open_new_tab' => 'no',
                    'id'         => 'customizer_repeater_56d7ea7f40b102',
                    
                    ),
                
                ) );
	}
    }


if($current_options['homepage_client_hide'] == true) { ?>
<script>
jQuery(function() {
	jQuery("#our_client_product").carousel(	{ 
	    interval: <?php echo $rambo_client_home_speed; ?>,
		pause: "hover",
	});
	
	jQuery('#our_client_product .item').each(function(){
		
		
	  var next = jQuery(this).next();
	  if (!next.length) {
		next = jQuery(this).siblings(':first');
	  }
	  next.children(':first-child').clone().appendTo(jQuery(this));
	  
	  // For Two Column Layout i=0
	  // For Three Column Layout i=1
	  for (var i=0;i<4;i++) {
		next=next.next();
		if (!next.length) {
			next = jQuery(this).siblings(':first');
		}
		
		next.children(':first-child').clone().appendTo(jQuery(this));
	  }
		  
	});
	
	
});
</script>
<div class="home_client">	
	<div class="container">	
			<?php if($current_options['homepage_client_title'] || $current_options['homepage_client_contents']){ ?>
			<div class="row-fluid featured_port_title">
			
				<?php if($current_options['homepage_client_title']!='') { ?>
					<h1><?php echo $current_options['homepage_client_title']; ?></h1>
				<?php } ?>
				
				<?php if($current_options['homepage_client_contents']!='') { ?>
					<p><?php echo $current_options['homepage_client_contents']; ?></p>
				<?php } ?>
				
			</div>
			<?php } ?>
            <div class="row our_client_service_section">
			
				<div id="our_client_product" class="carousel slide" data-ride="carousel">
				
			        <div class="carousel-inner">

					<?php
					
					$t=true;
					$client_options = json_decode($client_options);
					if( $client_options!='' )
						{
					$i=1;
					foreach($client_options as $client_iteam){ 
							$client_logo = ! empty( $client_iteam->image_url ) ? apply_filters( 'rambo_translate_single_string', $client_iteam->image_url, 'Client section' ) : '';
							$client_link = ! empty( $client_iteam->link ) ? apply_filters( 'rambo_translate_single_string', $client_iteam->link, 'Client section' ) : '';
							$open_new_tab = $client_iteam->open_new_tab;
						
						?>			
						<div class="item <?php if($i==1) { echo 'active';} $i++; ?>">
							<div class="span2">
								<a href="<?php echo $client_link; ?>" <?php if($open_new_tab == 'yes'){ echo 'target="_blank"';}?>><img src="<?php echo $client_logo; ?>" alt="clients"/></a>	
							</div>
					    </div>
				
					<?php } } ?>
				


	                </div>
                </div>
			</div>	
	</div>
</div>

<?php } ?>