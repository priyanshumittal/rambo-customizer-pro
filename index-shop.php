<?php
/*
 * Home page shop section
 */
$rambo_pro_theme_options = theme_data_setup();
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options ); ?>
<?php 
if ( class_exists( 'WooCommerce' ) ) {
	
if($current_options['home_shop_enable'] == false) { 
?>
<div class="additional_section_two home-shop">	
	<div class="container">	
			<?php if($current_options['home_shop_title'] || $current_options['home_shop_desciption']){ ?>
			<div class="row-fluid featured_port_title">
				<?php if($current_options['home_shop_title']!='') { ?>
					<h1><?php echo $current_options['home_shop_title']; ?></h1>
				<?php } ?>
				
				<?php if($current_options['home_shop_desciption']!='') { ?>
					<p><?php echo $current_options['home_shop_desciption']; ?></p>
				<?php } ?>
			</div>
			<?php } ?>
			
			<?php  if(is_active_sidebar('additional-area-two'))
				{
					echo '<div id="additional-sidebar-two " class="row additional-area-two">';
					dynamic_sidebar('additional-area-two');
					echo '</div>';
				}
				?>	
			
			
	</div>	
</div>
<?php }  } ?>