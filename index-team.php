<?php
/*
 * Home page Team section
*/
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), theme_data_setup() );
get_template_part('section-variation/team/team-'.$current_options['home_team_layout_section']);
?>