<?php
/************* Home slider custom post type ************************/
	$rambo_pro_theme_options = theme_data_setup();
	$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
    $slug_slide = $current_options['rambo_slider_slug'];
	$slug_portfolio = $current_options['rambo_portfolio_slug'];
	$slug_team = $current_options['rambo_team_slug'];

function rambopro_slider() {
	register_post_type( 'rambopro_slider');
	
}
add_action( 'init', 'rambopro_slider' );

/************* Home Service custom post type ***********************/	
function rambopro_service_type()
{	
	register_post_type( 'rambopro_service' );
}
add_action( 'init', 'rambopro_service_type' );

/************* Home project custom post type ***********************	*/
function rambopro_project_type()
{
	register_post_type( 'rambopro_project',
		array(
			'labels' => array(
				'name' => __('Portfolio / Project','rambo'),
				//'singular_name' => 'Featured Services',
				'add_new' => __('Add New', 'rambo'),
				'add_new_item' => __('Add New Project','rambo'),
				'edit_item' => __('Add New','rambo'),
				'new_item' => __('New Link','rambo'),
				'all_items' => __('All Portfolio Project','rambo'),
				'view_item' => __('View Link','rambo'),
				'search_items' => __('Search Links','rambo'),
				'not_found' =>  __('No Links found','rambo'),
				'not_found_in_trash' => __('No Links found in Trash','rambo'), 
			),
			'supports' => array('title','editor','thumbnail'),
			'show_in' => true,
			'show_in_nav_menus' => false,
			'rewrite' => array('slug' =>$GLOBALS['slug_portfolio']),
			'public' => true,
			'menu_position' =>20,
			'public' => true,
			'menu_icon' => WEBRITI_TEMPLATE_DIR_URI . '/images/option-icon-media.png',
		)
	);
}
add_action( 'init', 'rambopro_project_type' );

/******************************Team POST TYPE*******************************************************/
function rambopro_team_type()
{	register_post_type( 'rambopro_team',
		array(
			
			)
	);
}
add_action( 'init', 'rambopro_team_type' );

/************* Home project custom post type ***********************/		
function rambopro_client_strip()
{	register_post_type( 'rambopro_clientstrip',
		array(
			
			)
	);
}
add_action( 'init', 'rambopro_client_strip' );

/******************************Testimonial POST TYPE*****************************************/
function rambopro_testimonials_type()
{	register_post_type( 'rambopro_testimonial' );
}
add_action( 'init', 'rambopro_testimonials_type' );
?>