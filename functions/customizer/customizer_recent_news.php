<?php
// customizer Recent News panel
function customizer_recent_news_panel( $wp_customize ) {
		//Recent News panel
		$wp_customize->add_section( 'news_settings' , array(
		'title'      => __('Latest news settings', 'rambo'),
		'panel'  => 'section_settings',
		'priority'   => 5,
		) );
		
		   	// hide meta content
			$wp_customize->add_setting(
			'rambo_pro_theme_options[home_meta_section_settings]',
			array(
				'default' => false,
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
				'type' => 'option',
			)	
			);
			$wp_customize->add_control(
			'rambo_pro_theme_options[home_meta_section_settings]',
			array(
				'label' => __('Hide post meta from Latest News section','rambo'),
				'section' => 'news_settings',
				'type' => 'checkbox',
			)
			);
			
			
			// enable Recent News section
			$wp_customize->add_setting('rambo_pro_theme_options[news_enable]',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('rambo_pro_theme_options[news_enable]',array(
			'label' => __('Hide news section','rambo'),
			'section' => 'news_settings',
			'type' => 'checkbox',
			) );
			
			
			//Latest News Title
			$wp_customize->add_setting(
				'rambo_pro_theme_options[latest_news_tag_line]',
				array(
					'default' => __('Latest News','rambo'),
					'capability'     => 'edit_theme_options',
					'sanitize_callback' => 'rambo_project_sanitize_html',
					'type' => 'option',
					)
				);	
			$wp_customize->add_control('rambo_pro_theme_options[latest_news_tag_line]',array(
				'label'   => __('Title','rambo'),
				'section' => 'news_settings',
				 'type' => 'text',)
			);	


			$wp_customize->add_setting(
			'rambo_pro_theme_options[home_news_style]', array(
        	'default'        => 1,
        	'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option',
    		));
   			$wp_customize->add_control('rambo_pro_theme_options[home_news_style]', array(
        	'label'   => __('News Variation', 'rambo'),
        	'section' => 'news_settings',
        	'type'    => 'select',
			'priority'   => 140,
			'choices' => array(1=>__('News Variation 1', 'rambo'), 2=>__('News Variation 2', 'rambo'), 3=>__('News Variation 3', 'rambo'), 4=>__('News Variation 4', 'rambo')),
    ));


   			$wp_customize->add_setting(
			'rambo_pro_theme_options[post_display_count]', array(
        	'default'        => 3,
        	'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option',
    		));
   			$wp_customize->add_control('rambo_pro_theme_options[post_display_count]', array(
        	'label'   => __('No of post', 'rambo'),
        	'section' => 'news_settings',
        	'type'    => 'select',
			'priority'   => 140,
			'choices' => array(1=>__('1', 'rambo'), 2=>__('2', 'rambo'), 3=>__('3', 'rambo'), 4=>__('4', 'rambo'), 5=>__('5', 'rambo'), 6=>__('6', 'rambo'), 7=>__('7', 'rambo'), 8=>__('8', 'rambo'), 9=>__('9', 'rambo'), 10=>__('10', 'rambo'), 11=>__('11', 'rambo'), 12=>__('12', 'rambo')),
    ));
}
add_action( 'customize_register', 'customizer_recent_news_panel' );


/**
 * Add selective refresh for Front page section section controls.
 */
function rambo_pro_register_home_blog_section_partials( $wp_customize ){

$wp_customize->selective_refresh->add_partial( 'rambo_pro_theme_options[latest_news_tag_line]', array(
		'selector'            => '.for_mobile .row-fluid .team_head_title h3',
		'settings'            => 'rambo_pro_theme_options[latest_news_tag_line]',
	
	) );
	
}

add_action( 'customize_register', 'rambo_pro_register_home_blog_section_partials' );