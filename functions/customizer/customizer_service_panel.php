<?php
// customizer serive panel
function service_customizer_service_panel( $wp_customize ) {
		//Service panel
		$wp_customize->add_section( 'service_settings' , array(
		'title'      => __('Service settings', 'rambo'),
		'panel'  => 'section_settings',
		'priority'   => 3,
		) );
			
			// enable service section
			$wp_customize->add_setting('rambo_pro_theme_options[home_service_enabled]',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('rambo_pro_theme_options[home_service_enabled]',array(
			'label' => __('Hide service section','rambo'),
			'section' => 'service_settings',
			'type' => 'checkbox',
			) );
			
		
		// Service title
			$wp_customize->add_setting('rambo_pro_theme_options[service_section_title]',array(
			'default' => __('Our Services','rambo'),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'rambo_service_sanitize_html',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('rambo_pro_theme_options[service_section_title]',array(
			'label' => __('Title','rambo'),
			'section' => 'service_settings',
			'type' => 'text',
			) );
			
			
			// service description
			$wp_customize->add_setting('rambo_pro_theme_options[service_section_descritpion]',array(
			'default' => __('Check out our Main Services which we offer to every client','rambo'),
			'sanitize_callback' => 'rambo_service_sanitize_html',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('rambo_pro_theme_options[service_section_descritpion]',array(
			'label' => __('Description','rambo'),
			'section' => 'service_settings',
			'type' => 'textarea',
			) );


			//Service Design
			$wp_customize->add_setting(
    		'rambo_pro_theme_options[service_layout_section]',
    		array(
       			'default' => 1,
				'type' => 'option',
				'sanitize_callback' => 'sanitize_text_field',
		
    			));

			$wp_customize->add_control(
    		'rambo_pro_theme_options[service_layout_section]',
    		array(
        		'type' => 'select',
        		'label' => __('Select Service Design','appointment'),
       			 'section' => 'service_settings',
		 		'choices' => array(1=>__('Design 1', 'appointment'), 2=>__('Design 2', 'appointment'), 3=>__('Design 3', 'appointment'), 4=>__('Design 4', 'appointment'), 5=>__('Design 5', 'appointment')),
			));

			//Service Column Layout
			$wp_customize->add_setting(
    		'rambo_pro_theme_options[service_col_layout]',
    		array(
       			'default' => 3,
				'type' => 'option',
				'sanitize_callback' => 'sanitize_text_field',
		
    			));

			$wp_customize->add_control(
    		'rambo_pro_theme_options[service_col_layout]',
    		array(
        		'type' => 'select',
        		'label' => __('Select Column Layout','appointment'),
       			 'section' => 'service_settings',
		 		'choices' => array(6=>__('2 Column', 'appointment'), 4=>__('3 Column', 'appointment'), 3=>__('4 Column', 'appointment')),
			));
			
			
			if ( class_exists( 'Rambo_Repeater' ) ) {
			$wp_customize->add_setting('rambo_pro_theme_options[rambo_service_content]', array(
			'type'=> 'option',
			) );

			$wp_customize->add_control( new rambo_Repeater( $wp_customize, 'rambo_pro_theme_options[rambo_service_content]', array(
				'label'                             => esc_html__( 'Service content', 'rambo' ),
				'section'                           => 'service_settings',
				'priority'                          => 10,
				'add_field_label'                   => esc_html__( 'Add new Service', 'rambo' ),
				'item_name'                         => esc_html__( 'Service', 'rambo' ),
				'customizer_repeater_icon_control'  => true,
				'customizer_repeater_title_control' => true,
				'customizer_repeater_text_control'  => true,
				'customizer_repeater_link_control'  => true,
				'customizer_repeater_button_text_control' => true,
				'customizer_repeater_image_control' => true,
				'customizer_repeater_checkbox_control' => true,
				) ) );
		}
			
			
			function rambo_service_sanitize_html( $input ) {
				return force_balance_tags( $input );
			}
}
add_action( 'customize_register', 'service_customizer_service_panel' );

/**
 * Add selective refresh for Front page section section controls.
 */
function rambo_pro_register_home_service_section_partials( $wp_customize ){

$wp_customize->selective_refresh->add_partial( 'rambo_pro_theme_options[service_section_title]', array(
		'selector'            => '.home_service_section .featured_port_title h1',
		'settings'            => 'rambo_pro_theme_options[service_section_title]',
	
	) );
	
$wp_customize->selective_refresh->add_partial( 'rambo_pro_theme_options[service_section_descritpion]', array(
		'selector'            => '.home_service_section .featured_port_title p',
		'settings'            => 'rambo_pro_theme_options[service_section_descritpion]',
	
	) );
	
$wp_customize->selective_refresh->add_partial( 'rambo_pro_theme_options[rambo_service_content]', array(
		'selector'            => '.home_service_section .sidebar-service',
		'settings'            => 'rambo_pro_theme_options[rambo_service_content]',
	
	) );
	
	
}

add_action( 'customize_register', 'rambo_pro_register_home_service_section_partials' );
?>