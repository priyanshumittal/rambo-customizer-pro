<?php
function rambo_copyright_customizer( $wp_customize ) {
	
/* copyright */
	$wp_customize->add_panel( 'copyrightpanel', array(
		'priority'       => 610,
		'capability'     => 'edit_theme_options',
		'title'      => __('Footer copyright settings', 'rambo'),
	) );
	
	//Custom css
	$wp_customize->add_section( 'copyrightsection' , array(
		'title'      => __('Copyright text', 'rambo'),
		'panel'  => 'copyrightpanel',
		'priority'   => 1,
   	) );
	
	$wp_customize->add_setting('rambo_pro_theme_options[footer_copyright]', array(
        'default'        => sprintf(__('Copyright @ 2024 - RAMBO. Designed by <a href="http://webriti.com" rel="nofollow" target="_blank"> Webriti</a>','rambo')),
        'capability'     => 'edit_theme_options',
		'type'=> 'option',
    ));
	
    $wp_customize->add_control( 'rambo_pro_theme_options[footer_copyright]', array(
        'label'   => __('Copyright text','rambo'),
        'section' => 'copyrightsection',
        'type' => 'textarea',
    ));
	
}
add_action( 'customize_register', 'rambo_copyright_customizer' );

/**
 * Add selective refresh for Front page section section controls.
 */
function rambo_pro_register_home_copy_right_section_partials( $wp_customize ){

$wp_customize->selective_refresh->add_partial( 'rambo_pro_theme_options[footer_copyright]', array(
		'selector'            => '.footer-section .span8',
		'settings'            => 'rambo_pro_theme_options[footer_copyright]',
	
	) );
	
	$wp_customize->selective_refresh->add_partial( 'header_site_title', array(
        'selector' => '.brand',
        'settings' => array( 'blogname' ),
        'render_callback' => function() {
            return get_bloginfo( 'name', 'display' );
        },
    ) );
	
}

add_action( 'customize_register', 'rambo_pro_register_home_copy_right_section_partials' );