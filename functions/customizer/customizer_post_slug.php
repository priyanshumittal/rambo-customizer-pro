<?php
function rambo_post_slug_customizer( $wp_customize ) {
	
// add section to manage About us tPage
	$wp_customize->add_section(
        'post_slug_setting',
        array(
            'title' => __('Post type slug setting','rambo'),
			'panel'  => 'rambo_template',
			'priority'   => 6,
			)
    );	

//Portfolio/Project Slug
$wp_customize->add_setting(
    'rambo_pro_theme_options[rambo_portfolio_slug]',
    array(
        'default' => 'rambo_project',
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
		)
	);	
	$wp_customize->add_control('rambo_pro_theme_options[rambo_portfolio_slug]',array(
    'label'   => __('Project slug','rambo'),
    'section' => 'post_slug_setting',
	 'type' => 'text',)  );	

	 class spa_Customize_slug extends WP_Customize_Control {
			public function render_content() { ?>
			<h3><?php _e("After Changing the slug, please do not forget to save permalinks. Without saving, the old permalinks will not revise.","rambo"); ?> 
			<?php
			}
			}
			
			$wp_customize->add_setting( 'rambo_pro_theme_options[rambo_slug_setting]', array(
			'default'				=> false,
			'capability'			=> 'edit_theme_options',
			'sanitize_callback'	=> 'wp_filter_nohtml_kses',
			));
			$wp_customize->add_control(
			new spa_Customize_slug(
			$wp_customize,
			'rambo_pro_theme_options[rambo_slug_setting]',
			array(
				'section'				=> 'post_slug_setting',
				'settings'				=> 'rambo_pro_theme_options[rambo_slug_setting]',
			)));
			
			class WP_slug_Customize_Control extends WP_Customize_Control {
			public $type = 'new_menu';
			/**
			* Render the control's content.
			*/
			public function render_content() {
			?>
			<a href="<?php bloginfo ( 'url' );?>/wp-admin/options-permalink.php" class="button"  target="_blank"><?php _e('Click here permalinks setting','rambo'); ?></a>
			<?php
			}
			}

			$wp_customize->add_setting(
				'slug',
				array(
					'default' => '',
					'capability'     => 'edit_theme_options',
					'sanitize_callback' => 'sanitize_text_field',
				)	
			);
			$wp_customize->add_control( new WP_slug_Customize_Control( $wp_customize, 'slug', array(	
					'section' => 'post_slug_setting',
				))
			);
	 
	 }
	 add_action('customize_register','rambo_post_slug_customizer');