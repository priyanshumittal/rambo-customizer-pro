<?php
function rambo_header_customizer( $wp_customize ) {


	$wp_customize->add_setting( 'rambo_logo_length',
            array(
                'default' => 154,
                'transport'         => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );

    $wp_customize->add_control( new Rambo_Slider_Custom_Control( $wp_customize, 'rambo_logo_length',
        array(
            'label' => esc_html__( 'Logo Width', 'rambo'  ),
            'priority' => 50,
            'section' => 'title_tagline',
            'input_attrs' => array(
                'min' => 0,
                'max' => 500,
                'step' => 1,
            ),
        )
    ) );
	
	/* Header Section */	
	$wp_customize->add_panel( 'header_options', array(
		'priority'       => 125,
		'capability'     => 'edit_theme_options',
		'title'      => __('Header settings', 'rambo'),
	) );
		
		// custom css
		if ( version_compare( $GLOBALS['wp_version'], '4.6', '>=' ) ) {
			
			//Custom css
			$wp_customize->add_section( 'custom_css' , array(
				'title'      => __('Custom CSS','rambo'),
				'panel'  => 'header_options',
				'priority'   => 100,
			) );
		}
		else{
			
			//Custom css
			$wp_customize->add_section( 'custom_css_section' , array(
				'title'      => __('Custom CSS', 'rambo'),
				'panel'  => 'header_options',
				'priority'   => 100,
			) );
				$wp_customize->add_setting(
				'rambo_pro_theme_options[webrit_custom_css]'
					, array(
					'default'        => '',
					'capability'     => 'edit_theme_options',
					'sanitize_callback' => 'sanitize_text_field',
					'type'=> 'option',
				));
				$wp_customize->add_control( 'rambo_pro_theme_options[webrit_custom_css]', array(
					'label'   => __('Custom CSS', 'rambo'),
					'section' => 'custom_css_section',
					'type' => 'textarea',
				));
		}
		 

		$wp_customize->add_section(
			'footer_google_analytics',
			array(
				'title' => __('Google tracking code','rambo'),
			   'priority'    => 500,
				'panel' => 'header_options',
			)
		);
		
		// Google Anlytics
		$wp_customize->add_setting(
		'rambo_pro_theme_options[google_analytics]',
		array(
			'default' => '',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option',
		)
		
		);
		$wp_customize->add_control(
		'rambo_pro_theme_options[google_analytics]',
		array(
			'label' => __('Google tracking code','rambo'),
			'section' => 'footer_google_analytics',
			'type' => 'textarea',
		)
		);

		// Header Preset
	$wp_customize->add_section( 'header_preset' , array(
		'title'      => __('Header Variation', 'rambo'),
		'panel'  => 'header_options',
		'priority'   => 150,
   	) );
	$wp_customize->add_setting(
	'rambo_pro_theme_options[header_preset_section]', array(
        'default'        => 1,
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
    ));
    $wp_customize->add_control('rambo_pro_theme_options[header_preset_section]', array(
        'label'   => __('Header Variation', 'rambo'),
        'section' => 'header_preset',
        'type'    => 'select',
		'priority'   => 140,
		'choices' => array(1=>__('Header Variation 1', 'rambo'), 2=>__('Header Variation 2', 'rambo'), 3=>__('Header Variation 3', 'rambo'), 4=>__('Header Variation 4', 'rambo'), 5=>__('Header Variation 5', 'rambo')),
    ));
    //Enable/Disable Search Icon
	$wp_customize->add_setting(
    'rambo_pro_theme_options[search_icon]',array(
	'default'    => '',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option'
	));

	$wp_customize->add_control(
    'rambo_pro_theme_options[search_icon]',
    array(
        'type' => 'checkbox',
        'label' => __('Disable Search Icon','rambo'),
        'section' => 'header_preset',
		'priority'   => 150,
    )
	);
	//Search Icon Style
	$wp_customize->add_setting(
	'rambo_pro_theme_options[search_icon_style]', array(
        'default'        => 1,
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
    ));
    $wp_customize->add_control('rambo_pro_theme_options[search_icon_style]', array(
        'label'   => __('Search Effect', 'rambo'),
        'section' => 'header_preset',
        'type'    => 'select',
		'priority'   => 160,
		'choices' => array(1=>__('Pop Up Light', 'rambo'), 2=>__('Pop Up Dark', 'rambo')),
    ));
	
}
add_action( 'customize_register', 'rambo_header_customizer' );
?>