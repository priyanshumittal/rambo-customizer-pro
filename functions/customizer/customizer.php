<?php
/*** Rambo default slider content data  ***/
if ( ! function_exists( 'rambo_slider_default_customize_register' ) ) :
	function rambo_slider_default_customize_register( $wp_customize ){
		
		$ThemeData = get_option('rambo_pro_theme_options');

		if (!empty($ThemeData['slider_category']))
		{
			
			// Run this code section if user comes form pro old.
			$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), theme_data_setup() );
			if( $current_options['slider_category'] ){$SlideCategory = $current_options['slider_category'];}
			else{$SlideCategory = 1;
			}
			$rambo_slider_content_control = $wp_customize->get_setting( 'rambo_pro_theme_options[rambo_slider_content]' );
			if ( ! empty( $rambo_slider_content_control ) )
			{
				$args = array( 'post_type' => 'post', 'category__in'=> $SlideCategory); 	
				$slider = new WP_Query( $args ); 
				if( $slider->have_posts() )
				{
					
					while ( $slider->have_posts() ) : $slider->the_post();
					$my_meta = get_post_meta( get_the_ID() ,'_my_meta', TRUE );
					
					//if ( ! empty( $my_meta ) ) {
						
						if(isset($my_meta['link']))
						{	$meta_value_link = $my_meta['link']; }
						else
						{	$meta_value_link = get_permalink(); }
							
						if(isset($my_meta['check']))
						{	$target ='yes';  } 
						else 
						{	$target ='no';  }
					
					//}
					
					
					$more_enable = sanitize_text_field( get_post_meta( get_the_ID(), 'more_enable', true ));
					$btn_text = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_text', true ));
					$btn_link = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_link', true ));
					$btn_target = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_target', true ));
						
					$pro_slider_data_old[] = array(
					'title'   => get_the_title(),
					'text'       => get_the_excerpt(),
					'button_text' => $btn_text,
					'link'       => $btn_link,
					'image_url'  => get_the_post_thumbnail_url(),
					'open_new_tab' => $btn_target,
					'id'         => 'customizer_repeater_56d7ea7f40b41',
					);
					endwhile; 
					$rambo_slider_content_control->default = json_encode($pro_slider_data_old);
				}

			}
			
						
		}
		elseif($ThemeData!='')
		{
				$ThemeSliderData = get_option('rambo_pro_theme_options');
			// Run this code section if use comes from lite new to Pro new
			if(!isset($ThemeData['rambo_slider_content'])){
			$rambo_slider_content_control = $wp_customize->get_setting( 'rambo_pro_theme_options[rambo_slider_content]' );
			if ( ! empty( $rambo_slider_content_control ) ) 
			{
				$rambo_slider_content_control->default = json_encode( array(
				array(
				'title'		=> isset($ThemeSliderData['slider_title'])? $ThemeSliderData['slider_title'] : esc_html__('Powerful Bootstrap Theme','rambo'),
				'text'		=> isset($ThemeSliderData['slider_text'])? $ThemeSliderData['slider_text']: 'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc',
				'image_url'  => isset($ThemeSliderData['slider_post'])? $ThemeSliderData['slider_post']: get_template_directory_uri().'/images/default/slide/slide1.png',
				'button_text' => isset($ThemeSliderData['slider_readmore_text'])? $ThemeSliderData['slider_readmore_text']: esc_html__('Read More','rambo'),
				'link' => isset($ThemeSliderData['readmore_text_link']) ? $ThemeSliderData['readmore_text_link']:'#',
				'open_new_tab' => 'yes',
				'id'         => 'customizer_repeater_56d7ea7f40b89',
				),
				
				array(
				'title'		=> esc_html__('We Create Responsive Designs','rambo'),
				'text'		=> 'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc',
				'image_url'  => get_template_directory_uri().'/images/default/slide/slide2.png',
				'button_text'      => esc_html__('Read More','rambo'),
				'link'       => '#',
				'open_new_tab' => 'yes',
				'id'         => 'customizer_repeater_56d7ea7f40b90',
				),
				
				
				array(
				'title'		=> esc_html__('Get an Idea','rambo'),
				'text'		=> 'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc',
				'image_url'  => get_template_directory_uri().'/images/default/slide/slide3.png',
				'button_text'      => esc_html__('Read More','rambo'),
				'link'       => '#',
				'open_new_tab' => 'yes',
				'id'         => 'customizer_repeater_56d7ea7f40b91',
				),
				
				array(
				'title'		=> esc_html__('We are Rambo','rambo'),
				'text'		=> 'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc',
				'image_url'  => get_template_directory_uri().'/images/default/slide/slide4.png',
				'button_text'      => esc_html__('Read More','rambo'),
				'link'       => '#',
				'open_new_tab' => 'yes',
				'id'         => 'customizer_repeater_56d7ea7f40b92',
				),
					
				) );

			}
		}
				
		}
		else
		{
			//Run this code section if user Install direct pro new theme (This is the default slider data)
			$rambo_slider_content_control = $wp_customize->get_setting( 'rambo_pro_theme_options[rambo_slider_content]' );
			if ( ! empty( $rambo_slider_content_control ) ) 
			{
				$rambo_slider_content_control->default = json_encode( array(
					array(
					'title'		=> esc_html__('Powerful Bootstrap Theme','rambo'),
					'text'		=> 'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc',
					'image_url'  => get_template_directory_uri().'/images/default/slide/slide1.png',
					'button_text'      => esc_html__('Read More','rambo'),
					'link'       => '#',
					'open_new_tab' => 'yes',
					'id'         => 'customizer_repeater_56d7ea7f40b89',
					),
					array(
					'title'		=> esc_html__('We Create Responsive Design','rambo'),
					'text'		=> 'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc',
					'image_url'  => get_template_directory_uri().'/images/default/slide/slide1.png',
					'button_text'      => esc_html__('Read More','rambo'),
					'link'       => '#',
					'open_new_tab' => 'yes',
					'id'         => 'customizer_repeater_56d7ea7f40b90',
					),
					array(
					'title'		=> esc_html__('Get an Idea','rambo'),
					'text'		=> 'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc',
					'image_url'  => get_template_directory_uri().'/images/default/slide/slide1.png',
					'button_text'      => esc_html__('Read More','rambo'),
					'link'       => '#',
					'open_new_tab' => 'yes',
					'id'         => 'customizer_repeater_56d7ea7f40b91',
					),
					array(
					'title'		=> esc_html__('We are Rambo','rambo'),
					'text'		=> 'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc',
					'image_url'  => get_template_directory_uri().'/images/default/slide/slide1.png',
					'button_text'      => esc_html__('Read More','rambo'),
					'link'       => '#',
					'open_new_tab' => 'yes',
					'id'         => 'customizer_repeater_56d7ea7f40b92',
					),
				) );

			}
		}
	}
	
add_action( 'customize_register', 'rambo_slider_default_customize_register' );
	
endif;




/** Rambo default service content data ***/
if ( ! function_exists( 'rambo_service_default_customize_register' ) ) :
	function rambo_service_default_customize_register( $wp_customize ){
		
		$ServiceOldData = get_option('rambo_feature_page_widget');
			$the_sidebars = wp_get_sidebars_widgets();
			$rambo_service_content = $wp_customize->get_setting( 'rambo_pro_theme_options[rambo_service_content]' );
			if(!empty($the_sidebars['sidebar-service'])){
					$pro_service_data = array();
					foreach ($the_sidebars['sidebar-service'] as $data) {
						$widget_data = explode('-',$data);
						$data  = $widget_data[1];
						if($widget_data[0] == 'rambo_feature_page_widget'){
							$options = get_option( 'widget_rambo_feature_page_widget' );
							$pro_service_data[] = array(
							'icon_value' => isset($options[$widget_data[1]]['icon'])? $options[$widget_data[1]]['icon'] : '',
							'image_url'  => isset($options[$widget_data[1]]['selected_page'])? get_the_post_thumbnail_url($options[$widget_data[1]]['selected_page']) :'',
							'title'      => isset($options[$widget_data[1]]['selected_page'])? get_the_title( $options[$widget_data[1]]['selected_page'] ) : '',
							'text'       => isset($options[$widget_data[1]]['selected_page'])? get_post_field('post_content', $options[$widget_data[1]]['selected_page']) : '',
							'color'		 => '#f22853',
							'button_text' => isset($options[$widget_data[1]]['buttontext'])? $options[$widget_data[1]]['buttontext'] : '',
							'link'       => isset($options[$widget_data[1]]['servicelink'])? $options[$widget_data[1]]['servicelink'] : '',
							'open_new_tab' => isset($options[$widget_data[1]]['target']) ? $options[$widget_data[1]]['target'] : '',
							'id'         => 'customizer_repeater_56d7ea7f40b56',
							
							);
						}
						
					}
					
					$rambo_service_content->default = json_encode($pro_service_data);
		} else if($ServiceOldData==''){
			 //Run this code section if user Install direct pro new theme (This is the default service data)
				$rambo_service_content_control = $wp_customize->get_setting( 'rambo_pro_theme_options[rambo_service_content]' );
				if ( !empty( $rambo_service_content_control ) ) {
					$rambo_service_content_control->default = json_encode( array(
						array(
						'icon_value' => 'fa fa-camera',
						'image_url'  => '',
						'title'      => esc_html__( 'Incredibly Flexible', 'rambo' ),
						'text'       => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.',
						'color'		 => '#f22853',
						'button_text' => 'Read More',
						'link'       => '#',
						'open_new_tab' => 'yes',
						'choice'    => 'customizer_repeater_icon',
						'id'         => 'customizer_repeater_56d7ea7f40b56',
						),
						array(
						'icon_value' => 'fa fa-cogs',
						'image_url'  => '',
						'title'      => esc_html__( 'Powerful Admin', 'rambo' ),
						'text'       => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.',
						'color'		 => '#6dc82b',
						'button_text' => 'Read More',
						'link'       => '#',
						'open_new_tab' => 'yes',
						'choice'    => 'customizer_repeater_icon',
						'id'         => 'customizer_repeater_56d7ea7f40b66',
						),
						array(
						'icon_value' => 'fa fa-user',
						'image_url'  => '',
						'title'      => esc_html__( 'Easy To Use', 'rambo' ),
						'text'       => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.',
						'color'		 => '#fe8000',
						'button_text' => 'Read More',
						'link'       => '#',
						'open_new_tab' => 'yes',
						'choice'    => 'customizer_repeater_icon',
						'id'         => 'customizer_repeater_56d7ea7f40b86',
						),
						array(
						'icon_value' => 'fa fa-desktop',
						'image_url'  => '',
						'title'      => esc_html__( 'Responsive Design', 'rambo' ),
						'text'       => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.',
						'color'		 => '#1abac8',
						'button_text' => 'Read More',
						'link'       => '#',
						'open_new_tab' => 'yes',
						'choice'    => 'customizer_repeater_icon',
						'id'         => 'customizer_repeater_56d7ea7f40b86',
						),
						
					) );

				}
		 }
	}
	
add_action( 'customize_register', 'rambo_service_default_customize_register' );
	
endif;

// Rambo Team content data
if ( ! function_exists( 'rambo_team_default_customize_register' ) ) :
add_action( 'customize_register', 'rambo_team_default_customize_register' );
function rambo_team_default_customize_register( $wp_customize ){
	
	$rambo_pro_theme_options = get_option('rambo_pro_theme_options');

   // if(get_option('rambo_pro_theme_options')!='' && isset($rambo_pro_theme_options['home_client_title'])){

		//$rambo_team_content_control = $wp_customize->get_setting( 'rambo_team_content' );
		
		$ThemeData = get_option('rambo_pro_theme_options');

		if (!empty($ThemeData['slider_category']))
		{

			// Run this code section if user comes form pro old.
			$rambo_team_content_control = $wp_customize->get_setting( 'rambo_pro_theme_options[rambo_team_content]' );
			if ( ! empty( $rambo_team_content_control ) )
			{
			$count_posts = wp_count_posts( 'rambopro_team')->publish;
			$arg = array( 'post_type' => 'rambopro_team','posts_per_page' =>$count_posts);
			$team = new WP_Query( $arg );
			$i=1;
			if($team->have_posts())
			{	

				while ( $team->have_posts() ) : $team->the_post();	
			
				$pro_team_data_old[] = array(
				
					'title'      => get_the_title(),
					'subtitle'   => get_post_meta( get_the_ID(), 'team_designation', true),
					'text'       => get_post_meta( get_the_ID(), 'team_role', true ),
					'link'       => get_post_meta( get_the_ID(), 'team_link', true ),
					'image_url'  => get_the_post_thumbnail_url(),
					'open_new_tab' => get_post_meta( get_the_ID(), 'meta_team_target', true ),
					'id'    => 'customizer_repeater_56d7ea7f40t754',
					);
				endwhile;	
			$rambo_team_content_control->default = json_encode($pro_team_data_old);
						
			}
		
		}}
		else{
	
				//Rambo default team data.
				$rambo_team_content_control = $wp_customize->get_setting( 'rambo_pro_theme_options[rambo_team_content]' );
				if ( ! empty( $rambo_team_content_control ) ) 
				{
				$rambo_team_content_control->default = json_encode( array(
					array(
					'image_url'  => get_template_directory_uri().'/images/team/team1.jpg',
					'title'           => 'Michael Clarke',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'Founder', 'quality' ),
					'open_new_tab' => 'no',
					'id'              => 'customizer_repeater_56d7ea7f40c56',
				),
				array(
					'image_url'  => get_template_directory_uri().'/images/team/team2.jpg',
					'title'           => 'Paul Walker',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'UI Designer', 'quality' ),
					'open_new_tab' => 'no',
					'id'              => 'customizer_repeater_56d7ea7f40c66',
				),
				array(
					'image_url'  => get_template_directory_uri().'/images/team/team3.jpg',
					'title'           => 'Chris Gayle',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'Php Developer', 'quality' ),
					'open_new_tab' => 'no',
					'id'              => 'customizer_repeater_56d7ea7f40c76',
				),
				array(
					'image_url'  => get_template_directory_uri().'/images/team/team4.jpg',
					'title'           => 'Laura Michelle',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'Designer', 'quality' ),
					'open_new_tab' => 'no',
					'id'        => 'customizer_repeater_56d7ea7f40c86',
				),
				
				
				
				) );

			}
			
		}
}
endif;


// Rambo testimonial content data
if ( ! function_exists( 'rambo_testy_default_customize_register' ) ) :
add_action( 'customize_register', 'rambo_testy_default_customize_register' );
function rambo_testy_default_customize_register( $wp_customize ){
	
	$rambo_pro_theme_options = get_option('rambo_pro_theme_options');

   // if(get_option('rambo_pro_theme_options')!='' && isset($rambo_pro_theme_options['home_client_title'])){

		//$rambo_testimonial_content_control = $wp_customize->get_setting( 'rambo_testimonial_content' );
		
		$ThemeData = get_option('rambo_pro_theme_options');

		if (!empty($ThemeData['slider_category']))
		{

			// Run this code section if user comes form pro old.
			$rambo_testimonial_content_control = $wp_customize->get_setting( 'rambo_pro_theme_options[rambo_testy_content]' );
			if ( ! empty( $rambo_testimonial_content_control ) )
			{
			$count_posts = wp_count_posts( 'rambopro_testimonial')->publish;
			$arg = array( 'post_type' => 'rambopro_testimonial','posts_per_page' =>$count_posts);
			$testimonial = new WP_Query( $arg );
			$i=1;
			if($testimonial->have_posts())
			{	

				while ( $testimonial->have_posts() ) : $testimonial->the_post();	
			
				$pro_testimonial_data_old[] = array(
				
					'title'      => get_the_title(),
					'subtitle'   => get_post_meta( get_the_ID(), 'testimonial_designation', true),
					'text'       => get_post_meta( get_the_ID(), 'testimonial_role', true ),
					'link'       => get_post_meta( get_the_ID(), 'testimonial_link', true ),
					'image_url'  => get_the_post_thumbnail_url(),
					'open_new_tab' => get_post_meta( get_the_ID(), 'meta_testimonial_target', true ),
					'id'    => 'customizer_repeater_56d7ea7f40t754',
					);
				endwhile;	
			$rambo_testimonial_content_control->default = json_encode($pro_testimonial_data_old);
						
			}
		
		}}
		else{
	
				//Rambo default testimonial data.
				$rambo_testimonial_content_control = $wp_customize->get_setting( 'rambo_pro_theme_options[rambo_testy_content]' );
				if ( ! empty( $rambo_testimonial_content_control ) ) 
				{
				$rambo_testimonial_content_control->default = json_encode( array(
					array(
					'image_url'  => get_template_directory_uri().'/images/testimonial/1.png',
					'title'           => 'Williamson',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'Founder', 'quality' ),
					'open_new_tab' => 'no',
					'id'              => 'customizer_repeater_56d7ea7f40c56',
				),
				array(
					'image_url'  => get_template_directory_uri().'/images/testimonial/2.png',
					'title'           => 'Paul Walker',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'UI Designer', 'quality' ),
					'open_new_tab' => 'no',
					'id'              => 'customizer_repeater_56d7ea7f40c66',
				),
				array(
					'image_url'  => get_template_directory_uri().'/images/testimonial/3.png',
					'title'           => 'Kristina',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'Php Developer', 'quality' ),
					'open_new_tab' => 'no',
					'id'              => 'customizer_repeater_56d7ea7f40c76',
				),
				array(
					'image_url'  => get_template_directory_uri().'/images/testimonial/4.png',
					'title'           => 'Laura Michelle',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'Designer', 'quality' ),
					'open_new_tab' => 'no',
					'id'        => 'customizer_repeater_56d7ea7f40c86',
				),
				
				
				
				) );

			}
			
		}
}
endif;

// Rambo Testimonial content data
if ( ! function_exists( 'rambo_testimonial_default_customize_register' ) ) :
add_action( 'customize_register', 'rambo_testimonial_default_customize_register' );
function rambo_testimonial_default_customize_register( $wp_customize ){
	
	
		$ThemeData = get_option('rambo_pro_theme_options');

		if (!empty($ThemeData['slider_category']))
		{
			
			// Run this code section if user comes form pro old.
			$rambo_testimonial_content_control = $wp_customize->get_setting( 'rambo_pro_theme_options[rambo_testimonial_content]' );
				if (!empty( $rambo_testimonial_content_control ) ) 
				{
					$query_args = array(
				'ignore_sticky_posts' => 1, 
				'tax_query' => array( array( 'taxonomy' => 'post_format', 'field' => 'slug','terms' => array( 'post-format-quote' ) ))
				);
					$testi_loop = new WP_Query($query_args); 
					$i=1;
					if($testi_loop->have_posts())
			        {
					while ( $testi_loop->have_posts() ) : $testi_loop->the_post();	
					
						$pro_testimonial_old[] = array(
							'title'      => get_the_title(),
							'subtitle'   => get_post_meta( get_the_ID(), 'testimonial_designation', true),
							'text'       => get_the_content(),
							'link'       => '#',
							'image_url'  => get_the_post_thumbnail_url(),
							'open_new_tab' => 'no',
							'id'    => 'customizer_repeater_56d7ea7f40t754',
							);
						endwhile;
				//print_r($pro_testimonial_old);						
					$rambo_testimonial_content_control->default = json_encode($pro_testimonial_old);
					}	
			}
		
		}else{
				//Rambo default testimonial data.
				$rambo_testimonial_content_control = $wp_customize->get_setting( 'rambo_pro_theme_options[rambo_testimonial_content]' );
				if ( ! empty( $rambo_testimonial_content_control ) ) 
				{
				$rambo_testimonial_content_control->default = json_encode( array(
					array(
					'title'      => 'Natalie Doe',
					'text'       => 'Sed ut Perspi ciatis Unde Omnis Iste Sed ut perspi ciatis unde omnis iste natu error sit volup, tatem accu tium neque ferme ntum veposu miten a tempor nise. Consec tetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.',
					'subtitle' => __('Founder','spicepress'),
					'link'       => '#',
					'image_url'  => get_template_directory_uri().'/images/default/testi/testimonial1.png',
					'open_new_tab' => 'no',
					'id'         => 'customizer_repeater_56d7ea7f40b96',
					
					),
					array(
					'title'      => 'Bella Swan',
					'text'       => 'Sed ut Perspiciatis Unde Omnis Iste Sed ut perspiciatis unde omnis iste natu error sit voluptatem accu tium neque fermentum veposu miten a tempor nise. Duis autem vel eum iriure dolor in hendrerit in vulputate velit consequat reprehender in voluptate velit esse cillum duis dolor fugiat nulla pariatur.',
					'subtitle' => __('Manager','spicepress'),
					'link'       => '#',
					'image_url'  => get_template_directory_uri().'/images/default/testi/testimonial2.png',
					'open_new_tab' => 'no',
					'id'         => 'customizer_repeater_56d7ea7f40b97',
					),
					array(
					'title'      => 'Jenifer Doe',
					'text'       => 'Sed ut Perspiciatis Unde Omnis Iste Sed ut perspiciatis unde omnis iste natu error sit voluptatem accu tium neque fermentum veposu miten a tempor nise. Duis autem vel eum iriure dolor in hendrerit in vulputate velit consequat reprehender in voluptate velit esse cillum duis dolor fugiat nulla pariatur.',
					'subtitle' => __('Designer','spicepress'),
					'link'       => '#',
					'image_url'  => get_template_directory_uri().'/images/default/testi/testimonial3.png',
					'id'         => 'customizer_repeater_56d7ea7f40b98',
					'open_new_tab' => 'no',
					),
					
					
					
				) );

			}
		}
			
		}
endif;


//Client section
if ( ! function_exists( 'rambo_client_default_customize_register' ) ) :
add_action( 'customize_register', 'rambo_client_default_customize_register' );
function rambo_client_default_customize_register( $wp_customize ){
	
	$ThemeData = get_option('rambo_pro_theme_options');

		if (!empty($ThemeData['slider_category']))
		{ // Run this code if user come from pro old theme
		$rambo_client_content_control = $wp_customize->get_setting( 'rambo_pro_theme_options[rambo_clients_content]' );
			if ( ! empty( $rambo_client_content_control ) )
			{
			$count_posts = wp_count_posts( 'rambopro_clientstrip')->publish;
				$args = array( 'post_type' => 'rambopro_clientstrip','posts_per_page' =>$count_posts);
				$clientstrip = new WP_Query( $args ); 
				
				if( $clientstrip->have_posts() )
					{
						while ( $clientstrip->have_posts() ) : $clientstrip->the_post();
						
						$pro_clientstrip_data_old[] = array(
						'title'      => get_the_title(),
						'text'       => get_post_meta( get_the_ID(), 'description_meta_save', true ),
						'link'       => get_post_meta( get_the_ID(), 'clientstrip_link', true ),
						'image_url'  => get_the_post_thumbnail_url(),
						'open_new_tab' => get_post_meta( get_the_ID(), 'meta_client_target', true ),
						'id'    => 'customizer_repeater_56d7ea7f40b96',
							);
					
						endwhile;  
						$rambo_client_content_control->default = json_encode($pro_clientstrip_data_old);
					}
			
		}}
		else{
                //Rambo default client data.
                $rambo_client_content_control = $wp_customize->get_setting( 'rambo_pro_theme_options[rambo_clients_content]' );
                if ( ! empty( $rambo_client_content_control ) ) 
                {
                $rambo_client_content_control->default = json_encode( array(
                    array(
                    
                    'link'       => '#',
                    'image_url'  => get_template_directory_uri().'/images/clients/logo1.png',
                    'open_new_tab' => 'no',
                    'id'         => 'customizer_repeater_56d7ea7f40b96',
                    ),
                    
                    array(
                    
                    'link'       => '#',
                    'image_url'  => get_template_directory_uri().'/images/clients/logo2.png',
                    'open_new_tab' => 'no',
                    'id'         => 'customizer_repeater_56d7ea7f40b97',
                    ),
                    
                    array(
                    
                    'link'       => '#',
                    'image_url'  => get_template_directory_uri().'/images/clients/logo3.png',
                    'open_new_tab' => 'no',
                    'id'         => 'customizer_repeater_56d7ea7f40b98',
                    
                    ),
                    
                    array(
                    
                    'link'       => '#',
                    'image_url'  => get_template_directory_uri().'/images/clients/logo4.png',
                    'open_new_tab' => 'no',
                    'id'         => 'customizer_repeater_56d7ea7f40b99',
                    
                    ),
                    
                    array(
                    
                    'link'       => '#',
                    'image_url'  => get_template_directory_uri().'/images/clients/logo5.png',
                    'open_new_tab' => 'no',
                    'id'         => 'customizer_repeater_56d7ea7f40b100',
                    
                    ),
                    
                    array(
                    
                    'link'       => '#',
                    'image_url'  => get_template_directory_uri().'/images/clients/logo6.png',
                    'open_new_tab' => 'no',
                    'id'         => 'customizer_repeater_56d7ea7f40b101',
                    
                    ),
                    
                    array(
                    
                    'link'       => '#',
                    'image_url'  => get_template_directory_uri().'/images/clients/logo7.png',
                    'open_new_tab' => 'no',
                    'id'         => 'customizer_repeater_56d7ea7f40b102',
                    
                    ),
                
                ) );
			}
            }
}
endif;
