<?php
function rambo_portfolio_page_customizer( $wp_customize ) {

//Portfolio setting
$wp_customize->add_section(
        'project_category_settings',
        array(
            'title' => __('Portfolio single category layout','rambo'),
            'description' => '',
			'panel'  => 'rambo_template',
			'priority'   => 5,)
    );
	 
	
	 
$wp_customize->add_setting(
    'rambo_pro_theme_options[taxonomy_portfolio_list]',
    array(
       'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
		'default' => 2,
		)
	);	
	$wp_customize->add_control( 'rambo_pro_theme_options[taxonomy_portfolio_list]',array(
	 'type' => 'select',
	 'label'   => __('Select column layout','rambo'),
    'section' => 'project_category_settings',
	 'choices' => array(2=>2,3=>3,4=>4),
		)
	);
	
	

	$wp_customize->add_setting(
		'project',
		array(
			'default' => '',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
		)	
	);
	$wp_customize->add_control( new WP_project_Customize_Control( $wp_customize, 'project', array(	
			'section' => 'project_category_settings',
		))
	);


}
add_action( 'customize_register', 'rambo_portfolio_page_customizer' );