<?php
function rambo_testimonial_section_customizer( $wp_customize ) {
		//Site Intro Section
		$wp_customize->add_section( 'testimonial_settings' , array(
		'title'      => __('Testimonial settings', 'rambo'),
		'panel'  => 'section_settings',
		'priority'   => 6,
		) );
	
			// Enable testimonial section
			$wp_customize->add_setting('rambo_pro_theme_options[testimonial_section_enable]',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('rambo_pro_theme_options[testimonial_section_enable]',array(
			'label' => __('Hide testimonial section','rambo'),
			'section' => 'testimonial_settings',
			'type' => 'checkbox',
			) );
			
			// testimonial title
			$wp_customize->add_setting('rambo_pro_theme_options[testimonial_section_title]',array(
			'default' => __('What Clients Are Say','rambo'),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('rambo_pro_theme_options[testimonial_section_title]',array(
			'label' => __('Title','rambo'),
			'section' => 'testimonial_settings',
			'type' => 'text',
			) );
			
			// testimonial description
			$wp_customize->add_setting('rambo_pro_theme_options[testimonial_section_desc]',array(
			'default' => 'Maecenas sit amet tincidunt elit. Pellentesque habitant morbi tristique senectus et netus et Nulla facilisi.',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('rambo_pro_theme_options[testimonial_section_desc]',array(
			'label' => __('Description','rambo'),
			'section' => 'testimonial_settings',
			'type' => 'textarea',
			) );

			//testimonial Design
			$wp_customize->add_setting(
    		'rambo_pro_theme_options[home_testimonial_layout_section]',
    		array(
        	'default' => 1,
			'type' => 'option',
			'sanitize_callback' => 'sanitize_text_field',
    		));

			$wp_customize->add_control(
    		'rambo_pro_theme_options[home_testimonial_layout_section]',
    		array(
        	'type' => 'select',
        	'label' => __('Select Testimonial Design','rambo'),
        	'section' => 'testimonial_settings',
		 	'choices' => array(1=>__('Design 1', 'rambo'), 2=>__('Design 2', 'rambo'), 3=>__('Design 3', 'rambo')),
			));

			//testimonial Column Layout
			$wp_customize->add_setting(
    		'rambo_pro_theme_options[testimonial_col_layout]',
    		array(
       			'default' => 6,
				'type' => 'option',
				'sanitize_callback' => 'sanitize_text_field',
		
    			));

			$wp_customize->add_control(
    		'rambo_pro_theme_options[testimonial_col_layout]',
    		array(
        		'type' => 'select',
        		'label' => __('Select Column Layout','appointment'),
       			 'section' => 'testimonial_settings',
		 		'choices' => array(6=>__('2 Column', 'appointment'), 4=>__('3 Column', 'appointment'), 3=>__('4 Column', 'appointment')),
			));


			// testimonial Background image
    		$wp_customize->add_setting( 'rambo_pro_theme_options[testi_background]', array(
			'default' => get_template_directory_uri() . '/images/testimonial/bg.png',
			'sanitize_callback' => 'esc_url_raw',
			'type' => 'option',
    		) );
    
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'rambo_pro_theme_options[testi_background]', array(
      'label'    => __( 'Background Image', 'appointment' ),
      'section'  => 'testimonial_settings',
      'settings' => 'rambo_pro_theme_options[testi_background]',
    ) ) );
			
			if ( class_exists( 'Rambo_Repeater' ) ) {
			$wp_customize->add_setting(
				'rambo_pro_theme_options[rambo_testy_content]', array(
				'type'=> 'option',
				)
			);

			$wp_customize->add_control(
				new Rambo_Repeater(
					$wp_customize, 'rambo_pro_theme_options[rambo_testy_content]', array(
						'label'                            => esc_html__( 'Testimonial content', 'rambo' ),
						'section'                          => 'testimonial_settings',
						'priority'                         => 15,
						'add_field_label'                  => esc_html__( 'Add new Testimonial Member', 'rambo' ),
						'item_name'                        => esc_html__( 'Testimonial Member', 'rambo' ),
						'customizer_repeater_image_control' => true,
						'customizer_repeater_title_control' => true,
						'customizer_repeater_text_control'  => true,
						'customizer_repeater_subtitle_control' => true,
						'customizer_repeater_link_control' => true,
						'customizer_repeater_checkbox_control' => true,
						
					)
				)
			);
	 
	 }		
			
}
add_action( 'customize_register', 'rambo_testimonial_section_customizer' );

/**
 * Add selective refresh for Front page section section controls.
 */
function rambo_pro_register_home_testimonial_section_partials( $wp_customize ){

$wp_customize->selective_refresh->add_partial( 'rambo_pro_theme_options[testimonial_section_title]', array(
		'selector'            => '.home-testy .featured_port_title h1',
		'settings'            => 'rambo_pro_theme_options[testimonial_section_title]',
	
	) );
	
$wp_customize->selective_refresh->add_partial( 'rambo_pro_theme_options[testimonial_section_desc]', array(
		'selector'            => '.home-testy .featured_port_title p',
		'settings'            => 'rambo_pro_theme_options[testimonial_section_desc]',
	
	) );	
}

add_action( 'customize_register', 'rambo_pro_register_home_testimonial_section_partials' );
?>