<?php
function rambo_team_section_customizer( $wp_customize ) {
		//Site Intro Section
		$wp_customize->add_section( 'team_settings' , array(
		'title'      => __('Team settings', 'rambo'),
		'panel'  => 'section_settings',
		'priority'   => 6,
		) );
	
			// Enable Team section
			$wp_customize->add_setting('rambo_pro_theme_options[team_section_enable]',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('rambo_pro_theme_options[team_section_enable]',array(
			'label' => __('Hide team section','rambo'),
			'section' => 'team_settings',
			'type' => 'checkbox',
			) );
			
			
			/*$wp_customize->add_section( 'team_section' , array(
			'title'      => __('Section Header', 'rambo'),
			'panel'  => 'team_panel',
			'priority'   => 2,
			) );*/
			
			// Team title
			$wp_customize->add_setting('rambo_pro_theme_options[team_section_title]',array(
			'default' => __('Our Team','rambo'),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'rambo_additional_one_sanitize_html',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('rambo_pro_theme_options[team_section_title]',array(
			'label' => __('Title','rambo'),
			'section' => 'team_settings',
			'type' => 'text',
			) );
			
			// Team description
			$wp_customize->add_setting('rambo_pro_theme_options[team_section_desc]',array(
			'default' => 'Maecenas sit amet tincidunt elit. Pellentesque habitant morbi tristique senectus et netus et Nulla facilisi.',
			'sanitize_callback' => 'rambo_additional_one_sanitize_html',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('rambo_pro_theme_options[team_section_desc]',array(
			'label' => __('Description','rambo'),
			'section' => 'team_settings',
			'type' => 'textarea',
			) );

			//Team Design
			$wp_customize->add_setting(
    		'rambo_pro_theme_options[home_team_layout_section]',
    		array(
        	'default' => 1,
			'type' => 'option',
			'sanitize_callback' => 'sanitize_text_field',
    		));

			$wp_customize->add_control(
    		'rambo_pro_theme_options[home_team_layout_section]',
    		array(
        	'type' => 'select',
        	'label' => __('Select Team Design','rambo'),
        	'section' => 'team_settings',
		 	'choices' => array(1=>__('Design 1', 'rambo'), 2=>__('Design 2', 'rambo'), 3=>__('Design 3', 'rambo'), 4=>__('Design 4', 'rambo'), 5=>__('Design 5', 'rambo')),
			));

			//Team Column Layout
			$wp_customize->add_setting(
    		'rambo_pro_theme_options[team_col_layout]',
    		array(
       			'default' => 4,
				'type' => 'option',
				'sanitize_callback' => 'sanitize_text_field',
		
    			));

			$wp_customize->add_control(
    		'rambo_pro_theme_options[team_col_layout]',
    		array(
        		'type' => 'select',
        		'label' => __('Select Column Layout','appointment'),
       			 'section' => 'team_settings',
		 		'choices' => array(6=>__('2 Column', 'appointment'), 4=>__('3 Column', 'appointment'), 3=>__('4 Column', 'appointment')),
			));
			
			if ( class_exists( 'Rambo_Repeater' ) ) {
			$wp_customize->add_setting(
				'rambo_pro_theme_options[rambo_team_content]', array(
				'type'=> 'option',
				)
			);

			$wp_customize->add_control(
				new Rambo_Repeater(
					$wp_customize, 'rambo_pro_theme_options[rambo_team_content]', array(
						'label'                            => esc_html__( 'Team content', 'rambo' ),
						'section'                          => 'team_settings',
						'priority'                         => 15,
						'add_field_label'                  => esc_html__( 'Add new Team Member', 'rambo' ),
						'item_name'                        => esc_html__( 'Team Member', 'rambo' ),
						'customizer_repeater_image_control' => true,
						'customizer_repeater_title_control' => true,
						'customizer_repeater_text_control'  => true,
						'customizer_repeater_subtitle_control' => true,
						'customizer_repeater_link_control' => true,
						'customizer_repeater_checkbox_control' => true,
						
					)
				)
			);
	 
	 }
			
			
			function rambo_additional_one_sanitize_html( $input ) {
				return force_balance_tags( $input );
			}
			
			
}
add_action( 'customize_register', 'rambo_team_section_customizer' );

/**
 * Add selective refresh for Front page section section controls.
 */
function rambo_pro_register_home_team_section_partials( $wp_customize ){

$wp_customize->selective_refresh->add_partial( 'rambo_pro_theme_options[team_section_title]', array(
		'selector'            => '.additional_section_one .featured_port_title h1',
		'settings'            => 'rambo_pro_theme_options[team_section_title]',
	
	) );
	
$wp_customize->selective_refresh->add_partial( 'rambo_pro_theme_options[team_section_desc]', array(
		'selector'            => '.additional_section_one .featured_port_title p',
		'settings'            => 'rambo_pro_theme_options[team_section_desc]',
	
	) );

/*$wp_customize->selective_refresh->add_partial( 'rambo_pro_theme_options[rambo_team_content]', array(
		'selector'            => '.additional_section_one .additional-area-one',
		'settings'            => 'rambo_pro_theme_options[rambo_team_content]',
	
	) );*/
	
}

add_action( 'customize_register', 'rambo_pro_register_home_team_section_partials' );
?>