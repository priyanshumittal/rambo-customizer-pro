<?php
function rambo_project_customizer( $wp_customize ) {
	
	
	class Taxonomy_Dropdown_Custom_Control extends WP_Customize_Control
    {
	private $options = false;

    public function __construct($manager, $id, $args = array(), $options = array())
    {
        $this->options = $options;

        parent::__construct( $manager, $id, $args );
    }

	/**
	 * Render the control's content.
	 *
	 * Allows the content to be overriden without having to rewrite the wrapper.
	 *
	 * @since   11/14/2012
	 * @return  void
	 */
	public function render_content()
    {
        // call wp_dropdown_cats to get data and add to select field
        add_action( 'wp_dropdown_cats', array( $this, 'wp_dropdown_cats' ) );

		// Set defaults
		$this->defaults = array(
			'show_option_none' => __( 'None','rambo' ),
			'orderby'          => 'name',
			'hide_empty'       => 0,
			'id'               => $this->id,
			'selected'         => $this->value(),
			'taxonomy'		   => 'portfolio_categories'
		);

		?>
		<label>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			    <?php //print_r($cats); ?>
				<?php wp_dropdown_categories(Array ( 'show_option_none' => 'None','orderby' => 'name', 'hide_empty'=> 0 ,'id' => 'tax_project', 'taxonomy'=> 'portfolio_categories' )); ?>
		</label>
		<?php
	}
    /**
     * Replace WP default dropdown
     *
     * @since   11/14/2012
     * @return  String $output
     */
    public function wp_dropdown_cats( $output )
    {
	   
	  $output = str_replace( '<select', '<select multiple ' . $this->get_link(), $output );
	  return $output;
    }
}

			$wp_customize->add_section( 'project_settings' , array(
				'title'      => __('Project settings', 'rambo'),
				'panel'  => 'section_settings',
				'priority'   => 4,
				) );
			
			// enable project section
			$wp_customize->add_setting('rambo_pro_theme_options[project_protfolio_enabled]',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('rambo_pro_theme_options[project_protfolio_enabled]',array(
			'label' => __('Hide project section','rambo'),
			'section' => 'project_settings',
			'type' => 'checkbox',
			) );
			
		
		//Portfolio setting
		/*$wp_customize->add_section( 'project_section_settings' , array(
				'title'      => __('Section Header','rambo'),
				'panel'  => 'project_panel',
				'priority'   => 3,
				) );*/
	
				//Project Title
				$wp_customize->add_setting(
				'rambo_pro_theme_options[project_protfolio_tag_line]',
				array(
					'default' => __('Featured Portfolio Projects','rambo'),
					'capability'     => 'edit_theme_options',
					'sanitize_callback' => 'rambo_project_sanitize_html',
					'type' => 'option',
					)
				);	
				$wp_customize->add_control('rambo_pro_theme_options[project_protfolio_tag_line]',array(
				'label'   => __('Title','rambo'),
				'section' => 'project_settings',
				 'type' => 'text',)  );	
				 
				//Project Description 
				 $wp_customize->add_setting(
				'rambo_pro_theme_options[project_protfolio_description_tag]',
				array(
					'default' => 'Maecenas sit amet tincidunt elit. Pellentesque habitant morbi tristique senectus et netus et Nulla facilisi.

',
					'capability'     => 'edit_theme_options',
					'sanitize_callback' => 'rambo_project_sanitize_html',
					'type' => 'option',
					)
				);	
				$wp_customize->add_control( 'rambo_pro_theme_options[project_protfolio_description_tag]',array(
				'label'   => __('Description','rambo'),
				'section' => 'project_settings',
				 'type' => 'textarea',)  );
				 
			$wp_customize->add_setting(
			 'rambo_pro_theme_options[portfolio_selected_category_id]',
			array(
				'default' => 2,
			   'capability'     => 'edit_theme_options',
			   'type' => 'option',
				)
			);	
			$wp_customize->add_control( new Taxonomy_Dropdown_Custom_Control( $wp_customize, 'rambo_pro_theme_options[portfolio_selected_category_id]', array(
					'label'   => __('Select category for projects','rambo'),
					'section' => 'project_settings',
					'settings'   => 'rambo_pro_theme_options[portfolio_selected_category_id]',
					'sanitize_callback' => 'sanitize_text_field',
			) ) ); 
				 
				 
				 
				 
	 
			 // Project column layout
					$wp_customize->add_setting('rambo_pro_theme_options[project_column_layout]',array(
					'default' => 4,
					'type' => 'option',
					'sanitize_callback' => 'sanitize_text_field',
					) );

					$wp_customize->add_control('rambo_pro_theme_options[project_column_layout]',array(
					'type' => 'select',
					'label' => __('Select column layout','rambo'),
					'section' => 'project_settings',
					'choices' => array(1=>'1',2=> '2',3=>'3',4=>'4'),
					) );
		
	
				//link
					class WP_project_Customize_Control extends WP_Customize_Control {
					public $type = 'new_menu';
					/**
					* Render the control's content.
					*/
					public function render_content() {
					?>
					<a href="<?php bloginfo ( 'url' );?>/wp-admin/edit.php?post_type=rambopro_project" class="button"  target="_blank"><?php _e( 'Click here to add project', 'rambo' ); ?></a>
					<?php
					}
				}
				
				
			
			
				$wp_customize->add_setting(
					'project',
					array(
						'default' => '',
						'capability'     => 'edit_theme_options',
						'sanitize_callback' => 'sanitize_text_field',
					)	
				);
				$wp_customize->add_control( new WP_project_Customize_Control( $wp_customize, 'project', array(	
						'section' => 'project_settings',
						
					))
				);
				
				

				function rambo_project_sanitize_html( $input ) {
					return force_balance_tags( $input );
				}

}
add_action( 'customize_register', 'rambo_project_customizer' );

/**
 * Add selective refresh for Front page section section controls.
 */
function rambo_pro_register_home_project_section_partials( $wp_customize ){

$wp_customize->selective_refresh->add_partial( 'rambo_pro_theme_options[project_protfolio_tag_line]', array(
		'selector'            => '.portfolio_main_content .featured_port_title h1',
		'settings'            => 'rambo_pro_theme_options[project_protfolio_tag_line]',
	
	) );
	
$wp_customize->selective_refresh->add_partial( 'rambo_pro_theme_options[project_protfolio_description_tag]', array(
		'selector'            => '.portfolio_main_content .featured_port_title p',
		'settings'            => 'rambo_pro_theme_options[project_protfolio_description_tag]',
	
	) );

}

add_action( 'customize_register', 'rambo_pro_register_home_project_section_partials' );
?>