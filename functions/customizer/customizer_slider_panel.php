<?php
$repeater_path = trailingslashit( get_template_directory() ) . '/functions/customizer-repeater/functions.php';
if ( file_exists( $repeater_path ) ) {
require_once( $repeater_path );
}
function rambo_home_slider_customizer($wp_customize){
	
	if(!class_exists('WP_Customize_Control'))
	return null;

	$wp_customize->add_panel( 'section_settings', array(
		'priority'       => 126,
		'capability'     => 'edit_theme_options',
		'title'      => __('Homepage section settings', 'rambo'),
	) );
		
	/* Header Section */
	$wp_customize->add_section( 'slider_setting', array(
		'priority'   => 1,
		'title'      => __('Slider settings', 'rambo'),
		'panel'  => 'section_settings',
	) );
	
			//Hide slider
			
			$wp_customize->add_setting(
			'rambo_pro_theme_options[home_slider_enabled]',
			array(
				'default' => true,
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
				'type' => 'option',
			)	
			);
			$wp_customize->add_control(
			'rambo_pro_theme_options[home_slider_enabled]',
			array(
				'label' => __('Enable home slider','rambo'),
				'section' => 'slider_setting',
				'type' => 'checkbox',
				'description' => __('Enable slider on front page.','rambo'),
			));
			
			if ( class_exists( 'Rambo_Repeater' ) ) {
			$wp_customize->add_setting( 'rambo_pro_theme_options[rambo_slider_content]', array(
			'type'=> 'option',
			) );

			$wp_customize->add_control( new Rambo_Repeater( $wp_customize, 'rambo_pro_theme_options[rambo_slider_content]', array(
				'label'                             => esc_html__( 'Slider content', 'rambo' ),
				'section'                           => 'slider_setting',
				'priority'                          => 10,
				'add_field_label'                   => esc_html__( 'Add new Slider', 'rambo' ),
				'item_name'                         => esc_html__( 'Slider', 'rambo' ),
				'customizer_repeater_title_control' => true,
				'customizer_repeater_text_control'  => true,
				'customizer_repeater_image_control' => true,
				'customizer_repeater_link_control'	=> true,
				'customizer_repeater_button_text_control' => true,
				'customizer_repeater_checkbox_control' => true,
				) ) );
		}
	
			//Slider animation
			
			$wp_customize->add_setting(
			'rambo_pro_theme_options[slider_options]',
			array(
				'default' => __('slide','rambo'),
				'type' => 'option',
				'sanitize_callback' => 'sanitize_text_field',
				
			)
			);

			$wp_customize->add_control(
			'rambo_pro_theme_options[slider_options]',
			array(
				'type' => 'select',
				'label' => __('Select slider animation','rambo'),
				'section' => 'slider_setting',
				 'choices' => array('slide'=>__('slide', 'rambo'), 'carousel-fade'=>__('fade', 'rambo')),
				));
		
		
			//Slider Animation duration

			$wp_customize->add_setting(
			'rambo_pro_theme_options[slider_transition_delay]',
			array(
				'default' =>4500,
				'type' => 'option',
				'sanitize_callback' => 'sanitize_text_field',
				
			)
			);

			$wp_customize->add_control(
			'rambo_pro_theme_options[slider_transition_delay]',
			array(
				'type' => 'select',
				'label' => __('Duration','rambo'),
				'section' => 'slider_setting',
				 'choices' => array(500=>500, 1000=>1000, 1500=>1500, 2000=>2000, 2500=>2500, 3000=>3000,3500=>3500,4000=>4000, 4500=>4500, 5000=>5000,5500=>5500,6000=>6000,6500=>6500,7000=>7000,7500=>7500,8000=>8000,8500=>8500,9000=>9000,9500=>9500,10000=>10000),
				));
			
			
}
add_action( 'customize_register', 'rambo_home_slider_customizer' );

/**
 * Add selective refresh for Front page section section controls.
 */
function rambo_pro_register_slider_section_partials( $wp_customize ){

$wp_customize->selective_refresh->add_partial( 'rambo_pro_theme_options[rambo_slider_content]', array(
		'selector'            => '.main_slider #slider-carousel .active .slider_con h2',
		'settings'            => 'rambo_pro_theme_options[rambo_slider_content]',
	
	) );
	
}

add_action( 'customize_register', 'rambo_pro_register_slider_section_partials' );
?>