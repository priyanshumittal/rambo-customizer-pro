<?php
/**
 * Register testimonial section widget
 *
 */
add_action('widgets_init','wbr_testimonial_section_widget');
function wbr_testimonial_section_widget(){
	
	return register_widget('wbr_testimonial_section_widget');
}

class wbr_testimonial_section_widget extends WP_Widget{
	
	function __construct() {
		parent::__construct(
			'wbr_testimonial_section_widget', // Base ID
			__('WBR : Testimonial Slider Widget','rambo'), // Name
			array( 'description' => __( 'Testimonial section widget','rambo' ), ) // Args
		);
	}
	
	public function widget( $args , $instance ) {
		$ExcId = array();
		
		$instance['column_layout'] = (isset($instance['column_layout'])?$instance['column_layout']:'1');
		$title=(isset($instance['title'])?$instance['title']:'');
		$instance['testimoanial_effect'] = (isset($instance['testimoanial_effect'])?$instance['testimoanial_effect']:'');
		$testimonial_column_layout = 12 / $instance['column_layout'];
		if($instance['exclude_posts']!=null){ $ExcId = explode(',',$instance['exclude_posts']);	}
		$testimonial_slide_class=(isset($instance['testimonial_slide_class'])?$instance['testimonial_slide_class']:'');
			if($testimonial_slide_class !='')
			{
				$args['before_widget'] = str_replace('class="', 'class="'. $testimonial_slide_class . ' ',$args['before_widget']);
				
			}
		echo $args['before_widget'];
		
		$query_args = array(
		'ignore_sticky_posts' => 1, 
		'tax_query' => array( array( 'taxonomy' => 'post_format', 'field' => 'slug','terms' => array( 'post-format-quote' ) )),
		'post__not_in' =>$ExcId
		);
		
		$testi_loop = new WP_Query($query_args); 
		
		
		?>
		<!-- Testimonial Section -->
		<div class="row testimonial_section">
		<?php if($title!='') { ?>
		<div class="span12"><div class="service_head_title"><h3><?php echo $title; ?></h3></div></div><?php } ?>
		<div id="testimonial-<?php echo $args['widget_id']; ?>" class="carousel slide <?php if($instance['testimoanial_effect'] == 'fade') { echo 'carousel-fade'; } ?>" data-ride="carousel" data-type="multi" >
			<div class="carousel-inner">
			<?php 
			$i=1;
			while ( $testi_loop->have_posts() ) : $testi_loop->the_post(); 
			?>
			
				<div class="item <?php if($i==1) { echo 'active';} $i++; ?>">		
					<div class="span<?php echo $testimonial_column_layout; ?> testimonial_area">
						<blockquote class="style1"><span><?php echo the_content(); ?></span></blockquote>
						<div class="testimonial_author">
							<?php if( has_post_thumbnail() ): ?><?php the_post_thumbnail( 'thumbnail', array( 'class' => 'img-responsive','style'=>'height:100%;' ) )?><?php endif; ?><span><?php the_title(); ?> <?php if( get_post_meta(get_the_ID(),'testimonial_designation', true ) ): ?>
								<br/><small><?php echo get_post_meta( get_the_ID(),'testimonial_designation', true); ?></small>
								<?php endif; ?></span>
						</div>
					</div>
				</div>
				<?php endwhile;	?>
			</div>
		</div>
	</div>
	<!-- /Testimonial & Features -->
	<script>
				jQuery(function($) {

					//	Testimonial Scroll Js	
					
					$('#testimonial-<?php echo $args['widget_id']; ?>').carousel({
					  interval: <?php echo $instance['testimoanial_speed']; ?>
					})

					<?php $testimonial_for_layout = $instance['column_layout'];
						    $test=5;
							
					  if($testimonial_for_layout == 4)
					  {
						$test =2;
						
						  
					  }
					  elseif($testimonial_for_layout == 3)
					  {
						$test =1;
						
						  
					  }
					   elseif($testimonial_for_layout == 2)
					  {
						$test =0;
						
					  }
					  
					  if($test<5)
					  { ?>
					  /*$('#testimonial-<?php echo $args['widget_id']; ?> .item').each(function(){
							
					  var next = $(this).next();
					  if (!next.length) {
						next = $(this).siblings(':first');
					  }
					  next.children(':first-child').clone().appendTo($(this));
					  // For Two Column Layout i=0
					  // For Three Column Layout i=1
					  
					  for (var i=0;i<<?php echo $test; ?>;i++) {
						next=next.next();
						if (!next.length) {
							next = $(this).siblings(':first');
						}
						
						next.children(':first-child').clone().appendTo($(this));
					  }
					}); */
						  
					 <?php  } ?>	
				});
	</script>		
			
		
		<?php 	
		echo $args['after_widget'];
	}
	
		public function form( $instance ) {
		$instance['title'] = ( ! empty( $instance['title'] ) ) ? $instance['title'] : '';
		$instance['column_layout'] = (isset($instance['column_layout'])?$instance['column_layout']:1);
		$instance['exclude_posts'] = (isset($instance['exclude_posts'])?$instance['exclude_posts']:'');
		$instance['testimoanial_effect'] = (isset($instance['testimoanial_effect'])?$instance['testimoanial_effect']:'slide');
		$instance['testimoanial_speed'] = (isset($instance['testimoanial_speed'])?$instance['testimoanial_speed']:2000);
		$instance['testimonial_slide_class'] = ( ! empty( $instance['testimonial_slide_class'] ) ) ? $instance['testimonial_slide_class'] : '';
		
		?>
		<h4 for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title','rambo' ); ?></h4>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php if($instance[ 'title' ]) echo esc_attr($instance[ 'title' ]);?>" />
		<p>
			<label for="<?php echo $this->get_field_id( 'column_layout' ); ?>"><?php _e('select column layout','rambo' ); ?></label> 
			
			<select id="<?php echo $this->get_field_id( 'column_layout' ); ?>" name="<?php echo $this->get_field_name( 'column_layout' ); ?>">
			<option value>--<?php echo __('select column layout','rambo'); ?>--</option>
				<option value="1" <?php echo ($instance['column_layout']=='1'?'selected':''); ?>>
				<?php echo '1'; ?></option>
				<option value="2" <?php echo ($instance['column_layout']=='2'?'selected':''); ?>>
				<?php echo '2'; ?></option>
				<option value="3" <?php echo ($instance['column_layout']=='3'?'selected':''); ?>>
				<?php echo '3'; ?></option>
				<option value="4" <?php echo ($instance['column_layout']=='4'?'selected':''); ?>>
				<?php echo '4'; ?></option>
			</select>	
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'exclude_posts' ); ?>"><?php _e( 'Exclude posts id like ( 1,2,3...etc )','rambo' ); ?></label> 
			<textarea rows="5" class="widefat" id="<?php echo $this->get_field_id( 'exclude_posts' ); ?>" name="<?php echo $this->get_field_name( 'exclude_posts' ); ?>"><?php if($instance['exclude_posts']) echo $instance['exclude_posts']; ?></textarea>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'testimoanial_effect' ); ?>"><?php _e( 'Animation type','rambo' ); ?></label><br/> 
			<select id="<?php echo $this->get_field_id( 'testimoanial_effect' ); ?>" name="<?php echo $this->get_field_name( 'testimoanial_effect' ); ?>">
				<option value>--<?php echo __('Select animation','rambo'); ?>--</option>
				<option value="slide" <?php echo ($instance['testimoanial_effect']=='slide'?'selected':''); ?>><?php echo __('slide','rambo'); ?></option>
				<option value="fade" <?php echo ($instance['testimoanial_effect']=='fade'?'selected':''); ?>><?php echo __('fade','rambo'); ?></option>
			</select>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'testimoanial_speed' ); ?>"><?php _e( 'Speed','rambo' ); ?></label><br/> 
			<select id="<?php echo $this->get_field_id( 'testimoanial_speed' ); ?>" name="<?php echo $this->get_field_name( 'testimoanial_speed' ); ?>">
				<option value>--<?php echo __('Slider speed','rambo'); ?>--</option>
				<option value="500" <?php echo ($instance['testimoanial_speed']==500?'selected':''); ?>>
				<?php echo '500'; ?></option>
				<option value="1000" <?php echo ($instance['testimoanial_speed']==1000?'selected':''); ?>>
				<?php echo '1000'; ?></option>
				<option value="1500" <?php echo ($instance['testimoanial_speed']==1500?'selected':''); ?>>
				<?php echo '1500'; ?></option>
				<option value="2000" <?php echo ($instance['testimoanial_speed']==2000?'selected':''); ?>>
				<?php echo '2000'; ?></option>
				<option value="2500" <?php echo ($instance['testimoanial_speed']==2500?'selected':''); ?>>
				<?php echo '2500'; ?></option>
				<option value="3000" <?php echo ($instance['testimoanial_speed']==3000?'selected':''); ?>>
				<?php echo '3000'; ?></option>
				<option value="3500" <?php echo ($instance['testimoanial_speed']==3500?'selected':''); ?>>
				<?php echo '3500'; ?></option>
				<option value="4000" <?php echo ($instance['testimoanial_speed']==4000?'selected':''); ?>>
				<?php echo '4000'; ?></option>
				<option value="4500" <?php echo ($instance['testimoanial_speed']==4500?'selected':''); ?>>
				<?php echo '4500'; ?></option>
				<option value="5500" <?php echo ($instance['testimoanial_speed']==5500?'selected':''); ?>>
				<?php echo '5500'; ?></option>
				<option value="6000" <?php echo ($instance['testimoanial_speed']==6000?'selected':''); ?>>
				<?php echo '6000'; ?></option>
				<option value="6500" <?php echo ($instance['testimoanial_speed']==6500?'selected':''); ?>>
				<?php echo '6500'; ?></option>
				<option value="7000" <?php echo ($instance['testimoanial_speed']==7000?'selected':''); ?>>
				<?php echo '7000'; ?></option>
				<option value="7500" <?php echo ($instance['testimoanial_speed']==7500?'selected':''); ?>>
				<?php echo '7500'; ?></option>
				<option value="8000" <?php echo ($instance['testimoanial_speed']==8000?'selected':''); ?>>
				<?php echo '8000'; ?></option>
				<option value="8500" <?php echo ($instance['testimoanial_speed']==8500?'selected':''); ?>>
				<?php echo '8500'; ?></option>
				<option value="9000" <?php echo ($instance['testimoanial_speed']==9000?'selected':''); ?>>
				<?php echo '9000'; ?></option>
				<option value="9500" <?php echo ($instance['testimoanial_speed']==9500?'selected':''); ?>>
				<?php echo '9500'; ?></option>
				<option value="10000" <?php echo ($instance['testimoanial_speed']==10000?'selected':''); ?>>
				<?php echo '10000'; ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'testimonial_slide_class' ); ?>"><?php _e('CSS Classes (optional)','rambo' ); ?></label> 
		</p>
		<input class="widefat" id="<?php echo $this->get_field_id( 'testimonial_slide_class' ); ?>" name="<?php echo $this->get_field_name( 'testimonial_slide_class' ); ?>" type="text" value="<?php if($instance[ 'testimonial_slide_class' ]) echo esc_attr($instance[ 'testimonial_slide_class' ]);?>" />
		
		<?php 
	}
	
	
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? $new_instance['title'] : '';
		$instance['exclude_posts'] = ( ! empty( $new_instance['exclude_posts'] ) ) ? $new_instance['exclude_posts'] : '';
		$instance['column_layout'] = ( ! empty( $new_instance['column_layout'] ) ) ? $new_instance['column_layout'] : '';
		$instance['testimoanial_effect'] = ( ! empty( $new_instance['testimoanial_effect'] ) ) ? strip_tags( $new_instance['testimoanial_effect'] ) : '';
		$instance['testimoanial_speed'] = ( ! empty( $new_instance['testimoanial_speed'] ) ) ? strip_tags( $new_instance['testimoanial_speed'] ) : '';
		$instance['testimonial_slide_class'] = ( ! empty( $new_instance['testimonial_slide_class'] ) ) ? strip_tags( $new_instance['testimonial_slide_class'] ) : '';
		
		return $instance;
	}
}