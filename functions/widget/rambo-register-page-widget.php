<?php
/**
 * Feature Page Widget
 *
 */
add_action('widgets_init','rambo_feature_page_widget');
function rambo_feature_page_widget(){
	
	return register_widget('rambo_feature_page_widget');
}

class rambo_feature_page_widget extends WP_Widget{
	
	function __construct() {
		parent::__construct(
			'rambo_feature_page_widget', // Base ID
			__('WBR : Page / Service Widget', 'rambo'), // Name
			array( 'description' => __( 'Feature Page item widget', 'rambo'), ) // Args
		);
	}
	
	
	public function widget( $args , $instance ) {
		
			$instance['selected_page'] = (isset($instance['selected_page'])?$instance['selected_page']:'');
			$instance['hide_image'] = (isset($instance['hide_image'])?$instance['hide_image']:'');
			$instance['above_title'] = (isset($instance['above_title'])?$instance['above_title']:'');
			$instance['icon'] = (isset($instance['icon'])?$instance['icon']:'');
			$instance['servicelink'] = (isset($instance['servicelink'])?$instance['servicelink']:'');
			$instance['target'] = (isset($instance['target'])?$instance['target']:'');
			$instance['buttontext'] = (isset($instance['buttontext'])?$instance['buttontext']:'');
			$selected_page_class=(isset($instance['selected_page_class'])?$instance['selected_page_class']:'');
			
			if($selected_page_class !='')
			{
				$args['before_widget'] = str_replace('class="', 'class="'. $selected_page_class . ' ',$args['before_widget']);
				
			}
			
			echo $args['before_widget'];
			
			
			// Check for custom link
			
			
			// fetch page object
			$page= get_post($instance['selected_page']);
			
			$title = $page->post_title;
			
	if($instance['selected_page']!=''):
				if( ($instance['above_title']) == true ):
			
					if($instance['servicelink']!=null){
								echo '<a href="' .$instance['servicelink'].'" '.($instance['target']==true?'target="_blank"':'').' >';
							}
			
				    echo '<h2 class="widget-title">'. $title .'</h2>'; 
					if($instance['servicelink']!=null){
								echo '</a>';
							}	 
							
			
			    endif;	
			
	endif;
				
				if(($instance['icon'])!=null){
							echo '<span class="fa-stack fa-4x icon_align_center">';
							if($instance['servicelink']!=null){
								echo '<a href="' .$instance['servicelink'].'" '.($instance['target']==true?'target="_blank"':'').' >';
							}
							echo '<i class="'. $instance['icon'] .' home_media_icon_1x fa-inverse"></i>';
							if($instance['servicelink']!=null){
								echo '</a>';
							}
							echo '</span>';
					
				}
				
	if($instance['selected_page']!=''):	
	
				if( $instance['hide_image'] != true ):
						if ( has_post_thumbnail($page->ID)) {
				        echo '<div class="featured-service-img">';
						if($instance['servicelink']!=null){
						echo '<a href="' .$instance['servicelink'].'" '.($instance['target']==true?'target="_blank"':'').' >';
						}
					        $attr = array( 'class' => 'img-responsive' );
						    echo get_the_post_thumbnail($page->ID, 'large', $attr);
							if($instance['servicelink']!=null){
							echo "</a>";
							}
				        echo '</div>';
						}
				endif;
					
			    if( ($instance['above_title']) != true ):
				if($instance['servicelink']!=null){
								echo '<a href="' .$instance['servicelink'].'" '.($instance['target']==true?'target="_blank"':'').' >';
							}
			
				    echo '<h2 class="widget-title">'. $title .'</h2>'; 
					if($instance['servicelink']!=null){
								echo '</a>';
							}		
			    endif;
				    

				
				    if($page->post_content) {echo '<p>'.$page->post_content. '</p>';}
					
					if(($instance['buttontext'] != null) && ($instance['servicelink']!= null))
					{
					?>
					<p><strong><a href="<?php echo $instance['servicelink']; ?>" <?php if( $instance['target'] == 1 ) { echo "target='_blank'"; } ?>><?php echo $instance['buttontext'] ?></a></strong></p>
						
					<?php }
					
					
					
	endif;			       			
		
		
			
			
		echo $args['after_widget']; 	
	}
	
	public function form( $instance ) {
		$instance['selected_page'] = isset($instance['selected_page']) ? $instance['selected_page'] : '';
		$instance['hide_image'] = isset($instance['hide_image']) ? $instance['hide_image'] : '';
		$instance['above_title'] = isset($instance['above_title']) ? $instance['above_title'] : '';
		$instance['icon'] = isset($instance['icon']) ? $instance['icon'] : '';
		$instance['servicelink'] = (isset($instance['servicelink'])?$instance['servicelink']:'');
		$instance['buttonlink'] = isset($instance['buttonlink']) ? $instance['buttonlink'] : '';
		$instance['buttontext'] = isset($instance['buttontext']) ? $instance['buttontext'] : '';
		$instance['target'] = (isset($instance['target'])?$instance['target']:'');
		$instance['selected_page_class'] = (isset($instance['selected_page_class'])? $instance['selected_page_class']: '');
		
		
		
		?>
	
	    <p>
		<label for="<?php echo $this->get_field_id( 'icon' ); ?>">
		<?php _e( 'Specify Font Awesome icon class ( like: fa fa-shield ). <a href="//fontawesome.io/icons/" target="blank" >Browse all icons </a>','rambo' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'icon' ); ?>" name="<?php echo $this->get_field_name( 'icon' ); ?>" type="text" value="<?php echo esc_attr( $instance['icon'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'selected_page' ); ?>"><?php _e('Select Pages','rambo' ); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'selected_page' ); ?>" name="<?php echo $this->get_field_name( 'selected_page' ); ?>">
			<option value>---<?php _e('Select','rambo');?>---</option>
				<?php
					$selected_page = $instance['selected_page'];
					$pages = get_pages($selected_page); 				
					foreach ( $pages as $page ) {
						$option = '<option value="' . $page->ID . '" ';
						$option .= ( $page->ID == $selected_page ) ? 'selected="selected"' : '';
						$option .= '>';
						$option .= $page->post_title;
						$option .= '</option>';
						echo $option;
					}
				?>
						
			</select>
			<br/>
			
		</p>
		
		<p>
		
		<label for="<?php echo $this->get_field_id( 'buttontext' ); ?>"><?php _e( 'Button Text','spasalon' ); ?></label> 
		
		<input class="widefat" id="<?php echo $this->get_field_id( 'buttontext' ); ?>" name="<?php echo $this->get_field_name( 'buttontext' ); ?>" type="text" value="<?php echo esc_attr( $instance['buttontext'] ); ?>" />
		
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'servicelink' ); ?>"><?php _e('Enter Service Link','rambo' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'servicelink' ); ?>" name="<?php echo $this->get_field_name( 'servicelink' ); ?>" type="text" value="<?php echo esc_attr( $instance['servicelink'] ); ?>" />
		</p>
		
		<p>
		<input class="checkbox" type="checkbox" <?php if($instance['target']==true){ echo 'checked'; } ?> id="<?php echo $this->get_field_id( 'target' ); ?>" name="<?php echo $this->get_field_name( 'target' ); ?>" /> 
		<label for="<?php echo $this->get_field_id( 'target' ); ?>"><?php _e( 'Open link in new tab','rambo' ); ?></label>
		</p>
		
		
		
		<p>
		<input class="checkbox" type="checkbox" <?php if($instance['hide_image']==true){ echo 'checked'; } ?> id="<?php echo $this->get_field_id( 'hide_image' ); ?>" name="<?php echo $this->get_field_name( 'hide_image' ); ?>" /> 
		<label for="<?php echo $this->get_field_id( 'hide_image' ); ?>"><?php _e('Hide Featured Image','rambo' ); ?></label>
		</p>
		<p>
		<input class="checkbox" type="checkbox" <?php if($instance['above_title']==true){ echo 'checked'; } ?> id="<?php echo $this->get_field_id( 'above_title' ); ?>" name="<?php echo $this->get_field_name( 'above_title' ); ?>" /> 
		<label for="<?php echo $this->get_field_id( 'above_title' ); ?>"><?php _e( 'Display image / icon above title','rambo' ); ?></label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'selected_page_class' ); ?>"><?php _e('CSS Classes (optional)','rambo' ); ?></label> 
		</p>
		<input class="widefat" id="<?php echo $this->get_field_id( 'selected_page_class' ); ?>" name="<?php echo $this->get_field_name( 'selected_page_class' ); ?>" type="text" value="<?php if($instance[ 'selected_page_class' ]) echo esc_attr($instance[ 'selected_page_class' ]);?>" />
		<?php 
	}
	
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['selected_page'] = ( ! empty( $new_instance['selected_page'] ) ) ? $new_instance['selected_page'] : '';
		$instance['hide_image'] = ( ! empty( $new_instance['hide_image'] ) ) ? $new_instance['hide_image'] : '';
		$instance['above_title'] = ( ! empty( $new_instance['above_title'] ) ) ? $new_instance['above_title'] : '';
		$instance['icon'] = ( ! empty( $new_instance['icon'] ) ) ? $new_instance['icon'] : '';
		$instance['servicelink'] = ( ! empty( $new_instance['servicelink'] ) ) ? $new_instance['servicelink'] : '';
		$instance['buttontext'] = ( ! empty( $new_instance['buttontext'] ) ) ? $new_instance['buttontext'] : '';
		$instance['target'] = ( ! empty( $new_instance['target'] ) ) ? $new_instance['target'] : '';
		$instance['selected_page_class'] = ( ! empty( $new_instance['selected_page_class'] ) ) ? $new_instance['selected_page_class'] : '';
		
		
		return $instance;
	}
}