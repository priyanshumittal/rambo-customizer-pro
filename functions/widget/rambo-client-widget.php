<?php
/**
 * Register team section widget
 *
 */
add_action('widgets_init','rambo_client_grid_section_widget');
function rambo_client_grid_section_widget(){
	
	return register_widget('rambo_client_grid_section_widget');
}

class rambo_client_grid_section_widget extends WP_Widget{
	
	function __construct() {
		parent::__construct(
			'rambo_client_grid_section_widget', // Base ID
			__('WBR : Client Grid Widget', 'rambo'), // Name
			array( 'description' => __('Client item widget', 'rambo' ), ) // Args
		);
	}
	
	public function widget( $args , $instance ) {
		$ExcId = array();
		
		echo $args['before_widget'];
		
	$title=(isset($instance['title'])?$instance['title']:'');
	$instance['client_column_layout'] = (isset($instance['client_column_layout'])?$instance['client_column_layout']:'');
					
		$selected_team_id =( isset($instance['selected_team_id'])?$instance['selected_team_id']:''); 
		$client_simple_column_layout = 12 / $instance['client_column_layout'];
		if($instance['exclude_posts']!=null){ $ExcId = explode(',',$instance['exclude_posts']);	}
		
		?>
		<div class="row our_client_service_section">
			<?php if($title!='') { ?>
			<div class="span12">
			<div class="service_head_title">
				<h3><?php echo $title; ?></h3>
			</div>
			</div>
			<?php } ?>
		<div id="our_client_product"<?php if($instance['client_column_layout']== 5) { ?> class="five-column" <?php } ?>>
				<?php 	
				$count_posts = wp_count_posts( 'rambopro_clientstrip')->publish;                                              
				$arg = array( 'post_type' => 'rambopro_clientstrip','posts_per_page'=>$count_posts); 	
				$clientstrip = new WP_Query( $arg ); 
				if( $clientstrip->have_posts())
				{	
				$i=1;
				while ( $clientstrip->have_posts() ) : $clientstrip->the_post();
				$client_link=get_post_meta( get_the_ID(), 'clientstrip_link', true );
				?>
				
				<div class="span<?php if($instance['client_column_layout']== 5) { echo '2'; } else {echo $client_simple_column_layout; } ?>">
				<?php if(has_post_thumbnail()) : ?>
					<?php //$tt = the_title(); ?>
					<?php $tt = array('alt'=>"tt")?>
					<a href="<?php echo $client_link;?>" target="<?php if(get_post_meta( get_the_ID(),'meta_client_target', true )) echo "_blank";  ?>"> <?php the_post_thumbnail('',$tt);  ?></a>
					<?php else : ?>
					<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI ?>/images/client/logo.png"/>	
					<?php endif; ?>
				</div>
				<?php endwhile; } ?>
		</div>
	</div>
			
			
	<?php echo $args['after_widget'];
	}
	
	
	public function form( $instance ) {
		
		
		$instance['title'] = ( ! empty( $instance['title'] ) ) ? $instance['title'] : '';
		$instance['client_column_layout'] = (isset($instance['client_column_layout'])?$instance['client_column_layout']:1);
		$instance['exclude_posts'] = (isset($instance['exclude_posts'])?$instance['exclude_posts']:'');
		?>
		<h4 for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title','rambo' ); ?></h4>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php if($instance[ 'title' ]) echo esc_attr($instance[ 'title' ]);?>" />
		<p>
			<label for="<?php echo $this->get_field_id( 'client_column_layout' ); ?>"><?php _e('select column layout','rambo' ); ?></label> 
			
			<select id="<?php echo $this->get_field_id( 'client_column_layout' ); ?>" name="<?php echo $this->get_field_name( 'client_column_layout' ); ?>">
			<option value>--<?php echo __('select column layout','rambo'); ?>--</option>
				<option value="1" <?php echo ($instance['client_column_layout']=='1'?'selected':''); ?>>
				<?php echo '1'; ?></option>
				<option value="2" <?php echo ($instance['client_column_layout']=='2'?'selected':''); ?>>
				<?php echo '2'; ?></option>
				<option value="3" <?php echo ($instance['client_column_layout']=='3'?'selected':''); ?>>
				<?php echo '3'; ?></option>
				<option value="4" <?php echo ($instance['client_column_layout']=='4'?'selected':''); ?>>
				<?php echo '4'; ?></option>
				<option value="5" <?php echo ($instance['client_column_layout']=='5'?'selected':''); ?>>
				<?php echo '5'; ?></option>
				<option value="6" <?php echo ($instance['client_column_layout']=='6'?'selected':''); ?>>
				<?php echo '6'; ?></option>
			</select>	
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'exclude_posts' ); ?>"><?php _e( 'Exclude posts id like ( 1,2,3...etc )','rambo' ); ?></label> 
			<textarea rows="5" class="widefat" id="<?php echo $this->get_field_id( 'exclude_posts' ); ?>" name="<?php echo $this->get_field_name( 'exclude_posts' ); ?>"><?php if($instance['exclude_posts']) echo $instance['exclude_posts']; ?></textarea>
		</p>
	<?php 
	}
	
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? $new_instance['title'] : '';
		$instance['client_column_layout'] = ( ! empty( $new_instance['client_column_layout'] ) ) ? $new_instance['client_column_layout'] : '';
		$instance['exclude_posts'] = ( ! empty( $new_instance['exclude_posts'] ) ) ? $new_instance['exclude_posts'] : '';
		
		return $instance;
	}
}