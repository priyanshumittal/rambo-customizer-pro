<?php
/**
 * Register team section widget
 *
 */
add_action('widgets_init','rambo_testimonial_section_widget');
function rambo_testimonial_section_widget(){
	
	return register_widget('rambo_testimonial_section_widget');
}

class rambo_testimonial_section_widget extends WP_Widget{
	
	function __construct() {
		parent::__construct(
			'rambo_testimonial_section_widget', // Base ID
			__('WBR : Testimonial Grid Widget', 'rambo'), // Name
			array( 'description' => __('Testimonial item widget', 'rambo' ), ) // Args
		);
	}
	
	public function widget( $args , $instance ) {
		$ExcId = array();
		
		
		
		$instance['test_column_layout'] = (isset($instance['test_column_layout'])?$instance['test_column_layout']:'');
					
		$selected_team_id =( isset($instance['selected_team_id'])?$instance['selected_team_id']:''); 
		$test_simple_column_layout = 12 / $instance['test_column_layout'];
		if($instance['exclude_posts']!=null){ $ExcId = explode(',',$instance['exclude_posts']);	}
		$testimonial_grid_class=(isset($instance['testimonial_grid_class'])?$instance['testimonial_grid_class']:'');
			if($testimonial_grid_class !='')
			{
				$args['before_widget'] = str_replace('class="', 'class="'. $testimonial_grid_class . ' ',$args['before_widget']);
				
			}
		echo $args['before_widget'];
		
		$query_args = array(
		'ignore_sticky_posts' => 1, 
		'tax_query' => array( array( 'taxonomy' => 'post_format', 'field' => 'slug','terms' => array( 'post-format-quote' ) )),
		'post__not_in' =>$ExcId
		);
		
		$testi_loop = new WP_Query($query_args);
				
			echo '<div class="row">';
			while ( $testi_loop->have_posts() ) : $testi_loop->the_post();
			echo '<div class="span'.$test_simple_column_layout.'">';
			echo '<div class="media">';
				
				if( has_post_thumbnail() ): 
				the_post_thumbnail( 'thumbnail', array( 'class' => 'media-object aboutus_testimonial_img','style'=>'height:100%;' ) ); endif;
				
				
				echo '<div class="media-body">';
				
						echo '<p>'. get_the_content() .'</p>';
						
					echo '<h4>'. get_the_title() .'<br /><small>'.get_post_meta( get_the_ID(),'testimonial_designation', true).'</small></h4>';
					
				echo '</div>';
				
			echo '</div>';
			echo '</div>';
			endwhile;
			echo '</div>';
			
			
	
		echo $args['after_widget'];
	}
	
	
	public function form( $instance ) {
		
		$instance['test_column_layout'] = (isset($instance['test_column_layout'])?$instance['test_column_layout']:1);
		$instance['exclude_posts'] = (isset($instance['exclude_posts'])?$instance['exclude_posts']:'');
		
		$instance['testimonial_grid_class'] = (isset($instance['testimonial_grid_class'])?$instance['testimonial_grid_class']:'');
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'test_column_layout' ); ?>"><?php _e('select column layout','rambo' ); ?></label> 
			
			<select id="<?php echo $this->get_field_id( 'test_column_layout' ); ?>" name="<?php echo $this->get_field_name( 'test_column_layout' ); ?>">
			<option value>--<?php echo __('select column layout','rambo'); ?>--</option>
				<option value="1" <?php echo ($instance['test_column_layout']=='1'?'selected':''); ?>>
				<?php echo '1'; ?></option>
				<option value="2" <?php echo ($instance['test_column_layout']=='2'?'selected':''); ?>>
				<?php echo '2'; ?></option>
				<option value="3" <?php echo ($instance['test_column_layout']=='3'?'selected':''); ?>>
				<?php echo '3'; ?></option>
				<option value="4" <?php echo ($instance['test_column_layout']=='4'?'selected':''); ?>>
				<?php echo '4'; ?></option>
			</select>	
		</p>
        </select>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'exclude_posts' ); ?>"><?php _e( 'Exclude posts id like ( 1,2,3...etc )','rambo' ); ?></label> 
			<textarea rows="5" class="widefat" id="<?php echo $this->get_field_id( 'exclude_posts' ); ?>" name="<?php echo $this->get_field_name( 'exclude_posts' ); ?>"><?php if($instance['exclude_posts']) echo $instance['exclude_posts']; ?></textarea>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'testimonial_grid_class' ); ?>"><?php _e('CSS Classes (optional)','rambo' ); ?></label> 
		</p>
		<input class="widefat" id="<?php echo $this->get_field_id( 'testimonial_grid_class' ); ?>" name="<?php echo $this->get_field_name( 'testimonial_grid_class' ); ?>" type="text" value="<?php if($instance[ 'testimonial_grid_class' ]) echo esc_attr($instance[ 'testimonial_grid_class' ]);?>" />
	<?php 
	}
	
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['test_column_layout'] = ( ! empty( $new_instance['test_column_layout'] ) ) ? $new_instance['test_column_layout'] : '';
		$instance['exclude_posts'] = ( ! empty( $new_instance['exclude_posts'] ) ) ? $new_instance['exclude_posts'] : '';
		$instance['testimonial_grid_class'] = ( ! empty( $new_instance['testimonial_grid_class'] ) ) ? $new_instance['testimonial_grid_class'] : '';
		
		return $instance;
	}
}