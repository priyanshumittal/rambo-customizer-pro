<?php	
add_action( 'widgets_init', 'rambo_widgets_init');
function rambo_widgets_init() {
$rambo_pro_theme_options = theme_data_setup();
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
$site_intro_column_layout = 12 / $current_options['site_intro_column_layout'];	
$service_column_layout = 12 / $current_options['service_column_layout'];
$project_column_layout = 12 / $current_options['project_column_layout'];
$site_intro_bottom_column_layout = 12 / $current_options['site_intro_bottom_column_layout'];	
$additionla_section_one_column_layout = 12 / $current_options['additionla_section_one_column_layout'];
$additionla_section_two_column_layout = 12 / $current_options['additionla_section_two_column_layout'];	
$testimonials_column_layout = 12 / $current_options['testimonials_column_layout'];
//$news_column_layout = 12 / $current_options['news_column_layout'];

/*sidebar*/
register_sidebar( array(
		'name' => __('Sidebar widget area', 'rambo' ),
		'id' => 'sidebar-primary',
		'description' => __('Sidebar widget area', 'rambo' ),
		'before_widget' => '<div class="sidebar_widget widget %2$s" >',
		'after_widget' => '</div>',
		'before_title' => '<div class="sidebar_widget_title"><h2>',
		'after_title' => '</h2></div>',
	) );
	
	
	
	//Header sidebar
	register_sidebar( array(
		'name' => __('Top header left widget area', 'rambo' ),
		'id' => 'home_header_sidebar_left',
		'description' => __('Top header left widget area', 'rambo' ),
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget_title"><h2>',
		'after_title' => '</h2></div>',
	) );
	
	register_sidebar( array(
		'name' => __('Top header center widget area', 'rambo' ),
		'id' => 'home_header_sidebar_center',
		'description' => __('Top header right widget area', 'rambo' ),
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget_title"><h2>',
		'after_title' => '</h2></div>',
	) );
	
	
	register_sidebar( array(
		'name' => __('Top header right widget area', 'rambo' ),
		'id' => 'home_header_sidebar_right',
		'description' => __('Top header right widget area', 'rambo' ),
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget_title"><h2>',
		'after_title' => '</h2></div>',
	) );
	

//Site Intro Top	
register_sidebar( array(
		'name' => __('Call to action top widget area', 'rambo' ),
		'id' => 'site-intro-area',
		'description' => __('Call to action top widget area', 'rambo' ),
		'before_widget' => '<div id="%1$s" class="span'.$site_intro_column_layout.' widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget_title"><h2>',
		'after_title' => '</h2></div>',
	) );


// Service Widget Sidebar	
	register_sidebar( array(
		'name' => __('Homepage service section - sidebar', 'rambo' ),
		'id' => 'sidebar-service',
	) );
	
	
//Project Sidebar
	register_sidebar( array(
			'name' => __( 'Homepage project section - sidebar', 'rambo' ),
			'id' => 'sidebar-project',
		) );
		
//Site Intro Bottom	
register_sidebar( array(
		'name' => __( 'Call to action bottom widget area', 'rambo' ),
		'id' => 'site-intro-area-bottom',
		'description' => __( 'Call to action bottom widget area', 'rambo' ),
		'before_widget' => '<div id="%1$s" class="span'.$site_intro_bottom_column_layout.' widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget_title"><h2>',
		'after_title' => '</h2></div>',
	) );
		

//Additional Area One 
register_sidebar( array(
		'name' => __('HomePage Additional One Widget area', 'rambo' ),
		'id' => 'additional-area-one',
		'description' => __( 'HomePage Additional One Widget area', 'rambo' ),
		'before_widget' => '<div id="%1$s" class="span12 widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget_title"><h2>',
		'after_title' => '</h2></div>',
	) );
	
//Additional Area Two 
register_sidebar( array(
		'name' => __('HomePage Additional Two Widget area', 'rambo' ),
		'id' => 'additional-area-two',
	) );

	register_sidebar( array(
		'name' => __('About testimonial widget area', 'rambo' ),
		'id' => 'testimonial-widget-area',
	) );
	
//Contact Template Sidebar 
register_sidebar( array(
		'name' => __('Contact template sidebar area', 'rambo' ),
		'id' => 'contact-sidebar-area',
		'description' => __('Contact template sidebar area', 'rambo' ),
		'before_widget' => '<div id="%1$s"  class="sidebar_widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget_title"><h2>',
		'after_title' => '</h2></div>',
	) );
	
//Footer Sidebar	
register_sidebar( array(
		'name' => __('Footer widget area', 'rambo' ),
		'id' => 'footer-widget-area',
		'description' => __('Footer widget area', 'rambo' ),
		'before_widget' => '<div class="span4 footer_widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget_title"><h2>',
		'after_title' => '</h2></div>',
	) );
	
register_sidebar( array(
	'name' => __('Woocommerce sidebar widget area', 'rambo' ),
	'id' => 'woocommerce',
	'description' => __( 'Woocommerce sidebar widget area', 'rambo' ),
	'before_widget' => '<div class="sidebar-widget" >',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>'
	) );
	
	//Footer social Sidebar 
register_sidebar( array(
		'name' => __('Footer social icon sidebar area', 'rambo' ),
		'id' => 'footer-social-icon-sidebar-area',
		'description' => __('Footer social icon sidebar area', 'rambo' ),
		'before_widget' => '<div id="%1$s"  class="pull-right %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget_title"><h2>',
		'after_title' => '</h2></div>',
	) );
	
}	                     
?>