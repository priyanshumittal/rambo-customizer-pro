<?php
/**
 * Feature Page Widget
 *
 */
add_action('widgets_init','rambo_client_widget');
function rambo_client_widget(){
	
	return register_widget('rambo_client_widget');
}

class rambo_client_widget extends WP_Widget{
	
	function __construct() {
		parent::__construct(
			'rambo_client_widget', // Base ID
			__('WBR : Client Slider Widget','rambo'), // Name
			array( 'description' => __('Client item widget', 'rambo'), ) // Args
		);
	}
	
	
	public function widget( $args, $instance ) {
	
	$instance['column_layout'] = (isset($instance['column_layout'])?$instance['column_layout']:'1');
	$title=(isset($instance['title'])?$instance['title']:'');
	$instance['client_speed'] = (isset($instance['client_speed'])?$instance['client_speed']:4500);
	$client_column_layout = 12 / $instance['column_layout'];
	?>
	<div class="our_client_service_section">
			<?php if($title!='') { ?>
			<div class="span12">
			<div class="service_head_title">
				<h3><?php echo $title; ?></h3>
			</div>
			</div>
			<?php } ?>
			<div id="<?php echo $args['widget_id']; ?>" class="carousel slide" data-ride="carousel">
			<?php if($instance['column_layout']== 5) {?>
			<div class="carousel-inner five-column">
			<?php } else { ?> <div class="carousel-inner"> <?php } ?>
				<?php 	
				$count_posts = wp_count_posts( 'rambopro_clientstrip')->publish;                                              
				$arg = array( 'post_type' => 'rambopro_clientstrip','posts_per_page'=>$count_posts); 	
				$clientstrip = new WP_Query( $arg ); 
				if( $clientstrip->have_posts())
				{	
				$i=1;
				while ( $clientstrip->have_posts() ) : $clientstrip->the_post();
				$client_link=get_post_meta( get_the_ID(), 'clientstrip_link', true );
				?>
				<div class="item <?php if($i==1) { echo 'active';} $i++; ?>">
						<div class="span<?php if($instance['column_layout']== 5) { echo '2'; } else {echo $client_column_layout; } ?>">
						<?php if(has_post_thumbnail()) : ?>
							<?php //$tt = the_title(); ?>
							<?php $tt = array('alt'=>"tt")?>
							<a href="<?php echo $client_link;?>" target="<?php if(get_post_meta( get_the_ID(),'meta_client_target', true )) echo "_blank";  ?>"> <?php the_post_thumbnail('',$tt);  ?></a>
							<?php else : ?>
							<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI ?>/images/client/logo.png"/>	
							<?php endif; ?>
						</div>
					</div>
				<?php endwhile; } ?>
			</div>
		</div>
	</div>
	<script>
	// Clients Js	
jQuery(function() {


	jQuery("#<?php echo $args['widget_id']; ?>").carousel(	{ 
		pause: "hover",
		interval: <?php echo $instance['client_speed']; ?>
	});
	
	<?php $client_for_layout = $instance['column_layout'];
					 $test=7;
							
					  if($client_for_layout == 6)
					  {
						$test =4;
						
						  
					  }
					  if($client_for_layout == 5)
					  {
						$test =3;
						
						  
					  }
					  elseif($client_for_layout == 4)
					  {
						$test =2;
						
						  
					  }
					  elseif($client_for_layout == 3)
					  {
						$test =1;
						
						  
					  }
					   elseif($client_for_layout == 2)
					  {
						$test =0;
						
					  }
					  
					  if($test<7)
					  { ?>
	
	jQuery('#<?php echo $args['widget_id']; ?> .item').each(function(){
		
		
	  var next = jQuery(this).next();
	  if (!next.length) {
		next = jQuery(this).siblings(':first');
	  }
	  next.children(':first-child').clone().appendTo(jQuery(this));
	  
	  // For Two Column Layout i=0
	  // For Three Column Layout i=1
	  for (var i=0;i<<?php echo $test; ?>;i++) {
		next=next.next();
		if (!next.length) {
			next = jQuery(this).siblings(':first');
		}
		
		next.children(':first-child').clone().appendTo(jQuery(this));
	  }
		  
	});
	<?php  } ?>				
});	
</script>
    <?php 
	
	
	}
	
	public function form( $instance ) {
		$instance['title'] = ( ! empty( $instance['title'] ) ) ? $instance['title'] : '';
		$instance['client_speed'] = (isset($instance['client_speed'])?$instance['client_speed']:4500);
		$instance['column_layout'] = (isset($instance['column_layout'])?$instance['column_layout']:1);
		?>
		<h4 for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title','rambo' ); ?></h4>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php if($instance[ 'title' ]) echo esc_attr($instance[ 'title' ]);?>" />
		<p>
			<label for="<?php echo $this->get_field_id( 'column_layout' ); ?>"><?php _e('select column layout','rambo' ); ?></label> 
			
			<select id="<?php echo $this->get_field_id( 'column_layout' ); ?>" name="<?php echo $this->get_field_name( 'column_layout' ); ?>">
			<option value>--<?php echo __('select column layout','rambo'); ?>--</option>
				<option value="1" <?php echo ($instance['column_layout']=='1'?'selected':''); ?>>
				<?php echo '1'; ?></option>
				<option value="2" <?php echo ($instance['column_layout']=='2'?'selected':''); ?>>
				<?php echo '2'; ?></option>
				<option value="3" <?php echo ($instance['column_layout']=='3'?'selected':''); ?>>
				<?php echo '3'; ?></option>
				<option value="4" <?php echo ($instance['column_layout']=='4'?'selected':''); ?>>
				<?php echo '4'; ?></option>
				<option value="5" <?php echo ($instance['column_layout']=='5'?'selected':''); ?>>
				<?php echo '5'; ?></option>
				<option value="6" <?php echo ($instance['column_layout']=='6'?'selected':''); ?>>
				<?php echo '6'; ?></option>
			</select>	
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'client_speed' ); ?>"><?php _e( 'Speed','rambo' ); ?></label><br/> 
			<select id="<?php echo $this->get_field_id( 'client_speed' ); ?>" name="<?php echo $this->get_field_name( 'client_speed' ); ?>">
				<option value>--<?php echo __('Slider speed','rambo'); ?>--</option>
				<option value="500" <?php echo ($instance['client_speed']==500?'selected':''); ?>>
				<?php echo '500'; ?></option>
				<option value="1000" <?php echo ($instance['client_speed']==1000?'selected':''); ?>>
				<?php echo '1000'; ?></option>
				<option value="1500" <?php echo ($instance['client_speed']==1500?'selected':''); ?>>
				<?php echo '1500'; ?></option>
				<option value="2000" <?php echo ($instance['client_speed']==2000?'selected':''); ?>>
				<?php echo '2000'; ?></option>
				<option value="2500" <?php echo ($instance['client_speed']==2500?'selected':''); ?>>
				<?php echo '2500'; ?></option>
				<option value="3000" <?php echo ($instance['client_speed']==3000?'selected':''); ?>>
				<?php echo '3000'; ?></option>
				<option value="3500" <?php echo ($instance['client_speed']==3500?'selected':''); ?>>
				<?php echo '3500'; ?></option>
				<option value="4000" <?php echo ($instance['client_speed']==4000?'selected':''); ?>>
				<?php echo '4000'; ?></option>
				<option value="4500" <?php echo ($instance['client_speed']==4500?'selected':''); ?>>
				<?php echo '4500'; ?></option>
				<option value="5500" <?php echo ($instance['client_speed']==5500?'selected':''); ?>>
				<?php echo '5500'; ?></option>
				<option value="6000" <?php echo ($instance['client_speed']==6000?'selected':''); ?>>
				<?php echo '6000'; ?></option>
				<option value="6500" <?php echo ($instance['client_speed']==6500?'selected':''); ?>>
				<?php echo '6500'; ?></option>
				<option value="7000" <?php echo ($instance['client_speed']==7000?'selected':''); ?>>
				<?php echo '7000'; ?></option>
				<option value="7500" <?php echo ($instance['client_speed']==7500?'selected':''); ?>>
				<?php echo '7500'; ?></option>
				<option value="8000" <?php echo ($instance['client_speed']==8000?'selected':''); ?>>
				<?php echo '8000'; ?></option>
				<option value="8500" <?php echo ($instance['client_speed']==8500?'selected':''); ?>>
				<?php echo '8500'; ?></option>
				<option value="9000" <?php echo ($instance['client_speed']==9000?'selected':''); ?>>
				<?php echo '9000'; ?></option>
				<option value="9500" <?php echo ($instance['client_speed']==9500?'selected':''); ?>>
				<?php echo '9500'; ?></option>
				<option value="10000" <?php echo ($instance['client_speed']==10000?'selected':''); ?>>
				<?php echo '10000'; ?></option>
			</select>
		</p>
		<?php 
	}
	
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? $new_instance['title'] : '';
		$instance['client_speed'] = ( ! empty( $new_instance['client_speed'] ) ) ? $new_instance['client_speed'] : '';
		$instance['column_layout'] = ( ! empty( $new_instance['column_layout'] ) ) ? $new_instance['column_layout'] : '';
		
		return $instance;
	}
}