<?php
add_action( 'widgets_init','rambo_section_header_widget'); 
   function rambo_section_header_widget() { return   register_widget( 'rambo_section_header_widget' ); }
/**
 * Adds rambo_sidbar_usefull_page_widget widget.
 */
class rambo_section_header_widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'rambo_section_header_widget', // Base ID
			__('WBR: Section Header Widget', 'rambo'), // Name
			array( 'description' => __( 'Display the section title and description.','rambo' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		$instance[ 'title' ] = isset($instance[ 'title' ])?$instance[ 'title' ]:'';
		$instance[ 'description' ] = isset($instance[ 'description' ])?$instance[ 'description' ]:'';		
		
		echo $args['before_widget'];
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title']; ?>		
		
						<div class="section-header">
							<h1 class="section-title"><?php echo  $instance[ 'title' ];?></h1>
							<p class="section-subtitle"><?php echo  $instance[ 'description' ];?></p>
						</div>
					
	
		<?php		
		echo $args['after_widget']; // end of sidbar usefull links widget		
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
		public function form( $instance ) {
			
		
		$instance[ 'title' ] = isset($instance[ 'title' ])?$instance[ 'title' ]:'';
		$instance[ 'description' ] = isset($instance[ 'description' ])?$instance[ 'description' ]:'';	
		?>
		
		<h4 for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title','rambo' ); ?></h4>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php if($instance[ 'title' ]) echo esc_attr($instance[ 'title' ]);?>" />
		
		<h4 for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e('Description','rambo' ); ?></h4>
		<input class="widefat" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" type="text" value="<?php if($instance[ 'description' ]) echo esc_attr($instance[ 'description' ]);?>"/>
		
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		
		$instance['description'] = ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';
		
		return $instance;
	}

}
?>