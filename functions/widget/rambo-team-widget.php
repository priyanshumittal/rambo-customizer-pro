<?php
/**
 * Register team section widget
 *
 */
add_action('widgets_init','rambo_team_section_widget');
function rambo_team_section_widget(){
	
	return register_widget('rambo_team_section_widget');
}

class rambo_team_section_widget extends WP_Widget{
	
	protected $class;
	
	function __construct() {
		parent::__construct(
			'rambo_team_section_widget', // Base ID
			__('WBR : Team Widget', 'rambo'), // Name
			array( 'description' => __('Team item widget', 'rambo' ),
			) // Args
		);
	}
	
	public function widget( $args , $instance ) {
			
			$selected_team_id=(isset($instance['selected_team_id'])?$instance['selected_team_id']:'');
			$team_class=(isset($instance['team_class'])?$instance['team_class']:'');
			if($team_class !='')
			{
				$args['before_widget'] = str_replace('class="', 'class="'. $team_class . ' ',$args['before_widget']);
				
			}
			
			
			echo $args['before_widget'];
			
			if(($instance['selected_team_id']) !=null) {
			$page_data=get_post($selected_team_id);
			
			$i=1;
			
			$arg = array( 'post_type' => 'rambopro_team', 'post__in' => array( $selected_team_id ) );
			
			$team = new WP_Query( $arg );
			
			if($team->have_posts()):
			
				while ( $team->have_posts() ) : $team->the_post();
				
                    global $post;
					
				    $my_meta = get_post_meta( $post->ID ,'_my_meta', TRUE );
	
			?>
			
			        <div class="thumbnail team_bg">
					     <?php $link=get_post_meta( $instance['selected_team_id'], 'team_link', true );?>
						<?php if(has_post_thumbnail()):?>
						<a href="<?php echo $link;?>" target="<?php if(get_post_meta( $instance['selected_team_id'],'meta_team_target', true )) echo "_blank";  ?>"> <?php the_post_thumbnail(); ?> </a>
						<?php endif;?>
						<div class="caption">
						   
							<?php $designation=get_post_meta( $instance['selected_team_id'], 'team_designation', true );?>
							<h4 class="text-center"><a href="<?php echo $link;?>" target="<?php if(get_post_meta( $instance['selected_team_id'],'meta_team_target', true )) echo "_blank";  ?>"> <?php the_title();?></a>
								<small><?php if(isset( $designation ) ) { echo $designation;   }  else  { 
								echo _e('Designation','rambo'); } ?></small>
							</h4>
							<?php $description=get_post_meta( $instance['selected_team_id'], 'team_role', true );?>
							<p class="text-center">
								<?php if(isset($description)){ echo  $description; } 
								else { echo _e('Description','rambo'); } ?>
							</p>
						</div>
					</div>
               <?php 
			   
				if( $i%3==0 ) { echo '<div class="clearfix"></div>'; } $i++;
				
				endwhile;
			?>
			
		<?php endif; ?>
			
    <?php }			
		echo $args['after_widget'];
	}
	
		public function form( $instance ) {
		$instance['selected_team_id'] = isset($instance['selected_team_id']) ? $instance['selected_team_id'] : '';
		$instance['team_class'] = isset($instance['team_class']) ? $instance['team_class'] : '';
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'selected_team_id' ); ?>"><?php _e('Select Team Member','rambo' ); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'selected_team_id' ); ?>" name="<?php echo $this->get_field_name( 'selected_team_id' ); ?>">
				<option value>--<?php echo _e('Select','rambo'); ?>--</option>
				<?php
					$selected_team_id = $instance['selected_team_id'];
					$args = array(
					'post_type' => 'rambopro_team',
					);
					$loop = new WP_Query($args);

					while($loop->have_posts()): $loop->the_post();
						$option = '<option value="' . get_the_ID() . '" ';
						$option .= ( get_the_ID() == $selected_team_id ) ? 'selected="selected"' : '';
						$option .= '>';
						$option .= get_the_title();
						$option .= '</option>';
						echo $option;
					endwhile;
					wp_reset_query();
				?>	
			</select>
			<br/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'team_class' ); ?>"><?php _e('CSS Classes (optional)','rambo' ); ?></label> 
		</p>
		<input class="widefat" id="<?php echo $this->get_field_id( 'team_class' ); ?>" name="<?php echo $this->get_field_name( 'team_class' ); ?>" type="text" value="<?php if($instance[ 'team_class' ]) echo esc_attr($instance[ 'team_class' ]);?>" />
	<?php 
	}
	
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['selected_team_id'] = ( ! empty( $new_instance['selected_team_id'] ) ) ? $new_instance['selected_team_id'] : '';
		$instance['team_class'] = ( ! empty( $new_instance['team_class'] ) ) ? $new_instance['team_class'] : '';
		
		
		return $instance;
	}
}