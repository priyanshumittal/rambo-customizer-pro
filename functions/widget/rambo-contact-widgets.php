<?php
add_action( 'widgets_init','rambo_widget_contact'); 
   function rambo_widget_contact() { return   register_widget( 'rambo_contact_widget' ); }
/**
 * Adds rambo footer contact  widget.
 */
class rambo_contact_widget extends WP_Widget {



	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'rambo_contact_widget', // Base ID
			__('WBR : Contact Widget','rambo'), // Name
			array( 'description' => __('Your contact details display', 'rambo' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		error_reporting(0);
		$title = apply_filters( 'widget_title', $instance['title'] );
		$first_icon = apply_filters( 'widget_title', $instance['first_icon'] );
		$Contact_address = ! empty( $instance['Contact_address'] ) ? $instance['Contact_address'] : '';
		$second_icon = apply_filters( 'widget_title', $instance['second_icon'] );
		$Contact_phone_number = ! empty( $instance['Contact_phone_number'] ) ? $instance['Contact_phone_number'] : '';
		$third_icon = apply_filters( 'widget_title', $instance['third_icon'] );
		$Contact_email_address = ! empty( $instance['Contact_email_address'] ) ? $instance['Contact_email_address'] : '';
		
		$contact_widget_class=(isset($instance['contact_widget_class'])?$instance['contact_widget_class']:'');
			if($contact_widget_class !='')
			{
				$args['before_widget'] = str_replace('class="', 'class="'. $contact_widget_class . ' ',$args['before_widget']);
				
			}
		echo $args['before_widget'];
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title']; 
		//$current_options = get_option('rambo_pro_theme_options');
		?>
		<?php if($first_icon && $Contact_address != null): ?>
		<p class="widget_con_detail">
			<i class="<?php if($first_icon) { echo $first_icon; } ?> icon-spacing"></i> 
			<?php if($Contact_address) { echo $Contact_address; } ?>
		</p>
		<?php endif; ?>
		<?php if($second_icon && $Contact_phone_number != null): ?>
		<p class="widget_con_detail">
			<i class="<?php if($second_icon) { echo $second_icon; } ?> icon-spacing"></i> 
			<?php if($Contact_phone_number) { echo $Contact_phone_number; } ?>
		</p>
		<?php endif; ?>
		<?php if($third_icon && $Contact_email_address != null): ?>
		<p class="widget_con_detail">
			<i class="<?php if($third_icon) { echo $third_icon; } ?> icon-spacing"></i> 
			<?php if($Contact_email_address) { echo $Contact_email_address; } ?>			
		</p>
        <?php endif; ?>		
		<?php		
		echo $args['after_widget']; // end of footer usefull links widget		
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] )) { $title = $instance[ 'title' ];	}
		else {	$title = __('Contact Info', 'rambo' );		}
		
		if ( isset( $instance[ 'first_icon' ] )) { $first_icon = $instance[ 'first_icon' ];	}
		else {	$first_icon = 'fa fa-home';	}
		
		if ( isset( $instance[ 'Contact_address' ] )) { $Contact_address = $instance[ 'Contact_address' ];	}
		else {	$Contact_address = 'New York City, USA';		}
		
		if ( isset( $instance[ 'second_icon' ] )) { $second_icon = $instance[ 'second_icon' ];	}
		else {	$second_icon = 'fa fa-phone';	}
		
		if ( isset( $instance[ 'Contact_phone_number' ] )) { $Contact_phone_number = $instance[ 'Contact_phone_number' ];	}
		else {	$Contact_phone_number = '420-300-3850';		}
		
		if ( isset( $instance[ 'third_icon' ] )) { $third_icon = $instance[ 'third_icon' ];	}
		else {	$third_icon = 'fa fa-envelope';	}
		
		if ( isset( $instance[ 'Contact_email_address' ] )) { $Contact_email_address = $instance[ 'Contact_email_address' ];	}
		else {	$Contact_email_address = 'info@rambotheme.com';		}
		$instance['contact_widget_class'] = (isset($instance['contact_widget_class'])?$instance['contact_widget_class']:'');
		
		
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title','rambo' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
		<label for="<?php echo $this->get_field_id( 'first_icon' ); ?>"><?php _e('Icon','rambo' ); ?>
		<?php _e( 'Specify Font Awesome icon class ( like: fa fa-shield ). <a href="//fontawesome.io/icons/" target="blank" >Browse all icons </a>','rambo' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'first_icon' ); ?>" name="<?php echo $this->get_field_name( 'first_icon' ); ?>" type="text" value="<?php echo $first_icon; ?>" />
		</p>
		<p>
		<label for="<?php echo $this->get_field_id( 'Contact_address' ); ?>"><?php _e('Contact address','rambo' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'Contact_address' ); ?>" name="<?php echo $this->get_field_name( 'Contact_address' ); ?>" type="text" value="<?php echo $Contact_address; ?>" />
		</p>
		<p>
		<label for="<?php echo $this->get_field_id( 'second_icon' ); ?>"><?php _e('Icon','rambo' ); ?>
		<?php _e( 'Specify Font Awesome icon class ( like: fa fa-shield ). <a href="//fontawesome.io/icons/" target="blank" >Browse all icons </a>','rambo' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'second_icon' ); ?>" name="<?php echo $this->get_field_name( 'second_icon' ); ?>" type="text" value="<?php echo $second_icon; ?>" />
		</p>
		<p>	<label for="<?php echo $this->get_field_id( 'Contact_phone_number' ); ?>"><?php _e( 'Phone','rambo' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'Contact_phone_number' ); ?>" name="<?php echo $this->get_field_name( 'Contact_phone_number' ); ?>" type="text" value="<?php echo $Contact_phone_number ; ?>" />
		</p>
		<p>
		<label for="<?php echo $this->get_field_id( 'third_icon' ); ?>"><?php _e('Icon','rambo' ); ?>
		<?php _e( 'Specify Font Awesome icon class ( like: fa fa-shield ). <a href="//fontawesome.io/icons/" target="blank" >Browse all icons </a>','rambo' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'third_icon' ); ?>" name="<?php echo $this->get_field_name( 'third_icon' ); ?>" type="text" value="<?php echo $third_icon; ?>" />
		</p>
		<p>
		<label for="<?php echo $this->get_field_id( 'Contact_email_address' ); ?>"><?php _e('Email','rambo' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'Contact_email_address' ); ?>" name="<?php echo $this->get_field_name( 'Contact_email_address' ); ?>" type="text" value="<?php echo $Contact_email_address; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'contact_widget_class' ); ?>"><?php _e('CSS Classes (optional)','rambo' ); ?></label> 
		</p>
		<input class="widefat" id="<?php echo $this->get_field_id( 'contact_widget_class' ); ?>" name="<?php echo $this->get_field_name( 'contact_widget_class' ); ?>" type="text" value="<?php if($instance[ 'contact_widget_class' ]) echo esc_attr($instance[ 'contact_widget_class' ]);?>" />
		
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';	
		$instance['first_icon'] = ( ! empty( $new_instance['first_icon'] ) ) ? strip_tags( $new_instance['first_icon'] ) : '';
		$instance['Contact_address'] = ( ! empty( $new_instance['Contact_address'] ) ) ? $new_instance['Contact_address']  : '';
		$instance['second_icon'] = ( ! empty( $new_instance['second_icon'] ) ) ? strip_tags( $new_instance['second_icon'] ) : '';
		$instance['Contact_phone_number'] = ( ! empty( $new_instance['Contact_phone_number'] ) ) ? $new_instance['Contact_phone_number'] : '';
		$instance['third_icon'] = ( ! empty( $new_instance['third_icon'] ) ) ? strip_tags( $new_instance['third_icon'] ) : '';
		$instance['Contact_email_address'] = ( ! empty( $new_instance['Contact_email_address'] ) ) ? $new_instance['Contact_email_address'] : '';
		$instance['contact_widget_class'] = ( ! empty( $new_instance['contact_widget_class'] ) ) ? strip_tags( $new_instance['contact_widget_class'] ) : '';		
		return $instance;
	}

} // class Foo_Widget
?>