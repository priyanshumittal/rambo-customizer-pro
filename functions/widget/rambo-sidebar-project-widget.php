<?php
/**
 * Feature Page Widget
 *
 */
add_action('widgets_init','rambo_project_widget');
function rambo_project_widget(){
	
	return register_widget('rambo_project_widget');
}

class rambo_project_widget extends WP_Widget{
	
	function __construct() {
		parent::__construct(
			'rambo_project_widget', // Base ID
			__('WBR : Project Widget', 'rambo'), // Name
			array( 'description' => __('Project item widget', 'rambo'), ) // Args
		);
	}
	
	
		public function widget( $args, $instance ) {
		
		$selected_post_id=(isset($instance['selected_post_id'])?$instance['selected_post_id']:'');
		if(($instance['selected_post_id']) !=null) {
		$page_data=get_post($selected_post_id);
		$post_thumbnail_id = get_post_thumbnail_id($selected_post_id);
		$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
		$link=1;
		$portfolio_project_link = '';
		if(get_post_meta( $selected_post_id,'portfolio_project_link', true )) 
		{ $portfolio_project_link=get_post_meta( $selected_post_id,'portfolio_project_link', true ); }
		else { $portfolio_project_link = get_post_permalink(); }
		$portfolio_project_class=(isset($instance['portfolio_project_class'])?$instance['portfolio_project_class']:'');
			if($portfolio_project_class !='')
			{
				$args['before_widget'] = str_replace('class="', 'class="'. $portfolio_project_class . ' ',$args['before_widget']);
				
			}
		echo $args['before_widget'];
		
		
		?>
			<div class="thumbnail">
					<?php echo get_the_post_thumbnail( $selected_post_id, 'full' ); ?>
					
					
					<?php if(get_post_meta( $selected_post_id,'portfolio_project_link', true )) 
						{ $portfolio_project_link=get_post_meta( $selected_post_id,'portfolio_project_link', true ); }
						else { $portfolio_project_link = get_post_permalink($selected_post_id); } 
					?>
					<div class="featured_service_content">
					<?php if ($link==1 ) { ?>
					<h3><a href="<?php echo $portfolio_project_link; ?>" <?php  if(get_post_meta( $selected_post_id,'portfolio_project_target', true )) { echo "target='_blank'"; }  ?>><?php echo $page_data->post_title; ?></a></h3>
					<?php } else { ?><h3><?php echo $page_data->post_title; ?> </h3> <?php } ?>
					

					<p><?php echo substr($page_data->post_content, 0,100).' ...'; ?></p>
					
					<p><a class="featured_port_projects_btn pull-right" href="<?php echo $portfolio_project_link; ?>" <?php  if(get_post_meta( $selected_post_id,'portfolio_project_target', true )) { echo "target='_blank'"; }  ?> ><?php _e('Read More','rambo')?></a></p>
					</div>
				</div>
		<?php }
	echo $args['after_widget'];
	}
	
	public function form( $instance ) {
		$instance['selected_post_id'] = isset($instance['selected_post_id']) ? $instance['selected_post_id'] : '';
		$instance['portfolio_project_class'] = isset($instance['portfolio_project_class']) ? $instance['portfolio_project_class'] : '';
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'selected_post_id' ); ?>"><?php _e( 'Select Project Items','rambo' ); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'selected_post_id' ); ?>" name="<?php echo $this->get_field_name( 'selected_post_id' ); ?>">
				<option value>--<?php echo __('Select','rambo'); ?>--</option>
				<?php
					$selected_post_id = $instance['selected_post_id'];
					$args = array(
					'post_type' => 'rambopro_project',
					);
					$loop = new WP_Query($args);

					while($loop->have_posts()): $loop->the_post();
						$option = '<option value="' . get_the_ID() . '" ';
						$option .= ( get_the_ID() == $selected_post_id ) ? 'selected="selected"' : '';
						$option .= '>';
						$option .= get_the_title();
						$option .= '</option>';
						echo $option;
					endwhile;
					wp_reset_query();
				?>	
			</select>
			<br/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'portfolio_project_class' ); ?>"><?php _e('CSS Classes (optional)','rambo' ); ?></label> 
		</p>
		<input class="widefat" id="<?php echo $this->get_field_id( 'portfolio_project_class' ); ?>" name="<?php echo $this->get_field_name( 'portfolio_project_class' ); ?>" type="text" value="<?php if($instance[ 'portfolio_project_class' ]) echo esc_attr($instance[ 'portfolio_project_class' ]);?>" />
		<?php 
	}
	
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['selected_post_id'] = ( ! empty( $new_instance['selected_post_id'] ) ) ? $new_instance['selected_post_id'] : '';
		$instance['portfolio_project_class'] = ( ! empty( $new_instance['portfolio_project_class'] ) ) ? $new_instance['portfolio_project_class'] : '';
		
		return $instance;
	}
}