<?php
/**
 * WPML and Polylang compatibility functions.
*/

/**
 * Filter to translate strings
 */
function rambo_translate_single_string( $original_value, $domain ) {
	if ( is_customize_preview() ) {
		$wpml_translation = $original_value;
	} else {
		$wpml_translation = apply_filters( 'wpml_translate_single_string', $original_value, $domain, $original_value );
		if ( $wpml_translation === $original_value && function_exists( 'pll__' ) ) {
			return pll__( $original_value );
		}
	}
	return $wpml_translation;
}
add_filter( 'rambo_translate_single_string', 'rambo_translate_single_string', 10, 2 );

/**
 * Helper to register pll string.
*/
function rambo_pll_string_register_helper( $sectionname ) {
	if ( ! function_exists( 'pll_register_string' ) ) {
		return;
	}
	$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options'));
	if(isset($current_options[$sectionname])){
    $repeater_content = $current_options[$sectionname];
	$repeater_content = json_decode( $repeater_content );
	if ( ! empty( $repeater_content ) ) {
		foreach ( $repeater_content as $repeater_item ) {
			foreach ( $repeater_item as $field_name => $field_value ) {
				if ( $field_value !== 'undefined' ) {
					
						if ( $field_name !== 'id' ) {
							$f_n = ucfirst( $field_name );
							pll_register_string( $f_n, $field_value);
					}
				}
			}
		}
	 }
   }

}

/**
 * Features section. Register strings for translations.
 *
 * @modified 1.1.30
 * @access public
 */
 
/* Service Content translation */ 
function rambo_features_register_strings() {
	rambo_pll_string_register_helper( 'rambo_service_content', 'Service section' );
}

/* Testimonial Content translation */ 
function rambo_testimonials_register_strings() {
	rambo_pll_string_register_helper( 'rambo_testimonial_content', 'Testimonials section' );
 }

/* Slider Content translation */
function rambo_slider_register_strings() {
	rambo_pll_string_register_helper( 'rambo_slider_content', 'Slider section' );
}

/* Team Content translation */
function rambo_team_register_strings() {
	rambo_pll_string_register_helper( 'rambo_team_content', 'Team section' );
}

/* Client Content translation */
function rambo_clients_register_strings() {
	rambo_pll_string_register_helper( 'rambo_clients_content', 'Client section' );
}


if ( function_exists( 'pll_register_string' ) ) {
	add_action( 'after_setup_theme', 'rambo_features_register_strings', 11 );
	add_action( 'after_setup_theme', 'rambo_testimonials_register_strings', 11 );
	add_action( 'after_setup_theme', 'rambo_slider_register_strings', 11 );
	add_action( 'after_setup_theme', 'rambo_team_register_strings', 11 );
	add_action( 'after_setup_theme', 'rambo_clients_register_strings', 11 );
}