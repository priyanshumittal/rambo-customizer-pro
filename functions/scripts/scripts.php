<?php
function rambo_scripts()
{	if ( is_singular() ) wp_enqueue_script( "comment-reply" );
	/*Template Color Scheme CSs*/
	/*Font Awesome CSS*/
	wp_enqueue_style('rambo-style', get_stylesheet_uri() );
	wp_enqueue_style ('bootstrap',WEBRITI_TEMPLATE_DIR_URI.'/css/bootstrap.css');
	//bootstrap css
	$rambo_pro_theme_options = theme_data_setup();
	$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
	if($current_options['theme_color_enable'] == true) {
		custom_light();
		}
		else
		{
		$class=$current_options['theme_color'];
		wp_enqueue_style('default', WEBRITI_TEMPLATE_DIR_URI . '/css/'.$class);
		}
	wp_enqueue_style ('rambo-font-awesome',WEBRITI_TEMPLATE_DIR_URI .'/css/font-awesome/css/all.min.css');

	wp_enqueue_style ('element',WEBRITI_TEMPLATE_DIR_URI.'/css/element.css');
	wp_enqueue_style ('bootstrap-responsive',WEBRITI_TEMPLATE_DIR_URI .'/css/bootstrap-responsive.css'); //boot rsp css
	wp_enqueue_style ('docs',WEBRITI_TEMPLATE_DIR_URI .'/css/docs.css'); //docs css
	
	/*Layout Change Css*/
	wp_enqueue_style ('layout-responsive',WEBRITI_TEMPLATE_DIR_URI.'/css/switcher/layout-responsive.css');
	
	/*Flex Slider Css */
	wp_enqueue_style ('flex_css',WEBRITI_TEMPLATE_DIR_URI.'/css/flex_css/flexslider.css');// Flex Slider CSS
	
	/*Style Media Css*/
	wp_enqueue_style ('style-media',WEBRITI_TEMPLATE_DIR_URI .'/css/style-media.css'); //Style-Media
			
	//Template Color Scheme Js	
	wp_enqueue_script('bootstrap',WEBRITI_TEMPLATE_DIR_URI.'/js/menu/bootstrap.min.js',array('jquery'));
	wp_enqueue_script('Bootstrap-transtiton',WEBRITI_TEMPLATE_DIR_URI.'/js/menu/menu.js');
	
	wp_enqueue_script('Bootstrap-transtiton',WEBRITI_TEMPLATE_DIR_URI.'/js/bootstrap-transition.js');
	/*Color Schemes*/
	
	wp_enqueue_script('switcher',WEBRITI_TEMPLATE_DIR_URI.'/js/color_scheme/switcher.js');	
	wp_enqueue_script('spectrum',WEBRITI_TEMPLATE_DIR_URI.'/js/color_scheme/spectrum.js');
	
	/*Flex Slider JS*/
	wp_enqueue_script('flexjs',WEBRITI_TEMPLATE_DIR_URI.'/js/flex_slider/jquery.flexslider.js');
	
	//CLient-Strip Slides JS
	wp_enqueue_script('caro',WEBRITI_TEMPLATE_DIR_URI.'/js/carufredsel/jquery.carouFredSel-6.0.4-packed.js');
	
	/****** prettyPhoto for portfolio ******/
	wp_enqueue_style ('prettyPhotocss',WEBRITI_TEMPLATE_DIR_URI.'/css/lightbox/prettyPhoto.css'); // font css
	wp_enqueue_script('prettyPhoto',WEBRITI_TEMPLATE_DIR_URI.'/js/lightbox/jquery.prettyPhoto.js');
	wp_enqueue_script('lightbox',WEBRITI_TEMPLATE_DIR_URI.'/js/lightbox/lightbox.js');
	
	/******* webriti tab js*********/
	wp_enqueue_script('webriti-tab-js',WEBRITI_TEMPLATE_DIR_URI.'/js/webriti-tab-js.js');
	require_once('custom_style.php');	
	}
	add_action( 'wp_enqueue_scripts', 'rambo_scripts' );
	
	function footer_custom_script()
	{
	$rambo_pro_theme_options = theme_data_setup();
	$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
	if($current_options['theme_color_enable'] == true) {
	custom_light();
	}
	}
	add_action('wp_footer','footer_custom_script');	

	add_action( 'admin_enqueue_scripts', 'admin_enqueue_script_function' );
	function admin_enqueue_script_function()
	{
		
		global $wp_customize;
		if ( isset( $wp_customize ) ) {
		wp_enqueue_script('rambo-jquery-ui-drag' , WEBRITI_TEMPLATE_DIR_URI.'/js/layout-drag-drop.js');
		}
	wp_enqueue_style( 'jquery-ui' );
	wp_enqueue_style('rambo-drag-drop',WEBRITI_TEMPLATE_DIR_URI.'/css/drag-drop.css');
	
	}
	
	function rambo_registers() {
}
add_action( 'customize_controls_enqueue_scripts', 'rambo_registers' );
	
?>