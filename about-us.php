<?php
//Template Name:		About Us 
/*	* @Theme Name	:	Rambopro
	* @file         :	about-us.php
	* @package      :	Rambopro
	* @license      :	license.txt
	* @filesource   :	wp-content/themes/prambo-pro/about-us.php
*/
	get_template_part('banner','strip');
	$rambo_pro_theme_options = theme_data_setup();
	$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
	$rambo_testimonial_speed = $current_options['rambo_testimonial_speed'];
	the_post();	
?>	
<script>
// Testimonial Js	
jQuery(function() {
	
	jQuery('#testimonial').carousel({
		              interval: <?php echo $current_options['rambo_testimonial_speed']; ?>,
					  pause: "hover",
					})
	jQuery('#testimonial').each(function(){
		
		
	  var next = jQuery(this).next();
	  if (!next.length) {
		next = jQuery(this).siblings(':first');
	  }
	  next.children(':first-child').clone().appendTo(jQuery(this));
	  
	  for (var i=0;i<2;i++) {
		next=next.next();
		if (!next.length) {
			next = jQuery(this).siblings(':first');
		}

    next.children(':first-child').clone().appendTo(jQuery(this));
  }
		  
	});
					
});	
	
	
</script>
<!-- Container -->
<div class="container ">
	<?php if($current_options['aboutus_content_with_image'] == true) { ?>
	<div class="row about_section">
		<div class="span12">
			<?php 
			if( isset( $current_options['about_page_title'] ) && $current_options['about_page_title'] != '' ) { ?>
			<div class="head_title">
				<h3><?php echo esc_html($current_options['about_page_title']); ?></h3>
			</div>
			<?php } ?>
		</div>
			<p><?php the_content(); ?></p>
	</div>
	<?php } ?>
	
	<?php 
	if($current_options['aboutus_our_team_enabled'] == true) {

	$team_options  = ! empty($current_options['rambo_team_content']) ? $current_options['rambo_team_content'] : '';
	$ThemeData = get_option('rambo_pro_theme_options');
	if(empty($team_options))
	{
		if (!empty($current_options['slider_category'])){
			// Set default data from old pro theme
			$count_posts = wp_count_posts( 'rambopro_team')->publish;
			$arg = array( 'post_type' => 'rambopro_team','posts_per_page' =>$count_posts);
			$team = new WP_Query( $arg );
			$i=1;
			if($team->have_posts())
			{	while ( $team->have_posts() ) : $team->the_post();			
				$pro_team_data_old[] = array(
					'title'      => get_the_title(),
					'subtitle'   => get_post_meta( get_the_ID(), 'team_designation', true),
					'text'       => get_post_meta( get_the_ID(), 'team_role', true ),
					'link'       => get_post_meta( get_the_ID(), 'team_link', true ),
					'image_url'  => get_the_post_thumbnail_url(),
					'open_new_tab' => get_post_meta( get_the_ID(), 'meta_team_target', true ),
					'id'    => 'customizer_repeater_56d7ea7f40t754',
					);
				endwhile;
			$team_options = json_encode($pro_team_data_old);
						
			}
			
		}else{
		
		$team_options = json_encode( array(
					array(
					'image_url'  => get_template_directory_uri().'/images/team/team1.jpg',
					'title'           => 'Michael Clarke1',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'Founder', 'quality' ),
					'open_new_tab' => 'no',
					'id'              => 'customizer_repeater_56d7ea7f40c56',
				),
				array(
					'image_url'  => get_template_directory_uri().'/images/team/team2.jpg',
					'title'           => 'Paul Walker',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'UI Designer', 'quality' ),
					'open_new_tab' => 'no',
					'id'              => 'customizer_repeater_56d7ea7f40c66',
				),
				array(
					'image_url'  => get_template_directory_uri().'/images/team/team3.jpg',
					'title'           => 'Chris Gayle',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'Php Developer', 'quality' ),
					'open_new_tab' => 'no',
					'id'              => 'customizer_repeater_56d7ea7f40c76',
				),
				array(
					'image_url'  => get_template_directory_uri().'/images/team/team4.jpg',
					'title'           => 'Laura Michelle',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'Designer', 'quality' ),
					'open_new_tab' => 'no',
					'id'        => 'customizer_repeater_56d7ea7f40c86',
				),
				
				
				
				) );
	}
			
	}

	?>
		<?php if( isset( $current_options['our_team_title'] ) && $current_options['our_team_title'] != '' ) { ?>
		<div class="team_head_title">
			<h3><?php echo $current_options['our_team_title']; ?></h3>
		</div>	
		<?php } ?>
		
		<div class="row team_section">
		<?php  
		
		$team_options = json_decode($team_options);
					if( $team_options!='' )
					{
					foreach($team_options as $team_item){
					$image    = ! empty( $team_item->image_url ) ? apply_filters( 'rambo_translate_single_string', $team_item->image_url, 'Team section' ) : '';
					$title    = ! empty( $team_item->title ) ? apply_filters( 'rambo_translate_single_string', $team_item->title, 'Team section' ) : '';
					$text = !empty($team_item->text) ? apply_filters('rambo_translate_single_string', $team_item->text, 'Team section' ): '';
					$subtitle = ! empty( $team_item->subtitle ) ? apply_filters( 'rambo_translate_single_string', $team_item->subtitle, 'Team section' ) : '';
					$link     = ! empty( $team_item->link ) ? apply_filters( 'rambo_translate_single_string', $team_item->link, 'Team section' ) : '';
					$open_new_tab = $team_item->open_new_tab;
		
		
		
			?>
				<div class="span3">
					<div class="thumbnail team_bg">
					     <?php if ( ! empty( $image ) ) : ?>
										<?php
										if ( ! empty( $link ) ) :
											$link_html = '<a class="attachment-post-thumbnail size-post-thumbnail wp-post-image" href="' . esc_url( $link ) . '"';
											if ( function_exists( 'rambo_is_external_url' ) ) {
												$link_html .= rambo_is_external_url( $link );
											}
											$link_html .= '>';
											echo wp_kses_post( $link_html );
										endif;
										echo '<img class="img" src="' . esc_url( $image ) . '"';
										if ( ! empty( $title ) ) {
											echo 'alt="' . esc_attr( $title ) . '" title="' . esc_attr( $title ) . '"';
										}
										echo '/>';
										if ( ! empty( $link ) ) {
											echo '</a>';
										}
										?>
						<?php endif; ?>
						<div class="caption">
					
					<?php if ( ! empty( $link ) ) : ?>
						<h4 class="text-center"><a href="<?php echo $link ?>" <?php if($open_new_tab == 'yes'){ echo 'target="_blank"';}?>> <?php echo $title; ?></a>
										<small><?php echo $subtitle; ?></small>
						</h4>
					<?php else: ?>	

						<h4 class="text-center"><a><?php echo $title; ?> </a>
										<small><?php echo $subtitle; ?></small>
						</h4>
					<?php endif; ?>
					   
						
						<p class="text-center">
						<?php echo $text; ?>							
						</p>
					</div>
					</div>
				</div>
			<?php }
			} ?>
		</div>
	<?php } ?>
	
	<!-- Testimonial -->
	<?php if($current_options['aboutus_testimonial_enabled'] == true) { ?>
		
		<?php if( isset( $current_options['testimonials_title'] ) && $current_options['testimonials_title'] != '') { ?>
		<div class="team_head_title">
		<h3><?php echo $current_options['testimonials_title']; ?></h3>
		</div>
		<?php }
		
		
		
		$ThemeData = get_option('rambo_pro_theme_options');
		$TestimonialData  = ! empty($current_options['rambo_testimonial_content']) ? $current_options['rambo_testimonial_content'] : '';
	
	if(empty($TestimonialData))
	{
		if (!empty($current_options['slider_category'])){
			// Set default data from old pro theme
		
			
			$query_args = array(
		'ignore_sticky_posts' => 1, 
		'tax_query' => array( array( 'taxonomy' => 'post_format', 'field' => 'slug','terms' => array( 'post-format-quote' ) ))
		);
		$testi_loop = new WP_Query($query_args); 
		$i=1;
		if($testi_loop->have_posts())
			{
		while ( $testi_loop->have_posts() ) : $testi_loop->the_post();
		$pro_testimonial_old[] = array(
					'title'      => get_the_title(),
					'subtitle'   => get_post_meta( get_the_ID(), 'testimonial_designation', true),
					'text'       => get_the_content(),
					'link'       => '#',
					'image_url'  => get_the_post_thumbnail_url(),
					'open_new_tab' => 'no',
					'id'    => 'customizer_repeater_56d7ea7f40t754',
					);
		endwhile;
			$TestimonialData = json_encode($pro_testimonial_old);
		} }
		 else{
		
		$TestimonialData = json_encode( array(
					array(
					'title'      => 'Natalie Doe',
					'text'       => 'Sed ut Perspi ciatis Unde Omnis Iste Sed ut perspi ciatis unde omnis iste natu error sit volup, tatem accu tium neque ferme ntum veposu miten a tempor nise. Consec tetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.',
					'subtitle' => __('Founder','rambo'),
					'link'       => '#',
					'image_url'  => get_template_directory_uri().'/images/default/testi/testimonial1.png',
					'open_new_tab' => 'no',
					'id'         => 'customizer_repeater_56d7ea7f40b96',
					
					),
					array(
					'title'      => 'Bella Swan',
					'text'       => 'Sed ut Perspiciatis Unde Omnis Iste Sed ut perspiciatis unde omnis iste natu error sit voluptatem accu tium neque fermentum veposu miten a tempor nise. Duis autem vel eum iriure dolor in hendrerit in vulputate velit consequat reprehender in voluptate velit esse cillum duis dolor fugiat nulla pariatur.',
					'subtitle' => __('Manager','rambo'),
					'link'       => '#',
					'image_url'  => get_template_directory_uri().'/images/default/testi/testimonial2.png',
					'open_new_tab' => 'no',
					'id'         => 'customizer_repeater_56d7ea7f40b97',
					),
					array(
					'title'      => 'Jenifer Doe',
					'text'       => 'Sed ut Perspiciatis Unde Omnis Iste Sed ut perspiciatis unde omnis iste natu error sit voluptatem accu tium neque fermentum veposu miten a tempor nise. Duis autem vel eum iriure dolor in hendrerit in vulputate velit consequat reprehender in voluptate velit esse cillum duis dolor fugiat nulla pariatur.',
					'subtitle' => __('Designer','rambo'),
					'link'       => '#',
					'image_url'  => get_template_directory_uri().'/images/default/testi/testimonial3.png',
					'id'         => 'customizer_repeater_56d7ea7f40b98',
					'open_new_tab' => 'no',
					),
				) );
	}
	}	
	
		$TestimonialData = json_decode($TestimonialData);
		?>
		<div class="row main_space">
			<?php	if( $TestimonialData!='' )
					{
						?>
						<div class="row testimonial_section">
						<div id="testimonial" class="span12 carousel slide " data-ride="carousel" data-type="multi" >
						<div class="carousel-inner">
						<?php		
						$i=1;

						foreach($TestimonialData as $TeamIteam)
						{
							$image    = ! empty( $TeamIteam->image_url ) ? apply_filters( 'rambo_translate_single_string', $TeamIteam->image_url, 'Testimonial section' ) : '';
							$title    = ! empty( $TeamIteam->title ) ? apply_filters( 'rambo_translate_single_string', $TeamIteam->title, 'Testimonial section' ) : '';
							$text = !empty($TeamIteam->text) ? apply_filters('rambo_translate_single_string', $TeamIteam->text, 'Testimonial section' ): '';
							$subtitle = ! empty( $TeamIteam->subtitle ) ? apply_filters( 'rambo_translate_single_string', $TeamIteam->subtitle, 'Testimonial section' ) : '';
							$link     = ! empty( $TeamIteam->link ) ? apply_filters( 'rambo_translate_single_string', $TeamIteam->link, 'Testimonial section' ) : '';
							$open_new_tab = $TeamIteam->open_new_tab;
							?>
			
							<div class="item <?php if($i==1) { echo 'active';} $i++; ?>">		
								<div class="span12 testimonial_area">
									<blockquote class="style1"><span><?php echo $text; ?></span></blockquote>
									<div class="testimonial_author">
										<?php if( $image!='' ){ 
											echo "<img src='".$image."' class='img-responsive wp-post-image' >";
										} ?>
											<span>
											<?php if($link !=''){  ?>
											<a href="<?php echo $link; ?>" <?php if($open_new_tab == 'yes'){ echo 'target="_blank"';}?>> <?php echo $title; ?>  </a>
											<?php } else { ?>
											<?php echo $title; ?>
											<?php } ?>
											
										<?php if( $subtitle!='' ){ ?>
											<br/><small><?php echo $subtitle; ?></small>
										<?php } ?></span>
									</div>
								</div>
							</div>
							<?php
						}?>
						</div>
						</div>
						</div>
					<?php 
	
				} 
				?>			
		</div><?php
	
		/*else{
		
		
		if(is_active_sidebar('testimonial-widget-area')):?>
		
	
		<div class="row main_space">
		    <?php dynamic_sidebar('testimonial-widget-area'); ?>
        </div>
	
		
		<?php }*/
	}?>
	
	
	<!---- client strip enable ------>
	<?php if($current_options['aboutus_client_strip_enabled'] == true)
		{ get_template_part('client','strip');  } ?>	
</div>
<!-- /Container -->
<?php get_footer();?>