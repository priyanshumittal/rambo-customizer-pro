<?php
/*---------------------------------------------------------------------------------*
 * @file           theme_stup_data.php
 * @package        Rambopro
 * @copyright      2013 webriti
 * @license        license.txt
 * @author       :	webriti
 * @filesource     wp-content/themes/rambo-pro/theme_setup_data.php
 *	Admin  & front end defual data file 
 *-----------------------------------------------------------------------------------*/ 
function theme_data_setup()
{
	return $rambo_pro_theme_options = array(
			//Logo and Fevicon header			
			'layout_selector' => 'wide',
			'rambopro_stylesheet'=>'default.css',			
			'upload_image_logo'=>'',
			'theme_color_enable' => false,
			'theme_color' => '#DB2723',
			'height'=>'50',
			'width'=>'150',
			'rambo_texttitle'=>true,
			'upload_image_favicon'=>'',
			'google_analytics'=>'',
			'webrit_custom_css'=>'',
			
			// Site Intro Layout 
			'site_intro_column_layout' => 1,
			'site_intro_bottom_column_layout'=> 1,
			'site_intro_descritpion' => __('Rambo is a clean and fully responsive Template.','rambo'),
			'site_intro_button_text' =>  __('Purchase Now','rambo'),
			'site_intro_button_link' => '#',
			'intro_button_target' => true,
			// front page
			'front_page_data'=>'Call to action top,Service section,Project portfolio,Latest news,team,testimonial,shop,client,Call to action bottom',
			
			//Slide 	
			'home_slider_enabled'=>true,
			'slider_options' => 'slide',
			'slider_transition_delay' => 4500,
			'animation' => 'slide',								
			'animationSpeed' => '500',
			'slide_direction' => 'horizontal',
			'slideshowSpeed' => '2000',
			
			// service
			'home_service_enabled'=>false,
			'service_list' => 4,
			'service_section_title'=> __('Our Services','rambo'),
			'service_section_descritpion' => __('Check out our Main Services which we offer to every client','rambo'),
			'service_layout_section' => 1,
			'service_col_layout' => 3,
			
			//about us Settings
			'aboutus_content_with_image'=>true,
			'about_page_title' => 'Who we are',
			'aboutus_social_icon_enabled'=>true,
			
			//Client Strip Options
			'aboutus_client_strip_enabled' =>true,
			'service_client_strip_enabled' => true,			
			'client_strip_title' => 'Our Client',			
			'number_of_client'=> '5',
			'rambo_client_strip_speed'=> '2000',
			'rambo_client_home_speed'=> '2000',
			'rambo_testimonial_speed' => '2000',
			
			// Team settings
			'aboutus_our_team_enabled' => true,
			'our_team_title' =>'Meet the Team',
			
			// Testimonials
			'aboutus_testimonial_enabled' => true,
			'service_testimonial_enabled'=>true,
			'testimonials_title' =>'Testimonials',
			'testimonials_column_layout' => 3,
			'blog_meta_section_settings' => false,
			'archive_page_meta_section_settings' => false,
			
			// project 
			'project_protfolio_enabled'=>false,
			'project_protfolio_tag_line'=>__('Featured Portfolio Projects','rambo'),
			'project_protfolio_description_tag' =>'Maecenas sit amet tincidunt elit. Pellentesque habitant morbi tristique senectus et netus et Nulla facilisi',
			// home project 
			 'project_list'=>4,
			
			//home latest news
			'post_display_count' => 3,
			'news_enable' => false,
			'home_slider_post_enable' => true,
			'blog_section_head' =>'',
			'latest_news_tag_line'=>'Latest News',
			'home_news_style'=>1,
			'home_meta_section_settings' => false,
			
			// breadcrumbs setting
			'archive_prefix' => 'Archive',
			'category_prefix' => 'Category',
			'author_prefix' => 'Author',
			'tag_prefix' => 'Tag',
			'search_prefix' => 'Search results for',
			'404_prefix' => '404',
			'project_prefix' => 'Project categories',
			'shop_prefix' => 'shop',
		
			
			
			// site intro info 
			'site_info_enabled'=>true,
			'site_info_title'=> __('Rambo is a clean and fully responsive Template.','rambo'),
			'site_info_descritpion' =>'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos excepturi vehicula sem ut volutpat. Ut non libero magna fusce condimentum eleifend enim a feugiat.',
			'site_info_button_text'=> __('Purchase Now','rambo'),
			'site_info_button_link'=>'#',
			'site_info_button_link_target' => true,
			
			'home_shop_enable' => false,
			'home_shop_title' => __('Our Product','rambo'),
			'home_shop_desciption' => 'Check out our product',
			
			/** Client **/
			'homepage_client_hide' => true,
			'homepage_client_title' => __('Our Client', 'rambo'),
			'homepage_client_contents' => __('Check out our client','rambo'),
			
			/** footer customization **/
			'footer_copyright' => sprintf(__('Copyright @ 2024 - RAMBO. Designed by <a href="http://webriti.com" rel="nofollow" target="_blank"> Webriti</a>','rambo')),

			/* Footer social media */
			'footer_social_media_enabled'=>true,
			
			
			//Social media links
			'social_media_in_contact_page_enabled' => false,
			'social_media_twitter_link' =>"",
			'social_media_facebook_link' =>"",
			'social_media_linkedin_link' =>"",
			'social_media_google_plus' =>"",
			'social_media_youtube' =>"",
			'social_media_instagram' =>"",
			'social_media_skype' =>"",
			
			//contact page settings	
			'rambo_get_in_touch_enabled'=>true,
			'rambo_get_in_touch' =>'',
			'rambo_get_in_touch_description'=>'',
			
			'contact_form_heading' =>'Contact Form',
			'rambo_our_office_enabled'=>true,
			'rambo_our_office'=>'',
			'rambo_contact_address'=>'',
			'rambo_contact_phone_number'=>'',
			'rambo_contact_email'=>'',
			
			'service_temp_cta_enabled' =>true,
			'team_temp_clients_enabled' =>true,
			'team_temp_cta_enabled' =>true,
			'testimonial_temp_cta_enabled' =>true,
			'contact_google_map_enabled'=>true,
			'rambo_contact_google_map_shortcode' => 'https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Kota+Industrial+Area,+Kota,+Rajasthan&amp;aq=2&amp;oq=kota+&amp;sll=25.003049,76.117499&amp;sspn=0.020225,0.042014&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=Kota+Industrial+Area,+Kota,+Rajasthan&amp;z=13&amp;ll=25.142832,75.879538',
			
			'enable_custom_typography'=>false,
			
			// general typography			
			'general_typography_fontsize'=>'13',
			'general_typography_fontfamily'=>'Helvetica Neue,Helvetica,Arial,sans-serif',
			'general_typography_fontstyle'=>"normal",
			
			// menu title
			'menu_title_fontsize'=>18,
			'menu_title_fontfamily'=>'Roboto',
			'menu_title_fontstyle'=>"normal",
			
			// post title
			'post_title_fontsize'=>26,
			'post_title_fontfamily'=>'Roboto',
			'post_title_fontstyle'=> "normal",
			
			// page title
			'page_title_fontsize'=>30,
			'page_title_fontfamily'=>'Roboto',
			'page_title_fontstyle'=>"normal",
			
			// Service  title
			'service_title_fontsize'=>26,
			'service_title_fontfamily'=>'Roboto',
			'service_title_fontstyle'=>"normal",
			
			// Potfolio  title Widget Heading Title
			'portfolio_title_fontsize'=>20,
			'portfolio_title_fontfamily'=>'Roboto',
			'portfolio_title_fontstyle'=>"normal",
			
			// Widget Heading Title
			'widget_title_fontsize'=>24,
			'widget_title_fontfamily'=>'Roboto',
			'widget_title_fontstyle'=>"normal",
			
			// Call out area Title   
			'calloutarea_title_fontsize'=>34,
			'calloutarea_title_fontfamily'=>'Roboto',
			'calloutarea_title_fontstyle'=>"normal",
			
			// Call out area descritpion      
			'calloutarea_description_fontsize'=>15,
			'calloutarea_description_fontfamily'=>'Roboto',
			'calloutarea_description_fontstyle'=>"normal",
			
			// Call out area purches button      
			'calloutarea_purches_fontsize'=>16,
			'calloutarea_purches_fontfamily'=>'Roboto',
			'calloutarea_purches_fontstyle'=>"normal",
			
			//Callout Bottom
			'site_bottom_intro_title' => __('Rambo is a clean and fully responsive Template.','rambo'),
			'site_bottom_intro_des' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos excepturi vehicula sem ut volutpat. Ut non libero magna fusce condimentum eleifend enim a feugiat.',
			'site_intro_bottom_button_text' => __('Purchase Now','rambo'),
			'site_intro_bottom_button_link' => '#',
			'intro_bottom_button_target' => true,
			
			

            //Post Type slug Options
			'rambo_slider_slug' => 'rambo_slider',
			'rambo_portfolio_slug' => 'rambo_project',
			'rambo_team_slug' => 'rambo_team',

			//Taxonomy Archive Portfolio
			'taxonomy_portfolio_list'=>2,
			
			//New Data
			'slider_category' => '',
			
			//SErvice Layout
			'service_enable' => false,
			'service_column_layout'=> 4,
			
			//Project Layout
			'project_column_layout'  => 4,
			'portfolio_selected_category_id' => 2,

			//News Column Layout
			'$news_column_layout' => 3,
			
			//Additoional layout One
			'additional_enable_one' => false,
			'additional_title_one' => '',
			'additional_description_one' => '',
			'additionla_section_one_column_layout' => 4,
			
			//Team Section
			'team_section_enable' => false,
			'team_section_title' => __('Our Team','rambo'),
			'team_section_desc' => 'Maecenas sit amet tincidunt elit. Pellentesque habitant morbi tristique senectus et netus et Nulla facilisi.',
			'home_team_layout_section' => 1,
			'team_col_layout' => 4,
			//Additoional layout Two
			'additional_enable_two' => false,
			'additional_title_two' => '',
			'additional_description_two' => '',
			'additionla_section_two_column_layout' => 4,
			
			//Theme Color 
			'theme_color ' => '#db2723',

			//Header Preset
			'header_preset_section' => 1,
			'search_icon' => '',
			'search_icon_style' =>1,	

			//Testimonial		
			'testimonial_section_enable' => false,
			'testimonial_section_title' => 'What Clients Are Say',
			'testimonial_section_desc' => 'Maecenas sit amet tincidunt elit. Pellentesque habitant morbi tristique senectus et netus et Nulla facilisi.',
			'home_testimonial_layout_section' => 1,
			'testimonial_col_layout'=>6,
			'testi_background'=> get_template_directory_uri() . '/images/testimonial/bg.png',

		);
}
?>