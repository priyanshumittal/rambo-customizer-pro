<?php get_header(); 
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), theme_data_setup() );
?>
	  <div class="hero-unit-small">
	<div class="container">
		<div class="row-fluid about_space">
			<h2 class="page_head pull-left"><?php echo $current_options['project_prefix']; echo ' '; single_cat_title(); ?></h2>
			</a>
			<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<div class="input-append search_head pull-right">
				<input type="text"   name="s" id="s" placeholder="<?php esc_attr_e( "Search", 'rambo' ); ?>" />
				<button type="submit" class="Search_btn" name="submit" ><?php esc_attr_e( "Go", 'rambo' ); ?></button>
				</div>
			</form>
		</div>
	</div>
</div><div class="for_mobile">
	 <?php $rambo_pro_theme_options = theme_data_setup();
		$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
	  $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
?>

<div class="container">
<div class="tab-content main_portfolio_section">
	<!-- Portfolio 2 Column -->
	<?php 
			$norecord=0; $j=1;
			global $paged;
			$curpage = $paged ? $paged : 1;
			$total_post = wp_count_posts('rambopro_project')->publish;
			$posts_per_page = get_option('posts_per_page');
			
	 $args = array (
			'post_type' => $post_type,'paged'=>$paged,'portfolio_categories'=>$term->slug,'posts_per_page'=>$posts_per_page);
			$portfolio_query = null;
			$portfolio_query = new WP_Query($args);
			if(have_posts())
			{
			?>
			
			<div class="row portfolio_section">
			<?php	while ( have_posts() ) : the_post();
				 if($current_options['taxonomy_portfolio_list']==2) {
						if(has_post_thumbnail()) {
							$post_thumbnail_id = get_post_thumbnail_id();
							$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id ); }
						if(get_post_meta( get_the_ID(),'portfolio_project_link', true )) 
						{ $portfolio_project_link = get_post_meta( get_the_ID(),'portfolio_project_link', true ); }
					else 
					{ $portfolio_project_link = get_post_permalink(); } 
			?>
				<div class="span6 portfolio_column">		
					<div  class="portfolio_showcase">
						<div class="portfolio_showcase_media">				
							<?php the_post_thumbnail('portfolio-2c-thumb'); ?>							
							<div class="portfolio_showcase_overlay">
								<div class="portfolio_showcase_overlay_inner">
									<h2><a href="<?php echo $portfolio_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'portfolio_project_target', true )) { echo "target='_blank'"; }  ?> title="Rambo"><?php the_title(); ?></a></h2>
									<div class="portfolio_showcase_icons">
										<a href="<?php echo $portfolio_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'portfolio_project_target', true )) { echo "target='_blank'"; }  ?> title="Rambo"><i class="fa fa-link"></i></a>
										<a class="hover_thumb" rel="lightbox[group]" href="<?php echo $post_thumbnail_url; ?>"><i class="fa fa-eye"></i></a>
									</div>
								</div>
							</div>				
							
						</div>
					</div>
					<div class="portfolio_caption">
						<h3><a href="<?php echo $portfolio_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'portfolio_project_target', true )) { echo "target='_blank'"; }  ?>><?php the_title(); ?></a></h3>
						<?php $portfolio_client_project_title =sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_client_project_title', true )); ?>
						<small><?php if($portfolio_client_project_title)
							{ echo $portfolio_client_project_title; }else {  echo "Photography";} ?></small>	
					</div>
				</div>
				  
			<?php  } ?>
			
			<?php
				 if($current_options['taxonomy_portfolio_list']==3) {
						if(has_post_thumbnail()) {
							$post_thumbnail_id = get_post_thumbnail_id();
							$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id ); }
						if(get_post_meta( get_the_ID(),'portfolio_project_link', true )) 
						{ $portfolio_project_link = get_post_meta( get_the_ID(),'portfolio_project_link', true ); }
					else 
					{ $portfolio_project_link = get_post_permalink(); } 
			?>
				<div class="span4 portfolio_column">		
					<div  class="portfolio_showcase">
						<div class="portfolio_showcase_media">				
							<?php the_post_thumbnail('portfolio-2c-thumb'); ?>							
							<div class="portfolio_showcase_overlay">
								<div class="portfolio_showcase_overlay_inner">
									<h2><a href="<?php echo $portfolio_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'portfolio_project_target', true )) { echo "target='_blank'"; }  ?> title="Rambo"><?php the_title(); ?></a></h2>
									<div class="portfolio_showcase_icons">
										<a href="<?php echo $portfolio_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'portfolio_project_target', true )) { echo "target='_blank'"; }  ?> title="Rambo"><i class="fa fa-link"></i></a>
										<a class="hover_thumb" rel="lightbox[group]" href="<?php echo $post_thumbnail_url; ?>"><i class="fa fa-eye"></i></a>
									</div>
								</div>
							</div>				
							
						</div>
					</div>
					<div class="portfolio_caption">
						<h3><a href="<?php echo $portfolio_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'portfolio_project_target', true )) { echo "target='_blank'"; }  ?>><?php the_title(); ?></a></h3>
						<?php $portfolio_client_project_title =sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_client_project_title', true )); ?>
						<small><?php if($portfolio_client_project_title)
							{ echo $portfolio_client_project_title; }else {  echo "Photography";} ?></small>	
					</div>
				</div>
				  
			<?php  } ?>
			
			<?php
				 if($current_options['taxonomy_portfolio_list']==4) {
						if(has_post_thumbnail()) {
							$post_thumbnail_id = get_post_thumbnail_id();
							$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id ); }
						if(get_post_meta( get_the_ID(),'portfolio_project_link', true )) 
						{ $portfolio_project_link = get_post_meta( get_the_ID(),'portfolio_project_link', true ); }
					else 
					{ $portfolio_project_link = get_post_permalink(); } 
			?>
				<div class="span3 portfolio_column">		
					<div  class="portfolio_showcase">
						<div class="portfolio_showcase_media">				
							<?php the_post_thumbnail('portfolio-2c-thumb'); ?>							
							<div class="portfolio_showcase_overlay">
								<div class="portfolio_showcase_overlay_inner">
									<h2><a href="<?php echo $portfolio_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'portfolio_project_target', true )) { echo "target='_blank'"; }  ?> title="Rambo"><?php the_title(); ?></a></h2>
									<div class="portfolio_showcase_icons">
										<a href="<?php echo $portfolio_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'portfolio_project_target', true )) { echo "target='_blank'"; }  ?> title="Rambo"><i class="fa fa-link"></i></a>
										<a class="hover_thumb" rel="lightbox[group]" href="<?php echo $post_thumbnail_url; ?>"><i class="fa fa-eye"></i></a>
									</div>
								</div>
							</div>				
							
						</div>
					</div>
					<div class="portfolio_caption">
						<h3><a href="<?php echo $portfolio_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'portfolio_project_target', true )) { echo "target='_blank'"; }  ?>><?php the_title(); ?></a></h3>
						<?php $portfolio_client_project_title =sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_client_project_title', true )); ?>
						<small><?php if($portfolio_client_project_title)
							{ echo $portfolio_client_project_title; }else {  echo "Photography";} ?></small>	
					</div>
				</div>
				  
			<?php  }  ?>
			<?php $j++; $norecord=1; endwhile; ?>
			</div>
			<?php 
			$Webriti_pagination = new Webriti_pagination();
		    $Webriti_pagination->Webriti_page($curpage, $portfolio_query);
			?>
		<?php }  ?>
	</div>
		
	<!-- /Portfolio 2 Column -->		
</div>
</div>
<!-- /Container -->	
<?php get_footer(); ?>