<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head> 
	<meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">  
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>" charset="<?php bloginfo('charset'); ?>" />
	<meta name="generator" content="WordPress <?php bloginfo('version'); ?>"/>
	<?php 
	$rambo_pro_theme_options = theme_data_setup();
	$rambopro_current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
	?>	
	<?php if($rambopro_current_options['theme_color_enable'] != 'on'){if($rambopro_current_options['rambopro_stylesheet']!='')
		{ $rambo_css=$rambopro_current_options['rambopro_stylesheet']; } else { $rambo_css="default.css"; } }?>
	<link rel="stylesheet" href="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/css/<?php echo $rambo_css; ?>" type="text/css" media="screen" />
	<?php 		
		if($rambopro_current_options['upload_image_favicon']!='')
		{ ?><link rel="shortcut icon" href="<?php  echo $rambopro_current_options['upload_image_favicon']; ?>" /> 
			<?php } else {?>	
			
		<?php } 
		wp_head();

		if ($rambopro_current_options['layout_selector']!='wide'){ $boxed= "boxed"; } else{$boxed='wide';}
		 ?>
</head>
<body <?php body_class('header-preset'.$rambopro_current_options['header_preset_section'] .' '. $boxed); ?> >
<div id="wrapper" <?php if ($rambopro_current_options['layout_selector']!='wide'){ echo "class=boxed"; } ?>>
<?php if( is_active_sidebar('home_header_sidebar_left') || is_active_sidebar('home_header_sidebar_center') || is_active_sidebar('home_header_sidebar_right') ) { ?>
	<!--Header Top-->
	<section class="top-header-widget">
		<div class="container">
			<div class="row">
				<div class="span4">
					<div id="top-header-sidebar-left">
						<?php if( is_active_sidebar('home_header_sidebar_left') ) { ?>
						<?php  dynamic_sidebar( 'home_header_sidebar_left' ); ?>
						<?php } ?>
					</div>	
				</div>
				
				<div class="span4">
					<div id="top-header-sidebar-center">
						<?php  if( is_active_sidebar('home_header_sidebar_center') ) { ?>
						<?php dynamic_sidebar( 'home_header_sidebar_center' ); ?>
						<?php } ?>
					</div>	
				</div>
				
				<div class="span4">
					<div id="top-header-sidebar-right">
						<?php  if( is_active_sidebar('home_header_sidebar_right') ) { ?>
						<?php dynamic_sidebar( 'home_header_sidebar_right' ); ?>
						<?php } ?>
					</div>	
				</div>
			</div>	
		</div>
	</section>
	<!--End of Header Top-->
<?php } 
get_template_part('section-variation/header-preset/header-preset'.$rambopro_current_options['header_preset_section']);
?>
    <div id="searchbar_fullscreen" <?php if($rambopro_current_options['search_icon_style']==1):?>class="bg-light"<?php endif;?>>
        <button type="button" class="close">×</button>
        <form method="get" id="searchform" autocomplete="off" class="search-form" action="<?php echo esc_url( home_url( '/' ));?>">
            <input type="search" class="search-field" placeholder="Search …" value="" name="s">
            <input type="submit" class="btn btn-primary" value="Search">
        </form> 
    </div>
<!--/Search Box Content-->
<div class="clearfix"></div>