<?php // Template Name: Blog Grid View ?>
<?php get_template_part('banner','strip');
$image_uri= WEBRITI_TEMPLATE_DIR_URI. '/images' ;
?>
<div class="container">
	<!-- Blog Section Content -->
	<div class="row-fluid blog-grid">
		<!-- Blog Main 2 -->
    <?php 
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array( 'post_type' => 'post','paged'=>$paged);		
				$post_type_data = new WP_Query( $args );
					while($post_type_data->have_posts()):
					$post_type_data->the_post();
					global $more;
					$more = 0;?>
		<div class="span6 Blog_main">
			<div class="blog_section2">
				<?php $defalt_arg =array('class' => "media-object blog_section2_img" )?>
					<?php if(has_post_thumbnail()):?>
				<a href="#" class="pull-left blog_pull_img2">
					<?php the_post_thumbnail('', $defalt_arg); ?>
				</a>
				<?php endif;?>
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<?php if($current_options['blog_meta_section_settings'] == '' ) {?>
				<div class="blog_section2_comment ">
					<a href="<?php echo esc_url( home_url('/') ); ?><?php echo esc_html(date( 'Y/m' , strtotime( get_the_date() )) ); ?>"><i class="fa fa-calendar icon-spacing"></i> <?php echo esc_html(get_the_date());?></a>
					<a href="<?php the_permalink(); ?>#respond"><i class="fa fa-comments icon-spacing"></i> <?php echo esc_html(get_comments_number());?></a>
					<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) );?>"><i class="fa fa-user icon-spacing"></i> By <?php the_author();?></a>
				</div>
				<?php } ?>
				<?php the_excerpt();?>
				<p class="tags_alignment">
					<?php $posttags = get_the_tags();?>
					<?php if($current_options['blog_meta_section_settings'] == '' ) {
						if(has_tag()){
						?>
					<span class="blog_tags"><i class="fa fa-tags"></i> 
						<a href="<?php the_permalink(); ?>"><?php the_tags('<b>'.__('Tags','rambo').'</b>');?></a>
					</span>
					<?php } }?>
					<a href="<?php the_permalink(); ?>" class="blog_section2_readmore pull-right"><?php _e('Read More','rambo'); ?></a>
				</p>
			</div>
      </div>
			<?php endwhile ?>
			
		
		</div>
		<div class="row-fluid">
			<?php	$Webriti_pagination = new Webriti_pagination();
					$Webriti_pagination->Webriti_page($paged, $post_type_data);		?>
			</div>
		<!-- /Blog Main 2 -->
	
	</div>
<?php get_footer();?>