<?php
//Template Name:	portfolio-3-column 
/*	* @Theme Name	:	Rambopro
	* @file         :	portfolio-3-column.php
	* @license      :	license.txt
	* @filesource   :	wp-content/themes/prambo-pro/portfolio-3-column.php
*/
get_template_part('banner','strip');
 ?>
<?php 
	error_reporting(0);
    $post_type = 'rambopro_project';
	$tax = 'portfolio_categories'; 
	$term_args=array( 'hide_empty' => true,'orderby' => 'id');
	$posts_per_page = 6;
	$tax_terms = get_terms($tax, $term_args);
	$defualt_tex_id = get_option('custom_texo_rambo');
	$j=1;
	if (isset($_GET['tab'])) {
    	$tab = $_GET['tab'];
	}
	if(isset($_GET['div']))
	{
		$tab=$_GET['div'];
	}
?>

	<div class="container">
		<div class="row">
					<div class="col-md-12 portfolio_tabs_section">
						<ul id="tabs" class="portfolio_tabs" role="tablist">
						<?php foreach ($tax_terms  as $tax_term) { ?>
							<li rel="tab" class="nav-item" ><span class="tab">
								<a id="tab-<?php echo rawurldecode($tax_term->slug); ?>" href="#<?php echo rawurldecode($tax_term->slug); ?>"  class="nav-link <?php if($tab==''){if($j==1){echo 'active';$j=2;}}else if($tab==rawurldecode($tax_term->slug)){echo 'active';}?>"><?php echo $tax_term->name; ?></a></span>
							</li>
						<?php } ?>
						</ul>
					</div>
				</div>
					<div align="center" id="myDiv" style="display:none;">
					<img id="loading-image" src="<?php echo WEBRITI_TEMPLATE_DIR_URI.'/images/loading_2.gif';  ?>"  />
				</div>
 
 
 
 
				<div id="content" class="tab-content" role="tablist">
				    <?php 
					global $paged;
					$curpage = $paged ? $paged : 1;
					$norecord=0;
					$is_active=true;
					if ($tax_terms) 
					{ 
						foreach ($tax_terms  as $tax_term)
						{
							$args = array (
							'post_type' => $post_type,
							'post_status' => 'publish',					
							'portfolio_categories' => $tax_term->slug,
							'posts_per_page' => $posts_per_page,
							'paged' => $curpage,
							);
							$portfolio_query = null;		
							$portfolio_query = new WP_Query($args);
							if( $portfolio_query->have_posts() ):?>
							<div id="<?php echo rawurldecode($tax_term->slug); ?>" class="tab-pane fade in <?php if($tab==''){if($is_active==true){echo 'active';}$is_active=false;}else if($tab==rawurldecode($tax_term->slug)){echo 'active';} ?>" role="tabpanel" aria-labelledby="tab-<?php echo rawurldecode($tax_term->slug); ?>">
								<div class="row portfolio_section">
									<?php while ($portfolio_query->have_posts()) : $portfolio_query->the_post();
											$portfolio_target = sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_target', true ));
											$project_link_chkbx = sanitize_text_field( get_post_meta( get_the_ID(), 'project_link_chkbx', true ));  
											if(get_post_meta( get_the_ID(),'project_more_btn_link', true )) 
											{ 
												$project_more_btn_link = get_post_meta( get_the_ID(),'project_more_btn_link', true );
											} 
											else 
											{
												$project_more_btn_link =get_permalink();
											} 

 
											echo '<div class="span4 portfolio_column">';?>
											<div class="portfolio_showcase">
												<div class="portfolio_showcase_media">
														<?php the_post_thumbnail('full',array('class'=>'img-responsive'));
														if(has_post_thumbnail())
														{ 
															$post_thumbnail_id = get_post_thumbnail_id();
															$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id );
														} 
														?>
 
												<div class="portfolio_showcase_overlay">
 
													<div class="portfolio_showcase_overlay_inner">
 														<h2><a href="<?php echo $portfolio_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'portfolio_project_target', true )) { echo "target='_blank'"; }  ?> title="Rambo"><?php the_title(); ?></a></h2>
														<div class="portfolio_showcase_icons">
 															<?php if(!empty($project_more_btn_link)) {?>
															<a href="<?php echo $project_more_btn_link;?>" <?php if(!empty($project_link_chkbx)){ echo 'target="_blank"'; } ?>  title="Rambo" class="hover_thumb"><i class="fa fa-link"></i></a>
															<?php } ?>
															<?php if(isset($post_thumbnail_url)){ ?>
															<a href="<?php echo $post_thumbnail_url; ?>"  class="hover_thumb" rel="lightbox[group]" title="<?php the_title(); ?>"><i class="fa fa-eye"></i></a>
															<?php } ?>
															
 
														</div>
 
													</div>
 
												</div>
												</div>	
												</div>	
														<div class="portfolio_caption">
						<h3><a href="<?php echo $portfolio_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'portfolio_project_target', true )) { echo "target='_blank'"; }  ?>><?php the_title(); ?></a></h3>
						<?php $portfolio_client_project_title =sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_client_project_title', true )); ?>
						<small><?php if($portfolio_client_project_title)
							{ echo $portfolio_client_project_title; }else {  echo "Photography";} ?></small>	
					</div>
 
 
											<?php echo '</div>'; ?>
										<?php $norecord=1;?>
									<?php endwhile; ?>
								</div>
									<?php
									$total = $portfolio_query->found_posts;
									$Webriti_pagination = new Webriti_pagination();
									$Webriti_pagination->Webriti_page($curpage, $portfolio_query,$total,$posts_per_page);	?>
							</div>
							<?php
							wp_reset_query();
							else:?>
							<div id="<?php echo rawurldecode($tax_term->slug); ?>" class="tab-pane fade in <?php if($tab==''){if($is_active==true){echo 'active';}$is_active=false;}else if($tab==rawurldecode($tax_term->slug)){echo 'active';} ?>"></div>
							<?php
							endif;
						}
					}
				    ?>	
				</div>
 
 
 
 
	</div>
	

<script type="text/javascript">
  jQuery('.lightbox').hide();jQuery('#lightbox').hide();
  jQuery(".tab .nav-link ").click(function(e){
   	  jQuery("#lightbox").remove();
        var h=decodeURI(jQuery(this).attr('href').replace(/#/, ""));
         var tjk="<?php the_title();?>";
           var str1 = tjk.replace(/\s+/g, '-').toLowerCase();
           var pageurl="<?php $structure = get_option( 'permalink_structure' ); if($structure=='') {echo get_permalink()."&tab=";}else{echo get_permalink()."?tab=";}?>"+h;
        jQuery.ajax({url:pageurl,beforeSend: function() {
            jQuery(".tab-content").hide();
            jQuery("#myDiv").show();
          },success: function(data){
              jQuery(".tab-content").show();
            jQuery('.lightbox').remove();
            jQuery('#lightbox').remove();
            jQuery('.footer-copyright-section').hide();
        jQuery('#wrapper').html(data);
    },complete:function(data){
        jQuery("#myDiv").hide();
    }
});
    if(pageurl!=window.location){
        window.history.pushState({path:pageurl},'',pageurl);
           }
    return false;
    });
</script>
<!-- /Container -->	
<?php get_footer(); ?>