<?php // Template Name: Blog list view ?>
<?php get_template_part('banner','strip');
$image_uri= WEBRITI_TEMPLATE_DIR_URI. '/images' ;
?>
<div class="container">
	<!-- Blog Section Content -->
	<div class="row-fluid">
		<!-- Blog Main -->
		<div class="span12 Blog_main blog-list-view-sidebar">
			<?php 
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array( 'post_type' => 'post','paged'=>$paged);		
				$post_type_data = new WP_Query( $args );
					while($post_type_data->have_posts()):
					$post_type_data->the_post();
					global $more;
					$more = 0;?>
			<div class="blog_section" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="media">
					<?php $defalt_arg =array('class' => "img-responsive blog_pull_img" )?>
					<?php if(has_post_thumbnail()):?>
					<a  href="<?php the_permalink(); ?>" class="pull-left blog_pull_img">
					<?php the_post_thumbnail('', $defalt_arg); ?>
					</a>
					<?php endif;?>
					<div class="media-body">
					<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
					<?php if($current_options['blog_meta_section_settings'] == '' ) {?>
					<span class="blog_tags"><h5><a class="author_link" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) );?>"><?php the_author();?></a><span><?php echo get_the_date('M j,Y');?></span></h5></span>
					<span class="blog_tags"><i class="fa fa-group"></i><?php the_category(',');?></span>
					<?php } ?>
					<?php  the_excerpt(); ?>
					<?php $posttags = get_the_tags();?>
					<p><?php if($posttags) { ?>
					<?php if($current_options['blog_meta_section_settings'] == '' ) {?>
						<span class="blog_tags">
							<i class="fa fa-tags"></i> 
							<a href="<?php the_permalink(); ?>"><?php the_tags('<b>'.__('Tags','rambo').'</b>');?></a>
						</span>	<?php } ?>
                        <?php } ?>							
						<a class="blog_section_readmore pull-right" href="<?php the_permalink(); ?>">
						<?php _e('Read More','rambo'); ?></a>
					</p>
					</div>
				</div>
			</div>
			<?php endwhile ?>
			<?php	$Webriti_pagination = new Webriti_pagination();
					$Webriti_pagination->Webriti_page($paged, $post_type_data);		?>
		</div>
	</div>
</div>
<?php get_footer();?>