<?php
/**
* @Theme Name	:	Rambopro
* @file         :	index-project.php
* @license      :	license.txt
* @filesource   :	wp-content/themes/rambopro/index-project.php
*/
$rambo_pro_theme_options = theme_data_setup();
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
$project_column_layout = 12 / $current_options['project_column_layout'];
if($current_options['project_protfolio_enabled']==false)
{
?>
<!-- Recent Work Section -->
<div class="portfolio_main_content">	
	<div class="container">	
	<?php if($current_options['project_protfolio_tag_line'] || $current_options['project_protfolio_description_tag'] ) { ?>
	
		<div class="row-fluid featured_port_title">
			
			<?php if($current_options['project_protfolio_tag_line']!='') { ?>
				<h1><?php echo $current_options['project_protfolio_tag_line']; ?></h1>
			<?php } ?>
			
			<?php if($current_options['project_protfolio_description_tag']!='') { ?>
				<p><?php echo $current_options['project_protfolio_description_tag']; ?></p>
			<?php } ?>
			
		</div>
	<?php } ?>
	<div id="sidebar-project " class="row sidebar-project">
	<?php
$rambo_pro_theme_options = theme_data_setup();
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
$j=1;
		$total_project = $current_options['project_list'];
        $args = array( 'post_type' => 'rambopro_project',
					'tax_query' => array(
									array(
										'taxonomy' => 'portfolio_categories',
										'field'    => 'id',
										'terms'    => $current_options['portfolio_selected_category_id'],
										//'operator' => 'NOT IN',
									),
		) );
		
		$rambopro_project = new WP_Query( $args );
		if( $rambopro_project->have_posts() )
		{	
		while ( $rambopro_project->have_posts() ) : $rambopro_project->the_post();
		?>
			<div class="span<?php echo $project_column_layout; ?> featured_port_projects">
				<div class="thumbnail">
						<?php if(has_post_thumbnail()):?>
						<?php the_post_thumbnail(); ?>
						<?php endif;?>
						<?php if(get_post_meta( get_the_ID(),'portfolio_project_link', true )) 
							{ $portfolio_project_link=get_post_meta( get_the_ID(),'portfolio_project_link', true ); }
							else { $portfolio_project_link = get_post_permalink(); } 
						?>
					  <div class="featured_service_content">
						<h3><a href="<?php echo $portfolio_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'portfolio_project_target', true )) { echo "target='_blank'"; }  ?> ><?php echo the_title(); ?></a></h3>
						<p><?php echo substr(get_the_excerpt(), 0,100)." ..."; ?></p>
						<p><a class="featured_port_projects_btn pull-right" href="<?php echo $portfolio_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'portfolio_project_target', true )) { echo "target='_blank'"; }  ?> ><?php _e('Read More','rambo')?></a></p>
					  </div>
				</div>
			</div>
			<?php $j++; endwhile;
			} ?>
		
	</div>
</div>	
</div>
<?php } ?>	
<!-- /Recent Work Section -->	