<?php
/**
* @Theme Name	:	Rambopro
* @file         :	index-theme-introdunction.php
* @license      :	license.txt
* @filesource   :	wp-content/themes/rambopro/index-theme-introdunction.php
*/
$rambo_pro_theme_options = theme_data_setup();
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
if($current_options['site_info_enabled']==true)
{ 
?>

<div class="callout_main_content">

	<div class="container">
		<?php
		$oldData = get_option( 'rambo_pro_theme_options');
		if(isset($oldData['site_bottom_intro_title']))
		{ 
	?>
			<div class="row-fluid callout_now_content">
			
				<div class="span8">
					
					<?php if( $current_options['site_bottom_intro_title'] != '' ) { ?>
						<h1><?php echo $current_options['site_bottom_intro_title']; ?></h1>
					<?php } ?>
					
					<?php if($current_options['site_bottom_intro_des']!='') { ?>
						<p><?php echo $current_options['site_bottom_intro_des']; ?></p>
					<?php } ?>
					
				</div>
				
				<?php if($current_options['site_intro_bottom_button_text']!='') { ?>
				<div class="span3 offset1">
					<a class="callout_now_btn" href="<?php if($current_options['site_intro_bottom_button_link']!='') { echo $current_options['site_intro_bottom_button_link']; } else { echo "#"; } ?>" <?php if($current_options['intro_bottom_button_target']==true) { echo  "target='_blank'"; } ?> ><?php echo $current_options['site_intro_bottom_button_text']; ?></a>
				</div>
				<?php } ?>
				
			</div>	
		<?php 	
		} 
	    elseif(is_active_sidebar('site-intro-area-bottom') && get_option( 'rambo_pro_theme_options' ) != null) { 

			echo '<div class="row callout_now_content">';
			dynamic_sidebar( 'site-intro-area-bottom' );
			echo '</div>';
			
		}else{
			?>
			<div class="row-fluid callout_now_content">
			
				<div class="span8">
					
					<?php if( $current_options['site_bottom_intro_title'] != '' ) { ?>
						<h1><?php echo $current_options['site_bottom_intro_title']; ?></h1>
					<?php } ?>
					
					<?php if($current_options['site_bottom_intro_des']!='') { ?>
						<p><?php echo $current_options['site_bottom_intro_des']; ?></p>
					<?php } ?>
					
				</div>
				
				<?php if($current_options['site_intro_bottom_button_text']!='') { ?>
				<div class="span3 offset1">
					<a class="callout_now_btn" href="<?php if($current_options['site_intro_bottom_button_link']!='') { echo $current_options['site_intro_bottom_button_link']; } else { echo "#"; } ?>" <?php if($current_options['intro_bottom_button_target']==true) { echo  "target='_blank'"; } ?> ><?php echo $current_options['site_intro_bottom_button_text']; ?></a>
				</div>
				<?php } ?>
				
			</div>	
		<?php	
		}
		
	?>
	</div>
	
</div>
<?php } ?>
<!-- /Callout Section -->	