<?php
/**
* @Theme Name	:	Rambopro
*/
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), theme_data_setup() );
get_template_part('section-variation/news/news-'.$current_options['home_news_style']);
?>