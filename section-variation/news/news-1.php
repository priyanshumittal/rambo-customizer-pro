<?php
$rambo_pro_theme_options = theme_data_setup();
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
if( $current_options['news_enable'] == false )
{
?>
<div class="container">

    <?php if( $current_options['latest_news_tag_line'] != '' ) { ?>
<div class="row-fluid">
	<div class="team_head_title">
		<h3><?php echo $current_options['latest_news_tag_line']; ?></h3>
	</div>
</div>
<?php } ?>

<div class="row">
	<?php 	
	$no_of_post = $current_options['post_display_count'];	
	$args = array( 'post_type' => 'post','posts_per_page' =>$no_of_post,'post__not_in'=>get_option("sticky_posts")); 	
	
		query_posts( $args );
		if(query_posts( $args ))
		{	
		 $i=1;
		while(have_posts()):the_post();
			{ ?>		
				<div class="span4 latest_news_section">		
				<?php $defalt_arg =array('class' => "img-responsive latest_news_img");?>
					<?php if(has_post_thumbnail()){ ?>
						<a href="<?php the_permalink(); ?>" >
						<?php the_post_thumbnail('',$defalt_arg);?></a>
						<?php } ?>
					<h3><a class="home-news-var1" href="<?php the_permalink(); ?>"><?php the_title() ;?></a></h3>
					<p><?php  echo get_the_excerpt(); ?></p>

					<?php if($current_options['home_meta_section_settings'] == false){ ?>
					<div class="latest_news_comment">
						<!--<a class="pull-left" href="#"><i class="fa fa-calendar icon-spacing"></i><?php //echo get_the_date('M j,Y');?></a> -->
						<a class="pull-left" href="<?php the_permalink(); ?>"><i class="fa fa-calendar icon-spacing"></i><?php the_time('M j,Y');?></a>
						<a class="pull-right" href="<?php comments_link(); ?>"><i class="fa fa-comment icon-spacing"></i><?php echo get_comments_number();?></a>
					</div>
					<?php }?>
				</div>
				<?php 
			  $i++;
			  wp_reset_postdata();
			} endwhile;  ?>	
	<?php 	
		} ?>
	</div>
		


</div>	
<?php } ?>