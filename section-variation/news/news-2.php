<?php
$rambo_pro_theme_options = theme_data_setup();
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
if( $current_options['news_enable'] == false )
{
?>
<div class="container">
	<!-- Blog Section Content -->
	<?php if( $current_options['latest_news_tag_line'] != '' ) { ?>
<div class="row-fluid">
	<div class="team_head_title">
		<h3><?php echo $current_options['latest_news_tag_line']; ?></h3>
	</div>
</div>
<?php } ?>
  
	<div class="row-fluid rotate-list-view">
		<!-- Blog Main -->
		<?php 	
	$no_of_post = $current_options['post_display_count'];	
	$args = array( 'post_type' => 'post','posts_per_page' =>$no_of_post,'post__not_in'=>get_option("sticky_posts")); 	
	
		query_posts( $args );
		if(query_posts( $args ))
		{	
		 $i=1;
		while(have_posts()):the_post();
			{ ?>
		<div class="span12 Blog_main">
			<?php $defalt_arg =array('class' => "media-object blog_section_img");?>
			<div class="blog-section">
				<?php
				if($i % 2 == 0)
				{
				?>
<div class="span7">
					<div class="blog_section">
						<?php if($current_options['home_meta_section_settings'] == false){ ?>
						<span class="blog_tags"><h5><?php echo get_the_author();?> <span><?php the_time('M j,Y');?></span></h5></span>
						<?php if ( has_category() ) : ?>
						<span class="blog_tags"><i class="fa fa-group"></i> 
							<?php the_category( ' ' );?>
						</span>
						<?php endif; }?>
		      			<h2><a href="<?php the_permalink(); ?>"><?php the_title() ;?></a></h2>
						<p><?php  echo get_the_excerpt(); ?></p>
						<p><a href="<?php the_permalink(); ?>" class="blog_section_readmore">Read more...</a></p>
					</div>
				</div>
<?php if(has_post_thumbnail()){ ?>
				<div class="span5">
					<a href="<?php the_permalink(); ?>" class="left blog_pull_img">
						<?php the_post_thumbnail('',$defalt_arg);?>
					</a>
				</div>
			<?php } ?>
				<?php	
				}
				else
				{
?>






				<?php if(has_post_thumbnail()){ ?>
				<div class="span5">
					<a href="<?php the_permalink(); ?>" class="left blog_pull_img">
						<?php the_post_thumbnail('',$defalt_arg);?>
					</a>
				</div>
			<?php } ?>

				<div class="span7">
					<div class="blog_section">
						<?php if($current_options['home_meta_section_settings'] == false){ ?>
						<span class="blog_tags"><h5><?php echo get_the_author();?> <span><?php the_time('M j,Y');?></span></h5></span>
						<?php if ( has_category() ) : ?>
						<span class="blog_tags"><i class="fa fa-group"></i> 
							<?php the_category( ' ' );?>
						</span>
						<?php endif; }?>
		      			<h2><a href="<?php the_permalink(); ?>"><?php the_title() ;?></a></h2>
						<p><?php  echo get_the_excerpt(); ?></p>
						<p><a href="<?php the_permalink(); ?>" class="blog_section_readmore">Read more...</a></p>
					</div>
				</div>
<?php
				}
				?>
				
			</div>
		</div>
<?php 
			
			  $i++;
			  wp_reset_postdata();
			} endwhile;  ?>	
	<?php 	
		} ?>

	</div>

	
	<!-- /Blog Section Content -->	
		
</div>	
<?php } ?>