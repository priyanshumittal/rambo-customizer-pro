<?php
$rambo_pro_theme_options = theme_data_setup();
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
if( $current_options['news_enable'] == false )
{
?>
<div class="container home-news-4">
<?php if( $current_options['latest_news_tag_line'] != '' ) { ?>
<div class="row-fluid">
	<div class="team_head_title">
		<h3><?php echo $current_options['latest_news_tag_line']; ?></h3>
	</div>
</div>
<?php } ?>

<div class="row-fluid">
<?php 	
	$no_of_post = $current_options['post_display_count'];	
	$args = array( 'post_type' => 'post','posts_per_page' =>$no_of_post,'post__not_in'=>get_option("sticky_posts")); 	
	
		query_posts( $args );
		if(query_posts( $args ))
		{	
		 $i=1;
		while(have_posts()):the_post();
			{ ?>
<div class="span4 Blog_main">
	<?php $defalt_arg =array('class' => "pull-left blog_pull_img2");?>
<div class="blog_section2">
<?php if(has_post_thumbnail()){ ?>
					<a href="<?php the_permalink(); ?>" class="pull-left blog_pull_img">
					<?php the_post_thumbnail('',$defalt_arg);?>
					</a>
					<?php } ?>
<h2 class="home-news-4"><a href="<?php the_permalink(); ?>"><?php the_title() ;?></a></h2>
<?php if($current_options['home_meta_section_settings'] == false){ ?>
<div class="blog_section2_comment ">
<a href="<?php the_permalink(); ?>"><i class="fa fa-calendar icon-spacing"></i> <?php the_time('M j,Y');?></a>
<i class="fa fa-comments icon-spacing"></i> <a href="<?php comments_link(); ?>#respond">Leave a comment</a> 
<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) );?>"><i class="fa fa-user icon-spacing"></i> <?php _e("By",'rambo');?>&nbsp;<?php the_author();?></a>
</div>
<?php }?>
<p><?php  echo get_the_excerpt(); ?></p>
<p class="tags_alignment">
<a href="<?php the_permalink(); ?>" class="blog_section_readmore pull-right">Read more...</a>
</p>
</div>
</div>
<?php 
			  $i++;
			  wp_reset_postdata();
			} endwhile;  ?>	
	<?php 	
		} ?>


</div>
</div>
<?php } ?>