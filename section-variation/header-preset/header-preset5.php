<?php 
$rambo_pro_theme_options = theme_data_setup();
$rambopro_current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
$search_icon='';
if($rambopro_current_options['search_icon']==0  || $rambopro_current_options['search_icon']=='')
{
  $search_icon.='<div class="search-bar">
                      <div class="search-box-outer">
                        <div class="dropdown">
                          <a href="#searchbar_fullscreen" title="Search" class="nav-link search-iconaria-haspopup=" true"="" aria-expanded="false">
                            <i class="fa fa-search"></i>
                          </a>
                        </div>
                      </div>
                    </div>';                     
}
else
{
$search_icon.='';
}
?>
<header class="header">
<div class="navbar-header index1">
    <div class="container">
      <?php   
        
          if(has_custom_logo())
          {
          // Display the Custom Logo
          the_custom_logo();
          }
        
         elseif($rambopro_current_options['rambo_texttitle'] ==true) { ?>
          <?php $blogname = get_bloginfo( );
            $blogname1 = substr($blogname,0,1);
            $blogname2 = substr($blogname,1);
          ?>
           <a href="<?php echo home_url( '/' ); ?>" class="brand">
          <span class="logo-title"><?php echo ucfirst($blogname1); ?><small><?php echo $blogname2; ?></small></span>
          <?php } else if($rambopro_current_options['upload_image_logo']!='')
            { ?><img src="<?php echo $rambopro_current_options['upload_image_logo']; ?>" style="height:<?php if($rambopro_current_options['height']!='') { echo $rambopro_current_options['height']; }  else { "50"; } ?>px; width:<?php if($rambopro_current_options['width']!='') { echo $rambopro_current_options['width']; }  else { "150"; } ?>px;" /><?php
            } else { ?>
          <span class="logo-title"><?php sprintf(__('R<small>ambo</small>','rambo')); ?></span>
          <?php } ?>
          </a>
    </div>
  </div>
<?php
    $shop_button = '<ul class="nav">%3$s';
    if ( class_exists( 'WooCommerce' ) ) {
      $shop_button .= '</ul> <div class="header-module">';
      $shop_button .=$search_icon;
               $shop_button .='<div class="cart-header ">';
      global $woocommerce; 
      $link = function_exists( 'wc_get_cart_url' ) ? wc_get_cart_url() : $woocommerce->cart->get_cart_url();
      $shop_button .= '<a class="cart-icon" href="'.$link.'" >';
      
      if ($woocommerce->cart->cart_contents_count == 0){
          $shop_button .= '<i class="fa fa-shopping-cart" aria-hidden="true"></i>';
        }
        else
        {
          $shop_button .= '<i class="fa fa-shopping-cart" aria-hidden="true"></i>';
        }
           
        $shop_button .= '</a>';
        
        $shop_button .= '<a href="'.$link.'" ><span class="cart-total">
          '.sprintf(_n('%d item', $woocommerce->cart->cart_contents_count, 'honeypress'), $woocommerce->cart->cart_contents_count).'</span></a>';         
    }
    else
    {
      if($rambopro_current_options['search_icon']==0  || $rambopro_current_options['search_icon']=='')
        {
          $shop_button .= '</ul><div class="header-module">'.$search_icon.'</div>';
        }
    }
    ?>
  <div class="navbar menu-section navbar1">
        <div class="navbar-inner">
            <div class="container">
              <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
              <div class="nav-collapse collapse navbar-responsive-collapse ">
               <?php  wp_nav_menu( array(  
                  'theme_location' => 'primary',
                  'container'  => 'nav-collapse collapse navbar-inverse-collapse',
                  'menu_class' => 'nav',
                  'fallback_cb' => 'webriti_fallback_page_menu',
                 'items_wrap'  => $shop_button,
                  'walker' => new webriti_nav_walker()
                  )
                );  ?> 
              </div><!-- /.nav-collapse -->
            </div>
        </div><!-- /navbar-inner -->
  </div>
  </div>
  </div>
</header>