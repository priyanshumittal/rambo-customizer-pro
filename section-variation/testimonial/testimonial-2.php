<?php
$rambo_pro_theme_options = theme_data_setup();
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
$team_options  = ! empty($current_options['rambo_testy_content']) ? $current_options['rambo_testy_content'] : '';
$ThemeData = get_option('rambo_pro_theme_options');
if(empty($team_options))
	{
		
		if (!empty($current_options['slider_category'])){
			
			// Set default data from old pro theme
			$count_posts = wp_count_posts( 'rambopro_testimonial')->publish;
			$arg = array( 'post_type' => 'rambopro_testimonial','posts_per_page' =>$count_posts);
			$team = new WP_Query( $arg );
			$i=1;
			if($team->have_posts())
			{	while ( $team->have_posts() ) : $team->the_post();			
				$pro_team_data_old[] = array(
					'title'      => get_the_title(),
					'subtitle'   => get_post_meta( get_the_ID(), 'testimonial_designation', true),
					'text'       => get_post_meta( get_the_ID(), 'testimonial_role', true ),
					'link'       => get_post_meta( get_the_ID(), 'testimonial_link', true ),
					'image_url'  => get_the_post_thumbnail_url(),
					'open_new_tab' => get_post_meta( get_the_ID(), 'meta_testimonial_target', true ),
					'id'    => 'customizer_repeater_56d7ea7f40t754',
					);
				endwhile;
			$team_options = json_encode($pro_team_data_old);
						
			}
		}else{
		
		$team_options = json_encode( array(
					array(
					'image_url'  => get_template_directory_uri().'/images/testimonial/1.png',
					'title'           => 'Williamson',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'Founder', 'quality' ),
					'open_new_tab' => 'no',
					'id'              => 'customizer_repeater_56d7ea7f40c56',
				),
				array(
					'image_url'  => get_template_directory_uri().'/images/testimonial/2.png',
					'title'           => 'Paul Walker',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'UI Designer', 'quality' ),
					'open_new_tab' => 'no',
					'id'              => 'customizer_repeater_56d7ea7f40c66',
				),
				array(
					'image_url'  => get_template_directory_uri().'/images/testimonial/3.png',
					'title'           => 'Kristina',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'Php Developer', 'quality' ),
					'open_new_tab' => 'no',
					'id'              => 'customizer_repeater_56d7ea7f40c76',
				),
				array(
					'image_url'  => get_template_directory_uri().'/images/testimonial/4.png',
					'title'           => 'Laura Michelle',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'Designer', 'quality' ),
					'open_new_tab' => 'no',
					'id'        => 'customizer_repeater_56d7ea7f40c86',
				),
				
				
				
				) );
	}
			
	}


if(!empty($current_options['testi_background']))
{
?>
<div class="testimonial2 bg home-testy" style="background:rgba(0, 0, 0, 0) url('<?php echo $current_options['testi_background']; ?>')">
<?php
}
else
{
?>
<div class="testimonial2 bg-none home-testy">
<?php
}
?>
	<div class="overlay">
		<div class="container">
			<?php if(($current_options['testimonial_section_title']) || ($current_options['testimonial_section_desc']!='' )) {?>
		<div class="row-fluid featured_port_title">
			<?php if($current_options['testimonial_section_title']!='') { ?>
				<h1><?php echo $current_options['testimonial_section_title']; ?></h1>
			<?php } ?>
			<?php if($current_options['testimonial_section_desc']!='') { ?>
				<p><?php echo $current_options['testimonial_section_desc']; ?></p>
			<?php } ?>
		</div>
	<?php } ?>
			<div class="row main_space">
				<?php
			$team_options = json_decode($team_options);
					if( $team_options!='' )
					{
					foreach($team_options as $team_item){
				
					$image    = ! empty( $team_item->image_url ) ? apply_filters( 'rambo_translate_single_string', $team_item->image_url, 'Team section' ) : '';
					$title    = ! empty( $team_item->title ) ? apply_filters( 'rambo_translate_single_string', $team_item->title, 'Team section' ) : '';
					$text = !empty($team_item->text) ? apply_filters('rambo_translate_single_string', $team_item->text, 'Team section' ): '';
					$subtitle = ! empty( $team_item->subtitle ) ? apply_filters( 'rambo_translate_single_string', $team_item->subtitle, 'Team section' ) : '';
					$link     = ! empty( $team_item->link ) ? apply_filters( 'rambo_translate_single_string', $team_item->link, 'Team section' ) : '';
					$open_new_tab = $team_item->open_new_tab;
		        ?>
				<div class="span<?php echo $current_options['testimonial_col_layout'];?> aboutus_testimonial">
					<div class="media">
						<div class="author-box">
							<?php if ( ! empty( $image ) ) : ?>
										<?php
										if ( ! empty( $link ) ) :
											$link_html = '<a href="' . esc_url( $link ) . '"';
											if ( function_exists( 'rambo_is_external_url' ) ) {
												$link_html .= rambo_is_external_url( $link );
											}
											$link_html .= '>';
											echo wp_kses_post( $link_html );
										endif;
										echo '<img class="aboutus_testimonial_img" src="' . esc_url( $image ) . '"';
										if ( ! empty( $title ) ) {
											echo 'alt="' . esc_attr( $title ) . '" title="' . esc_attr( $title ) . '"';
										}
										echo '/>';
										if ( ! empty( $link ) ) {
											echo '</a>';
										}
										?>
						<?php endif; ?>
						</div>
						<h4><?php echo $title; ?> <br /><small><?php echo $subtitle; ?></small></h4>
					  <div class="media-body">
						<p><?php echo $text; ?></p>
					  </div>
					</div>
				</div>
				<?php } } ?>
			</div>
		</div>
	</div>
</div>