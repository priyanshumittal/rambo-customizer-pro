<?php
$rambo_pro_theme_options = theme_data_setup();
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
if($current_options['team_section_enable'] ==false) {
$team_options  = ! empty($current_options['rambo_team_content']) ? $current_options['rambo_team_content'] : '';
$ThemeData = get_option('rambo_pro_theme_options'); 
if(empty($team_options))
	{
		if (!empty($current_options['slider_category'])){		
			// Set default data from old pro theme
			$count_posts = wp_count_posts( 'rambopro_team')->publish;
			$arg = array( 'post_type' => 'rambopro_team','posts_per_page' =>$count_posts);
			$team = new WP_Query( $arg );
			$i=1;
			if($team->have_posts())
			{	while ( $team->have_posts() ) : $team->the_post();			
				$pro_team_data_old[] = array(
					'title'      => get_the_title(),
					'subtitle'   => get_post_meta( get_the_ID(), 'team_designation', true),
					'text'       => get_post_meta( get_the_ID(), 'team_role', true ),
					'link'       => get_post_meta( get_the_ID(), 'team_link', true ),
					'image_url'  => get_the_post_thumbnail_url(),
					'open_new_tab' => get_post_meta( get_the_ID(), 'meta_team_target', true ),
					'id'    => 'customizer_repeater_56d7ea7f40t754',
					);
				endwhile;
			$team_options = json_encode($pro_team_data_old);
			}
		}else{
		$team_options = json_encode( array(
					array(
					'image_url'  => get_template_directory_uri().'/images/team/team1.jpg',
					'title'           => 'Michael Clarke1',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'Founder', 'quality' ),
					'open_new_tab' => 'no',
					'id'              => 'customizer_repeater_56d7ea7f40c56',
				),
				array(
					'image_url'  => get_template_directory_uri().'/images/team/team2.jpg',
					'title'           => 'Paul Walker',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'UI Designer', 'quality' ),
					'open_new_tab' => 'no',
					'id'              => 'customizer_repeater_56d7ea7f40c66',
				),
				array(
					'image_url'  => get_template_directory_uri().'/images/team/team3.jpg',
					'title'           => 'Chris Gayle',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'Php Developer', 'quality' ),
					'open_new_tab' => 'no',
					'id'              => 'customizer_repeater_56d7ea7f40c76',
				),
				array(
					'image_url'  => get_template_directory_uri().'/images/team/team4.jpg',
					'title'           => 'Laura Michelle',
					'text' => 'Torquent per conubia nostra, per inceptos himenaeos. Curabitur dolor risus, consequat interdum commodo a, iaculis eu tellus. Donec risus eros, sollicitudin ac dictum vel, tristique consectetur.',
					'subtitle'        => esc_html__( 'Designer', 'quality' ),
					'open_new_tab' => 'no',
					'id'        => 'customizer_repeater_56d7ea7f40c86',
				),
				) );
	}	
	}
?>
<div class="additional_section_one team2">
	<div class="container">
	<?php if(($current_options['team_section_title']) || ($current_options['team_section_desc']!='' )) {?>
		<div class="row-fluid featured_port_title">
			<?php if($current_options['team_section_title']!='') { ?>
				<h1><?php echo $current_options['team_section_title']; ?></h1>
			<?php } ?>
			<?php if($current_options['team_section_desc']!='') { ?>
				<p><?php echo $current_options['team_section_desc']; ?></p>
			<?php } ?>
		</div>
	<?php } ?>
		<div class="row-fluid team_section">
			<?php
			$team_options = json_decode($team_options);
					if( $team_options!='' )
					{
					foreach($team_options as $team_item){
				
					$image    = ! empty( $team_item->image_url ) ? apply_filters( 'rambo_translate_single_string', $team_item->image_url, 'Team section' ) : '';
					$title    = ! empty( $team_item->title ) ? apply_filters( 'rambo_translate_single_string', $team_item->title, 'Team section' ) : '';
					$text = !empty($team_item->text) ? apply_filters('rambo_translate_single_string', $team_item->text, 'Team section' ): '';
					$subtitle = ! empty( $team_item->subtitle ) ? apply_filters( 'rambo_translate_single_string', $team_item->subtitle, 'Team section' ) : '';
					$link     = ! empty( $team_item->link ) ? apply_filters( 'rambo_translate_single_string', $team_item->link, 'Team section' ) : '';
					$open_new_tab = $team_item->open_new_tab;
		        ?>
			<div class="span<?php echo $current_options['team_col_layout'];?>">
				<div class="team-area">
					<div class="team-img">
							<?php if ( ! empty( $image ) ) : ?>
										<?php
										if ( ! empty( $link ) ) :
											$link_html = '<a class="attachment-post-thumbnail size-post-thumbnail wp-post-image" href="' . esc_url( $link ) . '"';
											if ( function_exists( 'rambo_is_external_url' ) ) {
												$link_html .= rambo_is_external_url( $link );
											}
											$link_html .= '>';
											echo wp_kses_post( $link_html );
										endif;
										echo '<img class="img" src="' . esc_url( $image ) . '"';
										if ( ! empty( $title ) ) {
											echo 'alt="' . esc_attr( $title ) . '" title="' . esc_attr( $title ) . '"';
										}
										echo '/>';
										if ( ! empty( $link ) ) {
											echo '</a>';
										}
										?>
						<?php endif; ?>
					</div>
					<div class="team-details">
					<?php if ( ! empty( $link ) ) : ?>
						<h3><a href="<?php echo $link ?>" <?php if($open_new_tab == 'yes'){ echo 'target="_blank"';}?>> <?php echo $title; ?></a></h3>
						<h5><?php echo $subtitle; ?></h5>
						
					<?php else: ?>	
						<h3><a><?php echo $title; ?> </a></h3>
						<h5><?php echo $subtitle; ?></h5>
					<?php endif; ?>
					<p class="text-center">
						<?php echo $text; ?>							
						</p>
					</div>
				</div>
			</div>
			<?php } } ?>			
		</div>
	</div>
</div>
<?php } ?>