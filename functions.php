<?php
/*
	*Theme Name	: Rambo
	*Theme Core Functions and Codes
*/	
	/**Includes reqired resources here**/
	define('WEBRITI_TEMPLATE_DIR_URI',get_template_directory_uri());	
	
	define('WEBRITI_TEMPLATE_DIR',get_template_directory());
	define('WEBRITI_THEME_FUNCTIONS_PATH',WEBRITI_TEMPLATE_DIR.'/functions');
	
	define('WEBRITI_THEME_OPTIONS_PATH',WEBRITI_TEMPLATE_DIR_URI.'/functions/theme_options');
	require_once('theme_setup_data.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/menu/default_menu_walker.php' ); // for Default Menus
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/menu/rambo_nav_walker.php' ); // for Custom Menus	
	
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/scripts/scripts.php' ); // all js and css file for rambo-pro	
	
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/pagination/webriti_pagination.php' ); //webriti pagination class
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/post-type/custom-post-type.php' );// for rambo-pro cpt
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/meta-box/post-meta.php' );// for rambo-pro meta box
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/commentbox/comment-function.php' ); //for comments
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/custom-sidebar.php' ); //for widget register
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/taxonomies/taxonomies.php' );// for rambo taxonomies
	
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/font/font.php'); //for font library
	
	// Footer widgets
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-footer-usefull-links-widgets.php' ); //for footer custom widgets	
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-contact-widgets.php' ); //for footer custom widgets	
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-site-intro-widget.php' ); //for Site Intro widgets
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-site-intro-botton-widget.php' ); //for Site Intro widgets
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-sidebar-project-widget.php' ); //for Project widgets
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-register-page-widget.php' ); //for Page / Service widgets
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-team-widget.php' ); //for sidebar Team widgets
    require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-testimonial-widget.php' ); //for sidebar Testimonial widgets	
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-header-widget.php' ); //for sidebar Header widgets
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-testimonial-carousel-widget.php' ); //carousel testimonial widgets
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-client-widget.php' );
	//Section widget
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-section-widget.php' );
	//testimonial carousel widget
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-client-carousel-widget.php' );
	
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/wpml-pll/functions.php' );
	
	// Sidebar Widgets
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-sidebar-latest-news.php' ); //for sidebar Latest News custom widgets	
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-sidebar-categoryt-list.php' ); //for sidebar category list	
	
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/rambo-sidebar-newsletter.php' ); //for news widget
	require( WEBRITI_TEMPLATE_DIR . '/css/custom_light.php');
	
	//Customizer
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_theme_color.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_header.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_slider_panel.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_service_panel.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_site_intro.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_project.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_recent_news.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_team.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_testimonial.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_site_intro_bottom.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_typography.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_post_slug.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_template.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_layout.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_copyright.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_client.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_shop.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_import_data.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer.php' );

	require( WEBRITI_TEMPLATE_DIR . '/inc/customizer/customizer-slider/customizer-slider.php');

	if ( ! function_exists( 'rambo_customizer_preview_scripts' ) ) {
    function rambo_customizer_preview_scripts() {
        wp_enqueue_script( 'rambo-customizer-preview', trailingslashit( get_template_directory_uri() ) . 'inc/customizer/customizer-slider/js/customizer-preview.js', array( 'customize-preview', 'jquery' ) );
	    }
	}
	add_action( 'customize_preview_init', 'rambo_customizer_preview_scripts' );

	
	// rambo Info Page
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/rambo-info/welcome-screen.php');
	
	//shortcode  
	require_once( WEBRITI_THEME_FUNCTIONS_PATH .'/shortcodes/shortcodes.php');
	
	//require( WEBRITI_THEME_FUNCTIONS_PATH . '/excerpt/excerpt.php' ); // for Excerpt Length
	
	global $resetno; //user for reset function
	//content width
	if ( ! isset( $content_width ) ) $content_width = 900;	
	
	//wp title tag starts here
	function webriti_head( $title, $sep )
	{	global $paged, $page;		
		if ( is_feed() )
			return $title;
		// Add the site name.
		$title .= get_bloginfo( 'name' );
		// Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title = "$title $sep $site_description";
		// Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( _e('Page','rambo'), max( $paged, $page ) );
		return $title;
	}	
	add_filter( 'wp_title', 'webriti_head', 10,2 );
	
		add_action( 'after_setup_theme', 'webriti_setup' ); 	
		function webriti_setup()
		{	// Load text domain for translation-ready
			load_theme_textdomain( 'rambo', WEBRITI_THEME_FUNCTIONS_PATH . '/lang' );	
			
		function custom_excerpt_length( $length ) {	return 50; }
		add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
		function new_excerpt_more( $more ) {	return '';}
		add_filter('excerpt_more', 'new_excerpt_more');
		
		function cleanup_shortcode_fix($content) {  
              $array = array ('<p>[' => '[',']</p>' => ']',']<br />' => ']',']<br>' => ']','<p>  </p>'=>'');              
			  $content = strtr($content, $array);				
			return $content;
            }
		add_filter('the_content', 'cleanup_shortcode_fix'); 
		
		add_theme_support( 'post-thumbnails' ); //supports featured image
		add_theme_support( 'post-formats', array( 'quote' ) );
		add_theme_support( 'woocommerce' );//woocommerce
		add_theme_support( 'title-tag' ); //Title Tag
		add_theme_support( 'automatic-feed-links' ); // Feed Link
		add_theme_support( 'custom-background' ); // Custom Background
		
		//Custom logo
	
		add_theme_support( 'custom-logo' , array(
	
	   'class'       => 'navbar-brand',
	   
	   'width'       => 200,
	   
	   'height'      => 50,
	   
	   'flex-width' => true,
	   
	   'flex-height' => true,
	   
		) );
		
		// This theme uses wp_nav_menu() in one location.
		register_nav_menu( 'primary', __( 'Primary Menu', 'rambo' ) );
		
		// setup admin pannel defual data for index page
		$rambo_pro_theme=theme_data_setup();
	}
	
	// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
	
	// change custom logo link class
	add_filter('get_custom_logo','change_logo_class');
	function change_logo_class($html)
	{
		$html = str_replace('class="custom-logo-link"', 'class="brand"', $html);
		return $html;
	}
	
function rambo_import_files() {
  return array(
    array(
      'import_file_name'           => 'Demo Import 1',
      'categories'                 => array( 'Category 1', 'Category 2' ),
      'import_file_url'            => 'https://webriti.com/themes/dummydata/rambo/pro/rambo-content.xml',
      'import_widget_file_url'     => 'https://webriti.com/themes/dummydata/rambo/pro/rambo-widget.json',
      'import_customizer_file_url' => 'https://webriti.com/themes/dummydata/rambo/pro/rambo-customize.dat',
      'import_notice'              => sprintf(__( 'Click the large blue button to start the dummy data import process.</br></br>Please be patient while WordPress imports all the content.</br></br>
			<h3>Recommended Plugins</h3>Rambo theme supports the following plugins:</br> </br><li> <a href="https://wordpress.org/plugins/contact-form-7/"> Contact form 7</a> </l1> </br> <li> <a href="https://wordpress.org/plugins/woocommerce/"> WooCommerce </a> </li><li><a href="https://wordpress.org/plugins/easy-bootstrap-shortcodes/"> Easy Bootstrap Shortcode </a></li><li> <a href="https://wordpress.org/plugins/jetpack/"> Jetpack </a></li><li> <a href="https://wordpress.org/plugins/wp-google-maps/"> WP Google Maps </a></li>', 'rambo' )),
			),


    	
	);
}
add_filter( 'pt-ocdi/import_files', 'rambo_import_files' );


function rambo_after_import_setup() {

	// Menus to assign after import.
	$main_menu   = get_term_by( 'name', 'My Menu', 'nav_menu' );

	set_theme_mod( 'nav_menu_locations', array(
		'primary'   => $main_menu->term_id,
	));	
	
}
add_action( 'pt-ocdi/after_import', 'rambo_after_import_setup' );
function wc_hide_page_title()
{
    if( !is_shop() ) // is_shop is the conditional tag
        return true;
}
add_filter( 'woocommerce_show_page_title', 'wc_hide_page_title' );




add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    #sidebar-service {
    display: none !important;
}

#testimonial-widget-area {
    display: none !important;
}

#sidebar-project {
    display: none !important;
}

#additional-area-two {
    display: none !important;
}

#site-intro-area {
    display: none !important;
}

#site-intro-area-bottom {
    display: none !important;
}
  </style>';
}



if(get_option('rambo_pro_theme_options')!=''){
	
	$old_theme_project = get_option('rambo_pro_theme_options');
	//print_r($old_theme_project['project_one_title']);
		/*Product one*/
		$status = get_option('rambo-migration-status','no');
	if($status == 'no'){
		
		$old_theme_project = get_option('rambo_pro_theme_options');
		if(isset($old_theme_project['project_one_title'])){
			$post_id = wp_insert_post(
			array (
		   'post_type' => 'rambopro_project',
		   'post_title' => $old_theme_project['project_one_title'],
		   'post_content'   => $old_theme_project['project_one_text'],
		   
		   'post_status' => 'publish',
		));

		$filename = $old_theme_project['project_one_thumb'];

		$filetype = wp_check_filetype( basename( $filename ), null );

		$wp_upload_dir = wp_upload_dir();

		$attachment = array(
			'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
			'post_mime_type' => $filetype['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
			'post_content'   => '',
			//'post_category' => array(2,3),
			'post_status'    => 'inherit',
			
		);
		
	
		
		$attach_id = wp_insert_attachment( $attachment, $filename, $post_id );
		set_post_thumbnail( $post_id, $attach_id );

		//update_post_meta($post_id, 'description', $old_theme_project['project_one_text']);
		
		$table_name = $wpdb->prefix . "term_relationships";
		
        $wpdb->insert($table_name, array('object_id' => $post_id, 'term_taxonomy_id' => 2 ,'term_order' => 0) ); 
		
		
		}
		
		/*Product two*/
		if(isset($old_theme_project['project_two_title'])){
			$post_id = wp_insert_post(
			array (
		   'post_type' => 'rambopro_project',
		   'post_title' => $old_theme_project['project_two_title'],
		   'post_content'   => $old_theme_project['project_two_text'],
		   
		   'post_status' => 'publish'
		   
		));
		$filename = $old_theme_project['project_two_thumb'];
		$filetype = wp_check_filetype( basename( $filename ), null );
		$wp_upload_dir = wp_upload_dir();
		$attachment = array(
			'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
			'post_mime_type' => $filetype['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
			'post_content'   => '',
			
			'post_status'    => 'inherit',
			//'post_category' => array(2),
		);
		$attach_id = wp_insert_attachment( $attachment, $filename, $post_id );
		set_post_thumbnail( $post_id, $attach_id );
		
		//update_post_meta($post_id, 'description', $old_theme_project['project_two_text']);
		
		$table_name = $wpdb->prefix . "term_relationships";
		
        $wpdb->insert($table_name, array('object_id' => $post_id, 'term_taxonomy_id' => 2 ,'term_order' => 0) ); 
		
		}
		
		/*Product three*/
		if(isset($old_theme_project['project_three_title'])){
			$post_id = wp_insert_post(
			array (
		   'post_type' => 'rambopro_project',
		   'post_title' => $old_theme_project['project_three_title'],
		   'post_content'   => $old_theme_project['project_three_text'],
		   'post_status' => 'publish',
		 
		   //'post_category' => array('All'),
		));
		$filename = $old_theme_project['project_three_thumb'];
		$filetype = wp_check_filetype( basename( $filename ), null );
		$wp_upload_dir = wp_upload_dir();
		$attachment = array(
			'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
			'post_mime_type' => $filetype['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
			'post_content'   => '',
			'post_status'    => 'inherit',
			
		);
		$attach_id = wp_insert_attachment( $attachment, $filename, $post_id );
		set_post_thumbnail( $post_id, $attach_id );
		
		$table_name = $wpdb->prefix . "term_relationships";
		
        $wpdb->insert($table_name, array('object_id' => $post_id, 'term_taxonomy_id' => 2 ,'term_order' => 0) ); 
		
		//update_post_meta($post_id, 'description', $old_theme_project['project_three_text']);
		}
		
		/*Product four*/
		if(isset($old_theme_project['project_four_title'])){
			$post_id = wp_insert_post(
			array (
		   'post_type' => 'rambopro_project',
		   'post_title' => $old_theme_project['project_four_title'],
		   'post_content'   => $old_theme_project['project_four_text'],
		   'post_status' => 'publish',
		  
		));
		$filename = $old_theme_project['project_four_thumb'];
		$filetype = wp_check_filetype( basename( $filename ), null );
		$wp_upload_dir = wp_upload_dir();
		$attachment = array(
			'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
			'post_mime_type' => $filetype['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
			'post_content'   => '',
			'post_status'    => 'inherit',
			
		);
		$attach_id = wp_insert_attachment( $attachment, $filename, $post_id );
		set_post_thumbnail( $post_id, $attach_id );
		
		$table_name = $wpdb->prefix . "term_relationships";
		
        $wpdb->insert($table_name, array('object_id' => $post_id, 'term_taxonomy_id' => 2 ,'term_order' => 0) ); 
	
		//update_post_meta($post_id, 'description', $old_theme_project['project_four_text']);
		}
		
		
	update_option('rambo-migration-status','yes');
	
	}
	
}


if ( ! function_exists( 'ram_fs' ) ) {
    // Create a helper function for easy SDK access.
    function ram_fs() {
        global $ram_fs;

        if ( ! isset( $ram_fs ) ) {
            // Include Freemius SDK.
            require_once WEBRITI_TEMPLATE_DIR . '/freemius/start.php';

            $ram_fs = fs_dynamic_init( array(
                'id'                  => '11282',
                'slug'                => 'rambo',
                'premium_slug'        => 'rambo-pro',
                'type'                => 'theme',
                'public_key'          => 'pk_86382d3a3a35e977b1b4654c728e7',
                'is_premium'          => true,
                'premium_suffix'      => 'Pro',
                // If your theme is a serviceware, set this option to false.
                'has_premium_version' => true,
                'has_addons'          => false,
                'has_paid_plans'      => true,
                'menu'                => array(
                    'slug'           => 'rambo-info',
                    'support'        => false,
                    'parent'         => array(
                        'slug' => 'themes.php',
                    ),
                ),
            ) );
        }

        return $ram_fs;
    }

    // Init Freemius.
    ram_fs();
    // Signal that SDK was initiated.
    do_action( 'ram_fs_loaded' );
}

function rambo_customizer_styles() {
    echo '<style> .accordion-section .accordion-section-title button.accordion-trigger { height:auto; } </style>';
}
add_action('customize_controls_enqueue_scripts', 'rambo_customizer_styles');
?>