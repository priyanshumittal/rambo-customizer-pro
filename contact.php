<?php // Template Name: Contact Page ?>
<?php get_template_part('banner','strip');
$image_uri= WEBRITI_TEMPLATE_DIR_URI. '/images' ;
$rambo_pro_theme_options = theme_data_setup();
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
$mapsrc= $current_options['rambo_contact_google_map_shortcode'];	
$mapsrc=$mapsrc.'&amp;output=embed';
?>
<!-- Google Map -->	
<?php if($current_options['contact_google_map_enabled'] == true){?>
<div class="Contact_google_map">
	<iframe width="100%" scrolling="no" height="400" frameborder="0" src="<?php echo esc_url($mapsrc); ?>" marginwidth="0" marginheight="0"></iframe>		
</div>
<?php } ?>
<!-- /Google Map -->
	
<!-- Container -->
<div class="container">
	<!-- Contact Container -->
	<div class="row-fluid">
		<!-- Contact -->
		<div class="span8">			
			<!-- Contact Form -->
			<div id="myformdata">
					<?php if( isset( $current_options['contact_form_heading'] ) && $current_options['contact_form_heading']!='') { ?> 
					<div class="blog_single_post_head_title">
						<h3><?php echo esc_attr( $current_options['contact_form_heading'] );  ?></h3>
					</div>
					<?php }
						the_post();
						the_content();
					?>
			</div>
			
			<!-- /Contact Form -->		
		</div>
		<!-- /Contact -->		
		<!-- Sidebar Widget -->
		<div class="span4 sidebar">
		<?php  
		
		if( is_active_sidebar('contact-sidebar-area') )
		{
			dynamic_sidebar('contact-sidebar-area');
			
		}
		else
		{
		?>
			<!--Get in Touch-->
			<?php if( $current_options['rambo_get_in_touch_enabled'] == true ){ ?>
			<div class="sidebar_widget">
				
				<?php if( $current_options['rambo_get_in_touch'] != '') { ?>
				<div class="sidebar_widget_title">
					<h2><?php echo esc_attr($current_options['rambo_get_in_touch']); ?></h2>
				</div>
				<?php } ?>
				
				<?php if($current_options['rambo_get_in_touch_description']!='') { ?>
				<div class="row-fluid">
					<p><?php echo($current_options['rambo_get_in_touch_description']); ?></p>
				</div>
				<?php } ?>
				
			</div>
			<?php } ?>
			<!--/Get in Touch-->
			
			
			<!--Contact Detail-->
			<?php if($current_options['rambo_our_office_enabled'] == true){?>
			<div class="sidebar_widget">
			
				<?php if( $current_options['rambo_our_office'] != '') { ?>
				<div class="sidebar_widget_title">
					<h2><?php echo esc_attr($current_options['rambo_our_office']); ?></h2>
				</div>
				<?php } ?>
				
				<div class="row-fluid">
					
					<?php if( $current_options['rambo_contact_address'] != '') { ?>
					<p class="sidebar_con_detail">
						<i class="fa fa-map-marker icon-spacing"></i><span>
							<?php _e('Address','rambo'); ?> <small><?php echo esc_attr($current_options['rambo_contact_address']); ?>
						</small></span>
					</p>
					<?php } ?>
					
					<?php if($current_options['rambo_contact_email'] != '') { ?>
					<p class="sidebar_con_detail">
						<i class="fa fa-envelope icon-spacing"></i><span><?php _e('Email','rambo'); ?> <small>
						<?php echo esc_attr($current_options['rambo_contact_email']); ?>
						</small></span>
					</p>
					<?php } ?>
					
					<?php if( $current_options['rambo_contact_phone_number'] != '') { ?>
					<p class="sidebar_con_detail">
						<i class="fa fa-phone icon-spacing"></i><span><?php _e('Phone','rambo'); ?> <small>
						<?php echo esc_attr($current_options['rambo_contact_phone_number']); ?>
						</small></span>
					</p>
					<?php } ?>
			
				</div>
			</div>
			<?php } ?>
			<!--/Contact Detail-->	
			
			<!--Sidebar Social Icons-->
			<?php if( $current_options['social_media_in_contact_page_enabled'] == true ){?>
			<div class="sidebar_widget">
			
				<div class="sidebar_widget_title"><h2><?php _e('Get Connected','rambo'); ?></h2></div>
				
				<div class="sidebar_social">
						
						<?php if( $current_options['social_media_facebook_link'] != '' ) { ?>
						<a href="<?php echo esc_attr($current_options['social_media_facebook_link']); ?>" class="facebook">&nbsp;</a>
						<?php } ?>
						
						<?php if( $current_options['social_media_twitter_link'] != '') { ?>
						<a href="<?php echo esc_attr($current_options['social_media_twitter_link']); ?>" class="twitter">&nbsp;</a>
						<?php } ?>
						
						<?php if( $current_options['social_media_linkedin_link'] != '') { ?>
						<a href="<?php echo esc_attr($current_options['social_media_linkedin_link']); ?>" class="linked-in">&nbsp;</a>
						<?php } ?>
						
						<?php if( $current_options['social_media_google_plus'] != '') { ?>
						<a href="<?php echo esc_attr($current_options['social_media_google_plus']); ?>" class="google_plus">&nbsp;</a>
						<?php } ?>
						
				</div>	
				
			</div><!--/Sidebar Social Icons-->
			
		<?php } } ?>
		</div>
		<!-- /Sidebar Widget -->

	</div>
	<!-- /Contact Container -->	
</div>
<?php get_footer();?>