<?php 
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), theme_data_setup() );
if ( $current_options['home_slider_enabled'] == true ){
//$slider_entries_main = $current_options['slider_entries_main'];
if( $current_options['slider_category'] ){$SlideCategory = $current_options['slider_category'];}
else{$SlideCategory = 1;
}
/*Slider data */
$slide_options  = ! empty($current_options['rambo_slider_content']) ? $current_options['rambo_slider_content'] : '';
//$ThemeData = get_option('rambo_pro_theme_options');
$ThemeData = get_option('rambo_pro_theme_options');
if(empty($slide_options))
{
		if (!empty($ThemeData['slider_category']))
		{
		
		$args = array( 'post_type' => 'post', 'category__in'=> $SlideCategory); 	
		$slider = new WP_Query( $args ); 
		if( $slider->have_posts() )
			{
				while ( $slider->have_posts() ) : $slider->the_post();
				$my_meta = get_post_meta( get_the_ID() ,'_my_meta', TRUE );
				if(isset($my_meta['link']))
				{	$meta_value_link = $my_meta['link']; }
				else
				{	$meta_value_link = get_permalink(); }
					
				if(isset($my_meta['check']))
				{	$target ='yes';  } 
				else 
				{	$target ='no';  }
				
				$more_enable = sanitize_text_field( get_post_meta( get_the_ID(), 'more_enable', true ));
				$btn_text = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_text', true ));
				$btn_link = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_link', true ));
				$btn_target = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_target', true ));
					
				$pro_slider_data_old[] = array(
				'title'   => get_the_title(),
				'text'       => get_the_excerpt(),
				'button_text' => $btn_text,
				'link'       => $btn_link,
				'image_url'  =>  get_the_post_thumbnail_url(),
				'open_new_tab' => $btn_target,
				'id'         => 'customizer_repeater_56d7ea7f40b41',
				);
				endwhile; 
				$slide_options = json_encode($pro_slider_data_old);
			}
		}  
		elseif($ThemeData!='')
		{ // Set default data from lite theme
			$ThemeSliderData = get_option('rambo_pro_theme_options');
				$slide_options = json_encode( array(
				array(
				'title'		=> isset($ThemeSliderData['slider_title'])? $ThemeSliderData['slider_title'] : esc_html__('Powerful Bootstrap Theme','rambo'),
				'text'		=> isset($ThemeSliderData['slider_text'])? $ThemeSliderData['slider_text']: 'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc',
				'image_url'  => isset($ThemeSliderData['slider_post'])? $ThemeSliderData['slider_post']: get_template_directory_uri().'/images/default/slide/slide1.png',
				'button_text'      => isset($ThemeSliderData['slider_readmore_text'])? $ThemeSliderData['slider_readmore_text']: esc_html__('Read More','rambo'),
				'link'       => isset($ThemeSliderData['readmore_text_link']) ? $ThemeSliderData['readmore_text_link']:'#',
				'open_new_tab' => 'yes',
				'id'         => 'customizer_repeater_56d7ea7f40b89',
				),
				
				array(
				'title'		=> esc_html__('We Create Responsive Designs','rambo'),
				'text'		=> 'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc',
				'image_url'  => get_template_directory_uri().'/images/default/slide/slide2.png',
				'button_text'      => esc_html__('Read More','rambo'),
				'link'       => '#',
				'open_new_tab' => 'yes',
				'id'         => 'customizer_repeater_56d7ea7f40b90',
				),
				
				
				array(
				'title'		=> esc_html__('Get an Idea','rambo'),
				'text'		=> 'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc',
				'image_url'  => get_template_directory_uri().'/images/default/slide/slide3.png',
				'button_text'      => esc_html__('Read More','rambo'),
				'link'       => '#',
				'open_new_tab' => 'yes',
				'id'         => 'customizer_repeater_56d7ea7f40b91',
				),
				
				array(
				'title'		=> esc_html__('We are Rambo','rambo'),
				'text'		=> 'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc',
				'image_url'  => get_template_directory_uri().'/images/default/slide/slide4.png',
				'button_text'      => esc_html__('Read More','rambo'),
				'link'       => '#',
				'open_new_tab' => 'yes',
				'id'         => 'customizer_repeater_56d7ea7f40b92',
				),
					
				) );

			
		
			
		}else {
	//Set Slider default data.
	$slide_options = json_encode( array(
		array(
		'title'		=> esc_html__('Powerful Bootstrap Theme','rambo'),
		'text'		=> esc_html__('Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc','rambo'),
		'image_url'  => get_template_directory_uri().'/images/default/slide/slide1.png',
		'button_text'      => esc_html__('Read More','rambo'),
		'link'       => '#',
		'open_new_tab' => 'yes',
		'id'         => 'customizer_repeater_56d7ea7f40b89',
		),
		array(
		'title'		=> esc_html__('We Create Responsive Design','rambo'),
		'text'		=> esc_html__('Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc','rambo'),
		'image_url'  => get_template_directory_uri().'/images/default/slide/slide1.png',
		'button_text'      => esc_html__('Read More','rambo'),
		'link'       => '#',
		'open_new_tab' => 'yes',
		'id'         => 'customizer_repeater_56d7ea7f40b90',
		),
		array(
		'title'		=> esc_html__('Get an Idea','rambo'),
		'text'		=> esc_html__('Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc','rambo'),
		'image_url'  => get_template_directory_uri().'/images/default/slide/slide1.png',
		'button_text'      => esc_html__('Read More','rambo'),
		'link'       => '#',
		'open_new_tab' => 'yes',
		'id'         => 'customizer_repeater_56d7ea7f40b91',
		),
		array(
		'title'		=> esc_html__('We are Rambo','rambo'),
		'text'		=> esc_html__('Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infanc','rambo'),
		'image_url'  => get_template_directory_uri().'/images/default/slide/slide1.png',
		'button_text'      => esc_html__('Read More','rambo'),
		'link'       => '#',
		'open_new_tab' => 'yes',
		'id'         => 'customizer_repeater_56d7ea7f40b92',
		),
		) );
}
}
$t=true;
?>
<script>
jQuery(function() {

	// Homepage Main Slider Js 
    jQuery("#slider-carousel").carousel(	{ 
		interval:<?php echo $current_options['slider_transition_delay']; ?>,
		pause: "hover"
	});
				
});
</script>
<div class="main_slider">
	<div id="slider-carousel" class="carousel <?php echo $current_options['slider_options']; ?>" data-ride="carousel" data-pause="false" >
		<div class="carousel-inner">
		<!-- Carousel items -->
			<?php 
			 $slide_options = json_decode($slide_options);
				if( $slide_options!='' ){
				foreach($slide_options as $slide_iteam){
					
				$slider_image = ! empty( $slide_iteam->image_url ) ? apply_filters( 'rambo_translate_single_string', $slide_iteam->image_url, 'slider section' ) : '';
				
				$slider_title = ! empty( $slide_iteam->title ) ? apply_filters( 'rambo_translate_single_string', $slide_iteam->title, 'slider section' ) : '';
				
				$slider_text = ! empty( $slide_iteam->text ) ? apply_filters( 'rambo_translate_single_string', $slide_iteam->text, 'slider section' ) : '';
				
				$slider_button_text = ! empty( $slide_iteam->button_text ) ? apply_filters( 'rambo_translate_single_string', $slide_iteam->button_text, 'slider section' ) : '';
				
				$slider_button_link = ! empty( $slide_iteam->link ) ? apply_filters( 'rambo_translate_single_string', $slide_iteam->link, 'slider section' ) : '';
				
			 ?>
			
			<div id="post-<?php the_ID(); ?>" class="item <?php if( $t == true ){ echo 'active'; } $t = false; ?>" >
				<?php 
					if($slider_image!=''){ ?>
				
					 <img alt="img" class="img-responsive" src="<?php echo $slider_image; ?>" draggable="false">	
				<?php } ?>
				<div class="container slider_con">
								<?php if( isset( $slider_title  ) ) { ?>
								<h2><?php echo $slider_title; ?></h2>
								<?php } 
								 if( isset( $slider_text  ) )
								{
								?>	
								<h5 class="slide-title"><span>
									<?php echo $slider_text;	
								} ?></span></h5>
								<?php
								if($slider_button_text!='' ):
								?>
								<a href="<?php echo $slider_button_link; ?>" <?php if($slide_iteam->open_new_tab =='yes'){echo "target='_blank'";} ?> class="slide-btn"><?php echo $slider_button_text; ?></a>		
								<?php endif; ?>
				</div>

			</div>
			
			<?php
				} }
					/*else {
						
						echo '<img alt="img" src="'.(get_template_directory_uri()).'/images/default/slide/slide1.png" />
								<div class="container slider_con">
									<h2>'.__('Get yourself','rambo').'</h2>
									<h5 class="slide-title"><span>'. 'Donec justo odio, lobortis eget congue sed, rutrum sit amet mauris. Curabitur sed lectus nulla. Curabitur sed lectus nulla.lobortis eget congue sed, rutrum sit amet mauris. Curabitur sed lectus nulla rutrum sit amet mauris'.'</span></h5>
									<a class="slide-btn" href="#" target="_blank">Read More</a>
								</div>';
					}*/
					
			?>
	</div>
			
			<?php if(sizeof($slide_options) > 1){?>
		<!-- Carousel nav -->
		<ul class="carou-direction-nav">
		<li><a class="carou-prev" href="#slider-carousel" data-slide="prev"></a></li>
		<li><a class="carou-next" href="#slider-carousel" data-slide="next"></a></li>
		</ul>
		
			<?php }?>
	</div>
</div>
<?php } ?>
<!-- /End of Slider -->
<!-- /Slider -->