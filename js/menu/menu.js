/*Menu Dropdown Onhover Js*/
jQuery(document).ready(function() {

        /* ---------------------------------------------- /*
         * Initialization General Scripts for all pages
         /* ---------------------------------------------- */

        var navbar      = jQuery('.navbar'),
            navHeight   = navbar.height(),
           // worksgrid   = jQuery('#works-grid'),
            width       = Math.max(jQuery(window).width(), window.innerWidth),
            mobileTest  = false;

        if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            mobileTest = true;
        }
        document.onload = rambo_load_menu();
        rambo_navbarSubmenu(width);
        rambo_hoverDropdown(width, mobileTest);

        jQuery(window).resize(function() {
            var width = Math.max(jQuery(window).width(), window.innerWidth);
            rambo_hoverDropdown(width, mobileTest);
        });
     function rambo_load_menu () {
    jQuery('.dropdown-menu').parent().addClass('dropdown');
    jQuery('.dropdown-menu > li > .dropdown-menu').parent().addClass('dropdown-submenu');
    }

        /* ---------------------------------------------- /*
         * Navbar submenu
         /* ---------------------------------------------- */

        function rambo_navbarSubmenu(width) {
            if (width > 1100) {
                jQuery('.navbar li.dropdown').hover(function() {
                    var MenuLeftOffset  = jQuery(this).offset().left;
                    var Menu1LevelWidth = jQuery('.dropdown-menu', jQuery(this)).width();
                    if (width - MenuLeftOffset < Menu1LevelWidth * 2) {
                        jQuery(this).children('.dropdown-menu').addClass('leftauto');
                    } else {
                        jQuery(this).children('.dropdown-menu').removeClass('leftauto');
                    }
                    if (jQuery('.dropdown', jQuery(this)).length > 0) {
                        var Menu2LevelWidth = jQuery('.dropdown-menu', jQuery(this)).width();
                        if (width - MenuLeftOffset - Menu1LevelWidth < Menu2LevelWidth) {
                            jQuery(this).children('.dropdown-menu').addClass('left-side');
                        } else {
                            jQuery(this).children('.dropdown-menu').removeClass('left-side');
                        }
                    }
                });

                 jQuery('.navbar li.dropdown a').focus(function() {
                    var MenuLeftOffsets  = jQuery(this).parent().offset().left;
                    var Menu1LevelWidth = jQuery('.dropdown-menu', jQuery(this).parent()).width();
                    if (width - MenuLeftOffsets < Menu1LevelWidth * 2) {
                        jQuery(this).parent().children('.dropdown-menu').addClass('leftauto');
                    } else {
                        jQuery(this).parent().children('.dropdown-menu').removeClass('leftauto');
                    }
                    if (jQuery('.dropdown', jQuery(this).parent()).length > 0) {
                        var Menu2LevelWidth = jQuery('.dropdown-menu', jQuery(this).parent()).width();
                        if (width - MenuLeftOffsets - Menu1LevelWidth < Menu2LevelWidth) {
                            jQuery(this).parent().children('.dropdown-menu').addClass('left-side');
                        } else {
                            jQuery(this).parent().children('.dropdown-menu').removeClass('left-side');
                        }
                    }
                });
            }
        }

        /* ---------------------------------------------- /*
         * Navbar hover dropdown on desctop
         /* ---------------------------------------------- */

        function rambo_hoverDropdown(width, mobileTest) {
            if ((width > 1100) && (mobileTest !== true)) {
                jQuery('.dropdown-menu').removeAttr("style");
                var delay = 0;
                var setTimeoutConst;
                jQuery('.navbar li.dropdown, .navbar li.dropdown li.dropdown-submenu').hover(function() {
                        var jQuerythis = jQuery(this);
                        setTimeoutConst = setTimeout(function() {
                            jQuerythis.addClass('open');
                            jQuerythis.find('.dropdown-toggle').addClass('disabled');
                        }, delay);
                    },
                    function() {
                        clearTimeout(setTimeoutConst);
                        jQuery(this).removeClass('open');
                        jQuery(this).find('.dropdown-toggle').removeClass('disabled');
                    });
            } else {
                jQuery('.navbar  li.dropdown, .navbar li.dropdown > .dropdown-menu li.dropdown-submenu').unbind('mouseenter mouseleave');
                jQuery('.navbar [data-toggle=dropdown]').not('.binded').addClass('binded').on('click', function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    jQuery(this).parent().siblings().removeClass('open');
                    jQuery(this).parent().siblings().find('[data-toggle=dropdown]').parent().removeClass('open');
                    jQuery(this).parent().toggleClass();
                });
            }
        }

          /* ---------------------------------------------- /*
         * Navbar focus dropdown on desktop
         /* ---------------------------------------------- */

           
            const topLevelLinks = document.querySelectorAll('.navbar li.dropdown a');
            topLevelLinks.forEach(link => {
              link.addEventListener('focus', function(e) {
                this.parentElement.classList.add('open')
                e.preventDefault();

                e.target.parentElement.querySelectorAll( ".open" ).forEach( e =>
                    e.classList.remove( "open" ) );
              })             

            })

            jQuery('li a').focus(function() { 

             jQuery(this).parent().siblings().removeClass('open');

            });

            jQuery('a,input').bind('focus', function() {
             if(!jQuery(this).closest(".menu-item").length ) {
                topLevelLinks.forEach(link => {
                link.parentElement.classList.remove('open')
            })
            }
        })
        
        jQuery('li.dropdown').find('.caret').each(function(){
            jQuery(this).on('click', function(){
                var caret_id= jQuery(this).attr('id');
               jQuery('.' +caret_id).addClass('hp5');
                if( jQuery(window).width() <= 1100) {
                  jQuery('li.dropdown,li.dropdown-submenu').removeClass('open');
                  //jQuery('li.dropdown,li.dropdown-submenu').slideToggle().removeClass('hp5');
                  jQuery(this).parent().next().slideToggle();
                }
             return false;
            });
        });
 
            jQuery('#rambo_search_icon').on('click', function(){
                if( jQuery(window).width() <= 1100) {
                    jQuery( '#rambo_hps' ).addClass('search-box-outer dropdown open');
                    jQuery('.search-box-outer').attr('id','bkkkkkkkkkk');  
                    jQuery(this).attr('id','');
                    console.log('a');
            }
            });
            jQuery('#rambo_search_icon_open').on('click', function(){
                if( jQuery(window).width() <= 1100) {
                    jQuery( '#rambo_hps_open' ).addClass('search-box-outer dropdown');
                    jQuery('.search-box-outer').attr('id','rambo_hps');  
                    jQuery(this).attr('id','rambo_search_icon');
                    console.log('b');
            }
            });

        /* ---------------------------------------------- /*
         * Lightbox Search
         /* ---------------------------------------------- */

        jQuery('a[href="#searchbar_fullscreen"]').on("click", function(event) {
        event.preventDefault();
        jQuery("#searchbar_fullscreen").addClass("open");
        jQuery('#searchbar_fullscreen > form > input[type="search"]').focus();
        });

        jQuery("#searchbar_fullscreen, #searchbar_fullscreen button.close").on("click keyup", function(event) {
        if (
          event.target == this ||
          event.target.className == "close" ||
          event.keyCode == 27
        ) {
          jQuery(this).removeClass("open");
        }
      });


        /* ---------------------------------------------- /*
        * Navbar menu sticky
        /* ---------------------------------------------- */
        jQuery(window).bind('scroll', function () {
             if (jQuery(window).scrollTop() > 200) {
            jQuery('.menu').addClass('stickymenu1');
            jQuery('.menu').slideDown();
        } else {
            jQuery('.menu').removeClass('stickymenu1');
            jQuery('.menu').attr('style','');
        }
    });


});
  
    