Rambo-pro Theme README.

A Free Blue colored Business Blog theme that supports Primary menu's , Primary sidebar,Four widgets area at the footer region  etc. 
It has a perfect design that's great for any Business/Firms  Blogs who wants a new look for their site. Three page templates Home ,Blog and Contact Page. 
Rambo-pro supports featured slider managed from Theme Option Panel. Added woo-commerce support which helps you create an e-commerce store directly on your website.

Author: Priyanshu Mittal,Hari Maliya,Shahid Mansuri and Vibhor Purandare.
Theme Homepage Url: http://www.webriti.com/demo/wp/rambo-pro

About:
Rambo-pro a theme for business, consultancy firms etc  by Priyanshu Mittal (Author URI: http://www.webriti.com). 

The CSS, XHTML and design is released under GPL:
http://www.opensource.org/licenses/gpl-license.php

Feel free to use as you please. I would be very pleased if you could keep the Auther-link in the footer. Thanks and enjoy.

Appoinment supports Custom Menu, Widgets and 
the following extra features:

 - Pre-installed menu and content colors
 - Responsive
 - Custom sidebars
 - Support for post thumbnails
 - Similar posts feature
 - 4 widgetized areas in the footer
 - Customise Front Page 
 - Custom footer
 - Translation Ready 
 

# Basic Setup of the Theme.
-----------------------------------
Fresh installation!

1. Upload the Rambo-pro Theme folder to your wp-content/themes folder.
2. Activate the theme from the WP Dashboard.
3. Done!
=== Images ===

All images in Rambo-pro are licensed under the terms of the GNU GPL.

# Top Navigation Menu:
- Default the page-links start from the left! Use the Menus function in Dashboard/Appearance to rearrange the buttons and build your own Custom-menu. DO NOT USE LONG PAGE NAMES, Maximum 14 letters/numbers incl. spaces!
- Read more here: http://codex.wordpress.org/WordPress_Menu_User_Guide

=============Page Templates======================
1. Contact  Page Tempalte:- Create a page as you do in WordPress and select the page template with the name 'Contact'


===========Front Page Added with the theme=================
1 It has header(logo + menus),Home Featured Image, services,recent comments widgets and footer.

======Site Title and Description=============
Site Title and its description in not shown on home page besides this both are used above each page / post along with the search field.
	
Support
-------
Do you enjoy this theme? Send your ideas - issues - on the theme formn . Thank you!

@version 2.9.5
1. Updated freemius directory and fixed customizer styling issue.
@version 2.9.4
1. Updated font-awesome library.
2. Updated freemius directory.
@version 2.9.3
1. Updated freemius directory.
@version 2.9.2
1. Added freemius directory and code snippet.
@version 2.9.1
1. Added logo width setting.
@version 2.9
1. Fixed the issues with PHP 8.
2. Removed Google Plus icon and fixed some style issues.
@version 2.8.1
1. Minor issue fixed
@version 2.8
1. Added Header, Service, Testimonial, Team , Blog Variations & respective template
@version 2.7.3
1. jQuery compatibility issue resolved with WP 5.5
@version 2.7.2
1. Fixed service section image and icon issue.
@version 2.7.1
1. Make slider content multilingual.
@version 2.7
1. Added Repeater control.
@version 2.6.8
1. Fixed Theme layout managerissue.
2. JS conflict issue on theme page.
@version 2.6.7
1. Fixed Read more link issue.
@version 2.6.6
1. Added Dummy data import button in customize setting.
@version 2.6.5
1. Update custom logo height and width value.
@version 2.6.4
1. Added Italian Locale
@version 2.6.3
1. Added Russian Locale
@version 2.6.2
1. Update strings.
@version 2.6.1
1. Add Widget for Home-page section.
@version 2.6
1. Update Pot file.
2. Update font-awesome library 4.7.0
@version 2.5.9
1. Added more social icons like Youtube,Instagram,Skype.
@version 2.5.8
1. Added Spanish Language Translation(po and mo file).

@version 2.5.7
1. change wordpress to WordPress

@version 2.5.6
1. Change opacity effect.

@version 2.5.5
1. Fixed Jetpack gallery overlay issue.

@version 2.5.4
1. Solved Spelling Issue and Update Pot Files.

@version 2.5.3
1. Added Jetpack plugin support and Gallery overlay css.

@version 2.5.2
1. Update Pot Files.

@version 2.5.1
1. Fixed issue display section title and description with dummy data if customizer setttings blank in home page .

@version 2.5
1. Added Contact Form Plugin Support.
2. Added Google Map Support by Plugin.
@version 2.4
1. Add Default value for Portfolio Categories Column.
@version 2.3
1. Update Font Awesome Icon 4.5.0 to 4.6.3
2. Add Title Tag Support.
3. Add full-width-template, threaded-comments, two-columns, three-columns, four-columns, left-sidebar, right-sidebar, custom-colors, featured-images, footer-widgets, blog, sticky-post, custom-background, custom-menu,translation-ready, portfolio Tag.
@version 2.2
1. Footer social icon Issue solved.
@version 2.1
1. Added WPML support
@version 2.0
1. Added Customizer.
2. Styling issue Fixed.
@version 1.7
1. Add Blog Full width Template, Blog Left Sidebar Template , Blog Right Sidebar Template.
2. Fixed Styling Issue. 
@version 1.6
1. Add Custom Color Skin Control.
2. Update Font-awesome Folder.
@version 1.5
1.Add Category tabs with pagination on portfolio templates.
@version 1.4
1.Update Archive.php file.
2.Remove image cropping from all pages.
3.Fixed custom sidebar and footer widget.
 
@version 1.3.3
1. Menubar - bottom spacing manage and font size changes.
2. Homepage-->
	1. Slider-section -->
		1. Add slider image max-width.
		2. Changes in slider image crop file width(1600,550).
		3. Image Stretching issues on media screen - Resolved.
	2. Service-> service icon line-height manage - Minor Issue.
	3. index Project/Portfolio row-fluid class replaced with row.
	4. Project-> add clearfix on home page project/portfolio.
	5. Homepage - Footer area not display in boxed layout.

3. Footer Widgets->
    1. calendar widget style manage - Padding and Text alignment issue - Resolved.
	2. Links Spacing - minor issues like that padding, margin with top-bottom. 
	3. Recent comment aligning issues resolved with added padding.
 
4. About Us --> 
	1. Manage about-us page content according to about-us featured image.
	2. Testimonial image resizing -  70by70 replace with 80by80 in both of testimonials.
	3  Add clearfix in about-us on team section. 
 
5. Comment date issue resolved comment, current date is not correctly display before.
6. Blog Full feature image-->
      1. remove pull-left class.
	  2. add a.post-comment class in blog post comment bar.
	  3. Remove image width 100% in both of blog pages.
	  4. media screen manage on blog thumb page template. minor padding and floating issue. 
	  5. Sidebar - Calender widget manage with padding and text-alignment issue resolved.
7. Add all option pannel enable/disable option is set none empty field.
8. Manage minor styling issue contact form in contact page template.

@version 1.3.1
1.Add limit option of project portfolio on home page in option panel setting.

@version 1.3
1. Fix Banner strip title in Portfolio term
2. Fix Blog Thumb Title
3. Change in portfolio Category slug
4. Changes multiple fields to single input field in Footer customization

@version 1.2.8
1. Added taxonomy archive template.
@version 1.2.6
1. Added Woocommerce Support.
2. Some Widgets styles fixed.

@Version 1.2.4
1. Fix contact page miner bug (Replay to query email address ) 

@Version 1.2.3
1. Home page intro section added
2. In Service section adding Title and Description text
3. Footer call out area manage 


@Version 1.2.2
1) Service and project portfolio linkable with open new tab.

@Version 1.2.1
1)FA-Icons and Service TITLE linkable.
@Version 1.2
1)Video In TABS Issue Fixed.
2)Widgets Function nammed.
@Version 1.1
Added Menu JS Only
@Version 1.0
released with including featured->
1. 	Copyright sign removed from hyperlink.
2. 	GPLv3 added in the theme.
3. 	Comments and reply width set to 90% to maintain proper layout.
4. 	Title bug fixed.
5.	Tag lien linked to home.
6.	Front page layout manager 
7.	Enabled and disabled section by dragging and dropping.
8.	Add New Featured Front page layout manager.
9.	Client strip slide speed manage option.
10.	Re-installation setting + latest Default installation setting Marge Code
# --- EOF --- #