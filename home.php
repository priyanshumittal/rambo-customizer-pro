<?php	
if('page' == get_option('show_on_front')){ get_template_part('index');}
		else
		{
		$rambo_pro_theme_options=theme_data_setup();
		$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), $rambo_pro_theme_options );
		get_header();
		get_template_part('index','slider');
		$data =is_array($current_options['front_page_data']) ? $current_options['front_page_data'] : explode(",",$current_options['front_page_data']);
		if($data) 
		{
			foreach($data as $key=>$value)
			{						
			switch($value) 
				{
					case 'Call to action top':
					get_template_part('index', 'theme-introduction-top');	
					break; 
					
					case 'Service section': 
					/****** get index service  ********/
					get_template_part('index', 'service') ;
					break;
					
					case 'Project portfolio':
					/****** get index project  ********/
					get_template_part('index', 'project');					
					break;
					
					case 'Latest news': 			
					/****** get index recent blog  ********/
					echo '<div class="for_mobile">';
					get_template_part('index', 'recentblog');					
					echo '</div>';
					break; 
					
					case 'team': 			
					//****** get index team Section  ********
					get_template_part('index', 'team');					
					break; 

					case 'testimonial': 			
					//****** get index recent shop  ********
					get_template_part('index', 'testimonial');				
					break;

					case 'shop': 			
					//****** get index recent shop  ********
					get_template_part('index', 'shop');				
					break;
					
					case 'client': 			
					//****** get index client  ********
					get_template_part('index', 'client');				
					break;
					
					/****** get index Callout blog  ********/
					case 'Call to action bottom':
					get_template_part('index', 'theme-introduction');					
					break;
					
				}	
			} 
		} 
 
	}get_footer();
?>