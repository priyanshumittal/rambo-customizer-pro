<?php 
get_header();

//For pages
$slider_enable_page = sanitize_text_field( get_post_meta( get_the_ID(), 'slider_enable_page', true ));

if($slider_enable_page == true){
get_template_part('index','slider');
}

//For post
$slider_enable_post = sanitize_text_field( get_post_meta( get_the_ID(), 'slider_enable_post', true ));
if($slider_enable_post == true){
get_template_part('index','slider');
}
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), theme_data_setup() );
?>
<!-- Header Strip -->
<div class="hero-unit-small">
	<div class="container">
		<div class="row-fluid about_space">
			<div class="span8">
				<h2 class="page_head"><?php
				if(is_search())
				{
					echo $current_options['search_prefix']; echo ' '; printf( get_search_query() );
				}
				else
				{
				 
				if(isset($wp_query->post->post_title)){echo $wp_query->post->post_title;} else{ echo $current_options['404_prefix']; }
				
				} ?></h2>
			</div>
			
			<div class="span4">
				<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<div class="input-append search_head pull-right">
					<input type="text"   name="s" id="s" placeholder="<?php esc_attr_e( "Search", 'rambo' ); ?>" />
					<button type="submit" class="Search_btn" name="submit" ><?php esc_attr_e( "Go", 'rambo' ); ?></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /Header Strip -->