<?php
/**
Template Name: Team 4
*/
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), theme_data_setup() );
get_template_part('banner','strip');
get_template_part('section-variation/team/team-4');
if($current_options['team_temp_clients_enabled']==true)
{
get_template_part('index', 'client');
}
if($current_options['team_temp_cta_enabled']==true){
get_template_part('index', 'theme-introduction-top');
}
get_footer();
 ?>