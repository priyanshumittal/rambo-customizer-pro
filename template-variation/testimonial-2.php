<?php
/**
Template Name: Testimonial 2
*/
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), theme_data_setup() );
get_template_part('banner','strip');
require('testimonial-user-input-content.php');
get_template_part('section-variation/testimonial/testimonial-2');
if($current_options['testimonial_temp_cta_enabled']==true){
get_template_part('index', 'theme-introduction');
}
get_footer();
?>