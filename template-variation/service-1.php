<?php
/**
Template Name: Service 1
*/
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), theme_data_setup() );
get_template_part('banner','strip');
get_template_part('section-variation/services/service-1');
if($current_options['service_temp_cta_enabled']==true)
{
get_template_part('index', 'theme-introduction');
}
get_footer();
 ?>