<?php
/**
* @Theme Name	:	Rambopro
* @file         :	index-service.php
* @license      :	license.txt
* @filesource   :	wp-content/themes/rambopro/index-service.php
*/
$current_options = wp_parse_args(  get_option( 'rambo_pro_theme_options', array() ), theme_data_setup() );
get_template_part('section-variation/services/service-'.$current_options['service_layout_section']);
?>